export interface Type {
  name: string;
  routerLink?: string;
  description?: string;
  subtypes?: Type[];
  ciphers?: number[];
}
