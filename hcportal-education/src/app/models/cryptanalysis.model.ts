export interface Cryptanalysis {
    id: number,
    targetCipher: string,
    attackName: string,
    type: string,
    path: string,
    icon: string,
    description: string
}
  