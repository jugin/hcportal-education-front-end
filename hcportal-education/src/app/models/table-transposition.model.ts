export type TranspositionGuess = {
  block: number;
  decryptedText: string;
  sum: number;
};

export type TranspositionGuesses = TranspositionGuess[];

export type TranspositionGuessResults = {
  trueKey: TranspositionGuess;
  bestGuess?: TranspositionGuess;
  allGuesses?: TranspositionGuesses;
};
