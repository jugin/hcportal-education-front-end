export type TAnagramRow = {
  char: string;
  color?: string;
  showColor: boolean;
};

export type TAnagramColumn = {
  pos: number;
  isFixed?: boolean;
  rows: TAnagramRow[];
};

export type TAnagramColumns = TAnagramColumn[];

export type TPositionedColors = {
  rowPosition: number;
  color: string;
}[];

export type TTopColumns = {
  score: number;
  columns: TAnagramColumns;
  anagrams: string[];
  select: 'Select';
}[];

export type WorkerResult = {
  topColumns: TTopColumns;
  hint: string;
  indexGroups?: Map<string, number[]>;
};

export type TAnagramWorkerArgs = {
  movableColumns: TAnagramColumns;
  hint: string;
  minDictWordLength?: number;
  evaluationMethod?: EvaluationMethod;
  evaluationLength?: EvaluationLength;
};

export enum EvaluationMethod {
  DICT5000 = 'dictionary_5000',
  DICT1000 = 'dictionary_1000',
  DICT10000 = 'dictionary_10000',
  DICT25000 = 'dictionary_25000',
  DICT50000 = 'dictionary_50000',
  DICT100000 = 'dictionary_100000',
  BIGRAMS = 'bigrams',
}

export enum EvaluationLength {
  WHOLE = 'wholeTexts',
  HINT_LENGTH = 'hintLength',
  DOUBLE_HINT_LENGTH = 'doubleHintLength',
}

export interface TSelectValue<T> {
  value: T;
  label: string;
}
