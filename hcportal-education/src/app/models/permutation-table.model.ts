export type TPermutationRow = string[] | null;

export type TPermutaionColumn = {
  // space to add below strips, so that strips can be moved up and down by user
  pos: number;
  rows: TPermutationRow[];
};

export type TPermutationMovableColumns = TPermutaionColumn[];

export type TTopColumns = {
  score: number;
  decryptedText: string;
  permutation: string;
  columns: TPermutationMovableColumns;
  select: 'Select';
}[];

export type TPermutationWorkerArgs = {
  movableColumns: TPermutationMovableColumns;
  minDictWordLength?: number;
};
