export interface CipherType {
  id: number;
  name: string;
  slug: string;
  data?: {
    general: {
      title: string,
      description: string,
      reference: string,
    },
    realCiphers?: number[];
    cryptanalysis?: number[];
    examples: [{
      title: string,
      description?: string,
      images?: [
        {
          path: string,
          alt: string,
          width?: number,
          height?: number
        }
      ],
      weakness: {
        description?: string,
        items?: [{
          title?: string,
          description: string,
        }]
      },
      cryptography?: {
        title: string,
        description?: string,
        items?: [{
          title?: string,
          description: string,
          equation?: string,
        }]
      }
    }],
  };
}
