export const NAME_CIPHER = 'Cipher form the Futurama TV show';
export const TYPE_CIPHER = 'Demonstration of a';
export const FUTURAMA1 = "assets/img/futurama1.PNG";
export const FUTURAMA2 = "assets/img/futurama2.PNG";

export const MESSAGE = 'BackwardsTimeTravelMadeEasy';

export const TOP_GAP = 110;
export const SIDE_MENU = [
    {
        title: "Description of the cipher",
        active: true,
        id: "description",
        bottomPosition: 0,
        topPosition: 0

    },
    {
        title: "Input setup",
        active: true,
        id: "inputs",
        bottomPosition: 0,
        topPosition: 0
    },
    {
        title: "Simulation",
        active: true,
        id: "simulation",
        bottomPosition: 0,
        topPosition: 0
    }
];
