import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  UntypedFormGroup,
  Validators,
  UntypedFormControl,
} from "@angular/forms";
import CommonUtils from "../../../utils/common-utils";
import { FuturamaService } from "../futurama.service";
import {
  MESSAGE,
  NAME_CIPHER,
  TYPE_CIPHER,
  TOP_GAP,
  SIDE_MENU,
  FUTURAMA1,
  FUTURAMA2,
} from "../futurama.constant";
import { Subscription } from "rxjs";
import { HeaderService } from "../../../components/header/header.service";

@Component({
  selector: "app-futurama",
  styleUrls: ['./futurama.component.css', "../../../app.component.css"],
  templateUrl: "./futurama.component.html",
})
export class Futurama implements OnInit, OnDestroy {

  private message = MESSAGE;
  private subscrMessage: Subscription;

  futurama1 = FUTURAMA1;
  futurama2 = FUTURAMA2;

  // var for sideNavbar
  sideMenu = SIDE_MENU;
  topGap = TOP_GAP;

  toggleOptions: string[] = CommonUtils.createArrayOfLength(26, 1);
  cipherInputsForm: UntypedFormGroup;

  constructor(
    private futuramaService: FuturamaService,
    headerService: HeaderService
  ) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next(NAME_CIPHER);
    headerService.cipherType.next(TYPE_CIPHER);
  }

  ngOnInit(): void {
    this.cipherInputsForm = new UntypedFormGroup({
      message: new UntypedFormControl(MESSAGE, [
        Validators.required,
        Validators.pattern("^[a-zA-Z]+$"),
        Validators.minLength(20),
        Validators.maxLength(2000),
      ]),
    });

    this.subscrMessage = this.cipherInputsForm.controls.message.valueChanges.subscribe(
      (newMessage) => {
        this.message = newMessage;
      }
    );

    this.enCryptMessage();
  }

  public enCryptMessage() {
    this.futuramaService.textToImages('source-textarea', 'target-div', this.message);
  }

  ngOnDestroy() {
    this.subscrMessage.unsubscribe();
  }

  deCrypt(value: any) {
    this.futuramaService.decrypt(value);
  }

  erase() {
    this.futuramaService.eraseLastCharacter();
  }

}
