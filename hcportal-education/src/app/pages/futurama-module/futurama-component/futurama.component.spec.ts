import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Futurama } from './futurama.component';

describe('Futurama', () => {
  let component: Futurama;
  let fixture: ComponentFixture<Futurama>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Futurama ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Futurama);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
