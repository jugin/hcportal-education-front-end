const letterToImage: Record<string, string> = {
    'A': '/assets/symbols/futurama_symbols/a.png',
    'B': '/assets/symbols/futurama_symbols/b.png',
    'C': '/assets/symbols/futurama_symbols/c.png',
    'D': '/assets/symbols/futurama_symbols/d.png',
    'E': '/assets/symbols/futurama_symbols/e.png',
    'F': '/assets/symbols/futurama_symbols/f.png',
    'G': '/assets/symbols/futurama_symbols/g.png',
    'H': '/assets/symbols/futurama_symbols/h.png',
    'I': '/assets/symbols/futurama_symbols/i.png',
    'J': '/assets/symbols/futurama_symbols/j.png',
    'K': '/assets/symbols/futurama_symbols/k.png',
    'L': '/assets/symbols/futurama_symbols/l.png',
    'M': '/assets/symbols/futurama_symbols/m.png',
    'N': '/assets/symbols/futurama_symbols/n.png',
    'O': '/assets/symbols/futurama_symbols/o.png',
    'P': '/assets/symbols/futurama_symbols/p.png',
    'Q': '/assets/symbols/futurama_symbols/q.png',
    'R': '/assets/symbols/futurama_symbols/r.png',
    'S': '/assets/symbols/futurama_symbols/s.png',
    'T': '/assets/symbols/futurama_symbols/t.png',
    'U': '/assets/symbols/futurama_symbols/u.png',
    'V': '/assets/symbols/futurama_symbols/v.png',
    'W': '/assets/symbols/futurama_symbols/w.png',
    'X': '/assets/symbols/futurama_symbols/x.png',
    'Y': '/assets/symbols/futurama_symbols/y.png',
    'Z': '/assets/symbols/futurama_symbols/z.png',
    '0': '/assets/symbols/futurama_symbols/0.png',
    '1': '/assets/symbols/futurama_symbols/1.png',
    '2': '/assets/symbols/futurama_symbols/2.png',
    '3': '/assets/symbols/futurama_symbols/3.png',
    '4': '/assets/symbols/futurama_symbols/4.png',
    '5': '/assets/symbols/futurama_symbols/5.png',
    '6': '/assets/symbols/futurama_symbols/6.png',
    '7': '/assets/symbols/futurama_symbols/7.png',
    '8': '/assets/symbols/futurama_symbols/8.png',
    '9': '/assets/symbols/futurama_symbols/9.png',
    ';': '/assets/symbols/futurama_symbols/semicolon.png',
    ':': '/assets/symbols/futurama_symbols/double_dot.png',
    '.': '/assets/symbols/futurama_symbols/dot.png',
    '-': '/assets/symbols/futurama_symbols/dash.png',
    "'": '/assets/symbols/futurama_symbols/line.png',
    '!': '/assets/symbols/futurama_symbols/4dots.png',
};

let accumulatedText = '';

export class FuturamaService {

    textToImages(textareaId: string, targetDivId: string, message: string) {
        const textarea = document.getElementById(textareaId) as HTMLTextAreaElement;
        const targetDiv = document.getElementById(targetDivId);

        if (!textarea || !targetDiv) {
            console.error('Textarea or div not found');
            return;
        }

        const text = message;
        targetDiv.innerHTML = '';


        for (const char of text.toUpperCase()) {
            if (letterToImage[char]) {
                const img = document.createElement('img');
                img.src = letterToImage[char];
                targetDiv.appendChild(img);
            }
        }

    }

    decrypt(value: any): void {
     // if (button && button.tagName === 'BUTTON' && button.value) {
      if (value) {
            this.updateDivWithText(value);
        } else {
            console.error('No active button element with value found');
        }
    }

    updateDivWithText(text: string): void {
        const targetDiv = document.getElementById('target-div-2');
        if (!targetDiv) {
            console.error('Target div not found');
            return;
        }
        accumulatedText += text;
        targetDiv.textContent = accumulatedText;
    }

    eraseLastCharacter(): void {
        console.log('eraseLastCharacter function called');

        if (accumulatedText.length > 0) {
            accumulatedText = accumulatedText.slice(0, -1);

            const targetDiv = document.getElementById('target-div-2');
            if (targetDiv) {
                targetDiv.textContent = accumulatedText;
            } else {
                console.error('Target div not found');
            }
        } else {
            console.error('No characters to erase');
        }
    }



}
