import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Futurama } from './futurama-component/futurama.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { FuturamaService } from './futurama.service';

const caesarRoutes: Routes = [
    { path: '', component: Futurama },
];

@NgModule({
    declarations: [
        Futurama
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(caesarRoutes),
        SharedModule
    ],
    providers: [FuturamaService]

})
export class FuturamaModule { }
