

export const PLAIN_TEXT = 'theenglishwikipediawasthefirstwikipediaeditionandhasremainedthelargestithaspioneeredmanyideasasconventionspoliciesorfeatureswhichwerelateradoptedbywikipediaeditionsinsomeoftheotherlanguagestheseideasincludefeaturedarticlestheneutralpointofviewpolicynavigationtemplatesthesortingofshortstubarticlesintosubcategoriesdisputeresolutionmechanismssuchasmediationandarbitrationandweeklycollaborationstheenglishwikipediahasadoptedfeaturesfromwikipediasinotherlanguagesthesefeaturesincludeverifiedrevisionsfromthegermanwikipediadewikiandtownpopulationlookuptemplatesfromthedutchwikipedianlwikialthoughtheenglishwikipediastoresimagesandaudiofilesaswellastextfilesmanyoftheimageshavebeenmovedtowikimediacommonswiththesamenameaspassedthroughfileshowevertheenglishwikipediaalsohasfairuseimagesandaudiovideofileswithcopyrightrestrictionsmostofwhicharenotallowedoncommonsmanyofthemostactiveparticipantsinthewikimediafoundationandthedevelopersofthemediawikisoftwarethatpowerswikipediaareenglishusers';
export const KEY = 'abr';

export const TEXT_MIN = 1;
export const TEXT_MAX = 2000;
export const KEY_MIN = 1;
export const KEY_MAX = 75;

export const NAME_CIPHER = 'Autokey Cipher';
export const TYPE_CIPHER = 'Autokey to Vigenere transformation, Friedman test and brute-force attack on';

export const TOP_GAP = 110;
export const SIDE_MENU = [
  {
    title: "Description of the cipher",
    active: true,
    id: "description",
    bottomPosition: 1000,
    topPosition: 1000
  },
  {
    title: "Input setup",
    active: true,
    id: "inputs",
    bottomPosition: 0,
    topPosition: 0
  },
  {
    title: "Transformation of the Autokey cipher text into Vigenere",
    active: true,
    id: "transformation_description",
    bottomPosition: 0,
    topPosition: 0
  },
  {
    title: "Friedman test",
    active: true,
    id: "friedman_test",
    bottomPosition: 0,
    topPosition: 0
  },
  {
    title: "Dividing the cipher text into cosets",
    active: true,
    id: "dividing_into_cosets",
    bottomPosition: 0,
    topPosition: 0
  },
  {
    title: "Description of the attack",
    active: true,
    id: "description_of_the_attack",
    bottomPosition: 0,
    topPosition: 0
  },
  {
    title: "Checking the result",
    active: true,
    id: "results_table",
    bottomPosition: 0,
    topPosition: 0
  }
];

export const MIN_KEY_LEN = 1;
export const MAX_KEY_LEN = 15;

export const EQUATION_IC1 = "\\sum_{i=A}^{i=Z}\\frac {n_i (n_i - 1)} {N (N - 1)}";
export const EQUATION_IC2 = "\\sum_{i=A}^{i=Z} {n_i}^2";
export const EQUATION_IC3 = "y_i = \\left\\{\\begin{matrix} x_i + k_i \\text{ } \\text{mod 26}, & \\text{for i < r;} \\\\x_i + x_{i-r} \\text{ } \\text{mod 26}, & \\text{for i} \\geq \\text{r.}\\end{matrix}\\right."
export const EQUATION_IC4 = "x_i = \\left\\{\\begin{matrix} y_i + k_i \\text{ } \\text{mod 26}, & \\text{for i < r;} \\\\y_i + x_{i-r} \\text{ } \\text{mod 26}, & \\text{for i} \\geq \\text{r.}\\end{matrix}\\right."
export const EQUATION_IC5 = "Y_i = \\left\\{\\begin{matrix} y_i       \\text{ } \\text{mod 26}, & \\text{for i < r;} \\\\y_i - Y_{i-r} \\text{ } \\text{mod 26}, & \\text{for i} \\geq \\text{r.}\\end{matrix}\\right."
export const EQUATION_L1_DIST = "\\sum_{i=A}^{i=Z} | {m_i} - {r_i} |";

export const EQUATION_xi = "x_0, x_1, x_2, ...";
export const EQUATION_yi = "y_0, y_1, y_2, ...";
export const EQUATION_Yi = "Y_0, Y_1, Y_2, ...";
export const EQUATION_ki = "k_0, k_1, ..., k_{r-1}";
export const EQUATION_ki_long = "k_0, k_1, ... , k_{r-1}, ...,  -k_0, -k_1, ..., -k_{r-1}";

