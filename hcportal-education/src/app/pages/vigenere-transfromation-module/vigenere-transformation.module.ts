import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VigenereTransformation} from './vigenere-transformation/vigenere-transformation.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { VigenereTransformationService } from './vigenere-transformation.service';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {VigenereBoxesComponent} from './vigenere-transformation/vigenere-boxes-component/vigenere-boxes.component';
import { VigenereBoxesService } from './vigenere-transformation/vigenere-boxes-component/vigenere-boxes.service';
const vigenereTransformationRoutes: Routes = [
  { path: '', component: VigenereTransformation },
];

@NgModule({
    declarations: [
        VigenereTransformation,
        VigenereBoxesComponent
    ],
  imports: [
    CommonModule,
    RouterModule.forChild(vigenereTransformationRoutes),
    SharedModule,
    MatButtonToggleModule
  ],
  providers: [VigenereTransformationService, VigenereBoxesService]

})
export class VigenereTransfromationModule { }
