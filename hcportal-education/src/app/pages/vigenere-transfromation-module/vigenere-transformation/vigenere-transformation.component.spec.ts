import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VigenereTransformation } from './vigenere-transformation.component';

describe('VigenereTransformationComponent', () => {
  let component: VigenereTransformation;
  let fixture: ComponentFixture<VigenereTransformation>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VigenereTransformation ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VigenereTransformation);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
