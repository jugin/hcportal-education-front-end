import { Component, OnInit, Input } from '@angular/core';
import AnalysisText from '../../../../analysis-text';
import {
  LANGUAGEIC_DATA,
  COLORS,
} from '../../../../constants/language.constants';
import CommonUtils from '../../../../utils/common-utils';
import { VigenereBoxesService } from './vigenere-boxes.service';
import { MIN_KEY_LEN, MAX_KEY_LEN } from '../../vigenere-transform.constants';
import { HeaderService } from "../../../../components/header/header.service";

@Component({
  selector: 'app-vigenere-boxes-component',
  templateUrl: './vigenere-boxes.component.html',
  styleUrls: [
    '../../../../app.component.css',
    './vigenere-boxes.component.css',
  ],
})
export class VigenereBoxesComponent implements OnInit {
  @Input() allBoxesAvgIc = [];
  @Input() ic;
  @Input() allBoxes;
  @Input() transformations;
  @Input() allBoxesIc: number[][] = [];

  @Input() selectedValue = '1';
  @Input() selectedValueNum: number = 1;
  @Input() toggleOptions: string[] = [];

  private nearestLanguage: string;
  passedMinIc = false;
  colors = COLORS;

  single_transformation;

  constructor(
    private vigenereBoxesService: VigenereBoxesService,
    headerService: HeaderService
  ) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next('Autokey Cipher');
    headerService.cipherType.next('Autokey to Vigenere transformation, Friedman test and bruteforce attack on');

    this.vigenereBoxesService.selectedValue.subscribe((newSelectedValue) => {
      this.selectedValue = newSelectedValue.toString();
      this.selectedValueNum = newSelectedValue;
    });
  }

  ngOnInit() {
    // generate array <2-16>
    this.toggleOptions = CommonUtils.createArrayOfLength(
      MAX_KEY_LEN,
      MIN_KEY_LEN
    );
    this.showSingleTransformation();
  }

  ngOnChanges() {
    this.showSingleTransformation();
  }

  public showSingleTransformation() {
    this.single_transformation =
      this.transformations[this.selectedValueNum - 1];
    this.ic = this.allBoxesAvgIc[parseInt(this.selectedValue) - 1];
  }

  // Change data for updateFlagCompareFreq based of selection <1-26>
  public selectionOfGraphChanged(item, numItem?: number) {
    this.selectedValueNum = numItem ? numItem : item.value;
    this.single_transformation =
      this.transformations[this.selectedValueNum - 1];
    this.ic = numItem
      ? this.allBoxesAvgIc[numItem - 1]
      : this.allBoxesAvgIc[item.value - 1];
    // console.log("All Boxes Avg IC: ", this.allBoxesAvgIc);

    this.nearestLanguage = AnalysisText.findNearestLanguage(
      this.ic,
      LANGUAGEIC_DATA
    );
    if (this.nearestLanguage === 'Min IC') {
      this.passedMinIc = true;
    }
  }
}
