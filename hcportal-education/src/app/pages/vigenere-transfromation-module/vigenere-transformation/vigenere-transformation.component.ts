import { Component, OnInit, OnDestroy} from '@angular/core';
import { MatLegacyTableDataSource as MatTableDataSource } from "@angular/material/legacy-table";
import { SortTable, Ordering } from "../../../models/common.model";
import {
  SIDE_MENU, TOP_GAP, PLAIN_TEXT, TEXT_MAX,
  TEXT_MIN, KEY, KEY_MIN, MIN_KEY_LEN, MAX_KEY_LEN,
  EQUATION_IC1, EQUATION_IC2, EQUATION_IC3, EQUATION_IC4, EQUATION_IC5, EQUATION_L1_DIST,
  EQUATION_xi, EQUATION_yi, EQUATION_Yi, EQUATION_ki, EQUATION_ki_long
} from '../vigenere-transform.constants';
import { UntypedFormGroup, UntypedFormControl,Validators } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { COLORS, A_ASCII, EN_ALPHABET_FREQUENCY} from "../../../constants/language.constants";
import AnalysisText from "../../../analysis-text";
@Component({
  selector: 'app-vigenere-transformation',
  templateUrl: './vigenere-transformation.component.html',
  styleUrls: ["../../../app.component.css",'./vigenere-transformation.component.css']
})
export class VigenereTransformation implements OnInit, OnDestroy {

  // variables to display 'Dividing into cosets / Transformation' sections
  transformationCompleted = false;
  columnsBestResults: string[] = ["i", "key_i", "score", "text"];
  stickyColumns = { stickyStart: "i", stickyEnd: "", stickyHeader: true };
  dataSourceBestResults = new MatTableDataSource<any>();
  dataSourceBestResultsReady = new Subject<boolean>();
  sortBestResults: SortTable = {
    sortByColumn: "sum",
    order: Ordering.asc,
  } as SortTable;
  bestShiftResults = [];

  // Constants imported and used for HTML settings
  textMax = TEXT_MAX;      textMin = TEXT_MIN;
  sideMenu = SIDE_MENU;
  topGap = TOP_GAP;
  equation_ic1 = EQUATION_IC1;
  equation_ic2 = EQUATION_IC2;
  equation_ic3 = EQUATION_IC3;
  equation_ic4 = EQUATION_IC4;
  equation_ic5 = EQUATION_IC5;
  equation_score = EQUATION_L1_DIST;
  equation_xi = EQUATION_xi;
  equation_yi = EQUATION_yi;
  equation_Yi = EQUATION_Yi;
  equation_ki = EQUATION_ki;
  equation_ki_long = EQUATION_ki_long;



  colors = COLORS;
  message = PLAIN_TEXT;
  key = KEY;

  // global values used for this module
  cipherInputsForm: UntypedFormGroup;
  private subscrMessage: Subscription;
  private subscrKey: Subscription;
  maxSelectedValue = 15;
  formatted_text = "";
  formatted_key = "";

  encrypted_text:string = "";
  decrypted_text:string = "";

  IC = -1;
  highestIC = [];
  highestICValue = 0;
  allCosetsFrequency: number[][][] = [];
  allCosetsIc: number[][] = [];
  allCosetsAvgIc = [];
  allCosets;

  result:string = "";
  result_title:string = "Encryption Result";

  toggleOptions: string[] = [];

  all_tranformation = [];


  ngOnInit(): void {
    this.cipherInputsForm = new UntypedFormGroup({
      key: new UntypedFormControl(this.key, [
        Validators.required,
        Validators.pattern("^[a-zA-Z]+$"),
        Validators.minLength(MIN_KEY_LEN),
        Validators.maxLength(MAX_KEY_LEN),
      ]),
      message: new UntypedFormControl(this.message, [
        Validators.required,
        Validators.pattern("^[a-zA-Z]+$"),
        Validators.minLength(20),
        Validators.maxLength(2000),
      ]),
    });
    this.subscrMessage = this.cipherInputsForm.controls.message.valueChanges.subscribe(
      (newMessage) => { this.message = newMessage; });
    this.subscrKey = this.cipherInputsForm.controls.key.valueChanges.subscribe(
      (newKey) => { this.key = newKey; });
    this.runEncryptAndTransform();
  }

  runEncryptAndTransform(){
    this.transformationCompleted = false;
    this.bestShiftResults = [];
    this.allCosets = [];
    this.all_tranformation = [];

    // 1st encrypt with Autokey
    this.encrypted_text = this.computeAutokeyEncryption(this.message, this.key);

    let best_tranformation = "";
    let maxObservedIC = -1;
    let best_cosets = [];

    // 2nd compute all transformations between MIN_KEY_LEN and MAX_KEY_LEN (2-15)
    for(let i = MIN_KEY_LEN; i <= MAX_KEY_LEN; i++){
      let transformation = this.transformAutokeyToVigenere(this.encrypted_text, i);
      let res = this.computeSingleCoset(transformation.split(""), i*2);
      this.allCosets.push(res);
      this.all_tranformation.push(transformation);
    }

    // 3rd compute frequency and IC of all coset transformations
    let result = this.computeFrequencyAndICForCosets(this.allCosets);

    this.allCosetsAvgIc = result.all_cosets_avg_ic;
    this.allCosetsFrequency = result.all_cosets_frequency;
    this.allCosetsIc = result.all_cosets_ic;

    // 4th find transformation with highest score (most likely to be our solution)
    for(let i = 0; i < result.all_cosets_avg_ic.length; i+=1){
      let IC = result.all_cosets_avg_ic[i].value;
      if(IC > maxObservedIC){
        maxObservedIC = IC;
        best_cosets = [this.allCosets[i] , result.all_cosets_frequency[i]];
        best_tranformation = this.all_tranformation[i];
      }
    }

    let cosets = best_cosets[0];
    let frequencies = best_cosets[1];
    let shifts = [];
    let copyAllBoxesAvgIc = Object.assign([], this.allCosetsAvgIc);

    const sortedIC = copyAllBoxesAvgIc
      .sort((a,b) => b.value - a.value)
      .slice();

    this.highestIC = sortedIC.slice(0, 5);
    this.highestICValue = this.highestIC[0].coset_id;

    // 5th compute shifts for best transformation
    for(let c = 0; c < cosets.length; c++){
      let best_dot_product:number = -1;
      let best_shift_index = -1;
      for(let i = 0; i < 26; i++){
        let current_dot_product = this.computeDotProduct(frequencies[c], i);
        if(current_dot_product > best_dot_product){
          best_dot_product = current_dot_product;
          best_shift_index = i;
        }
      }
      // this will be displayed in 'Checking the results' table
      this.bestShiftResults.push(
        {
          'i':c+1,
          'key_i':String.fromCharCode(best_shift_index + A_ASCII),
          'score':best_dot_product,
          'text':cosets[c]
        }
      );
      shifts.push(best_shift_index);
    }

    // 6th build the key off the shifts
    let key_2r = "";
    for(let shift of shifts){
      key_2r += String.fromCharCode(shift + A_ASCII);
    }
    this.prepareDataToBeDisplayed();

    // 7th decrypt with new key
    this.decrypted_text = this.computeVigenereDecryption(best_tranformation, key_2r);
  }

  computeAutokeyEncryption(plainText:string, key:string){
    if(plainText === '' || plainText === null || plainText === undefined){ alert("Enter valid input text.")}
    if(key === '' || key === null || key === undefined){ alert("Enter valid encryption key.")}
    let k_lenght = key.length;
    let encrypted_text = "";

    for (let i = 0; i < plainText.length; i++) {
      if(i < k_lenght){
        const letterAscii = plainText[i].charCodeAt(0);
        const k = key[i % k_lenght].charCodeAt(0) - A_ASCII;
        const encryptedLetter: number =
          ((letterAscii - A_ASCII + k) % 26) + A_ASCII;
          encrypted_text += String.fromCharCode(encryptedLetter);
      }
      else{
        const letterAscii = plainText[i].charCodeAt(0);
        const k = plainText[i - k_lenght].charCodeAt(0) - A_ASCII;
        const encryptedLetter: number =
        ((letterAscii - A_ASCII + k) % 26) + A_ASCII;
        encrypted_text += String.fromCharCode(encryptedLetter);
      }
    }
    return encrypted_text;
  }

  computeVigenereDecryption(vigenere_encrypted_text: string, key_2r: string): string {
    let plain_text = "";
    for(let i = 0; i < vigenere_encrypted_text.length; i++) {
      const letterAscii = vigenere_encrypted_text[i].charCodeAt(0);
      const key = key_2r[i % key_2r.length].charCodeAt(0) - A_ASCII;
      const decryptedLetter: number =
        ((letterAscii - A_ASCII - key + 26) % 26) + A_ASCII;
        plain_text += String.fromCharCode(decryptedLetter);
    }
    return plain_text;
  }

  computeSingleCoset(message: string[], i:number): string[]{
    const coset = [];
    for (let index = 0; index < message.length; index++) {
      const numBox = index % i;
      if (!coset[numBox]) {
        coset[numBox] = [];
      }
      coset[numBox].push(message[index]);
    }
    const boxToString: string[] = [];
    coset.forEach((element) => {
      boxToString.push(element.join(""));
    });
    return boxToString;
  }

  computeFrequencyAndICForCosets(allCosets) {
    let coset_id_counter = 1;
    const all_cosets_frequency = [];
    const all_cosets_ic = [];
    const all_cosets_avg_ic = [];
    allCosets.forEach((cosets) => {
      const coset_frequency: number[][] = [];
      const coset_ic: number[] = [];
      cosets.forEach((box) => {
        const coset_freq: number[] = AnalysisText.getFrequencyOfText(box);
        coset_frequency.push(coset_freq);
        coset_ic.push(AnalysisText.getIC(coset_freq, box.length));
      });

      all_cosets_frequency.push(coset_frequency);
      all_cosets_ic.push(coset_ic);

      //  Compute sum and avg
      const sum = coset_ic.reduce((a, b) => a + b, 0);
      const avg = sum / coset_ic.length || 0;
      const tmp = { coset_id: 0, value: 0 };
      tmp.coset_id = coset_id_counter;
      tmp.value = avg;
      all_cosets_avg_ic.push(tmp);
      coset_id_counter +=1;
    });
    return { all_cosets_frequency, all_cosets_ic, all_cosets_avg_ic };
  }

  transformAutokeyToVigenere(message:string, key_r:number):string{
    let transformed = "";
    for (let i = 0; i < message.length; i++) {
      if(i < key_r){
        transformed += message[i];
      }
      else{
        const letter_ascii = message[i].charCodeAt(0) - A_ASCII;
        const key = transformed[i - key_r].charCodeAt(0) - A_ASCII;
        const transformed_letter: number =
        ((letter_ascii - key + 26) % 26) + A_ASCII;
        transformed += String.fromCharCode(transformed_letter);
      }
    }
    return transformed;
  }

  computeDotProduct(coset_frequencies, shift:number){
    let dotProduct = 0;
    let ref_frequencies = EN_ALPHABET_FREQUENCY;
    for(let i = 0; i < coset_frequencies.length; i++){
      let shift_index = (i + shift) % 26;
      dotProduct += coset_frequencies[shift_index] * ref_frequencies[i];
    }
    return dotProduct / coset_frequencies.length;
  }

  prepareDataToBeDisplayed(){
    this.dataSourceBestResults.data = this.bestShiftResults
    this.dataSourceBestResultsReady.next(true);
    this.transformationCompleted = true;
  }

  ngOnDestroy(): void {
    this.subscrMessage.unsubscribe()
    this.subscrKey.unsubscribe()
  }
}
