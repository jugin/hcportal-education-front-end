export const NAME_CIPHER = 'Scytale';
export const TYPE_CIPHER = 'Brute-force attack on';
export const MIN_COLUMNS = 1;
export const MAX_COLUMNS = 100;
export const MIN_MESSAGE_LENGTH = 5;
export const MAX_MESSAGE_LENGTH = 2000;
export const INITIAL_COLUMNS = 10;
export const INITIAL_MESSAGE =
  'theenglishwikipediawasthefirstwikipediaeditionandhasremainedthelargestithaspioneeredmanyideasasconventionspoliciesorfeatureswhichwerelateradoptedbywikipediaeditionsinsomeoftheotherlanguagestheseideasincludefeaturedarticlestheneutralpointofviewpolicynavigationtemplatesthesortingofshortstubarticlesintosubcategoriesdisputeresolutionmechanismssuchasmediationandarbitrationandweeklycollaborationstheenglishwikipediahasadoptedfeaturesfromwikipediasinotherlanguagesthesefeaturesincludeverifiedrevisionsfromthegermanwikipediadewikiandtownpopulationlookuptemplatesfromthedutchwikipedianlwikialthoughtheenglishwikipediastoresimagesandaudiofilesaswellastextfilesmanyoftheimageshavebeenmovedtowikimediacommonswiththesamenameaspassedthroughfileshowevertheenglishwikipediaalsohasfairuseimagesandaudiovideofileswithcopyrightrestrictionsmostofwhicharenotallowedoncommonsmanyofthemostactiveparticipantsinthewikimediafoundationandthedevelopersofthemediawikisoftwarethatpowerswikipediaareenglishusers';
export const TOP_GAP = 120;
export const SIDE_MENU = [
  {
    title: 'Input setup',
    active: true,
    id: 'inputs',
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: 'Transposition table',
    active: true,
    id: 'table',
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: 'Bruteforce setup',
    active: true,
    id: 'findKey',
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: 'Best block size candidate',
    active: true,
    id: 'results',
    bottomPosition: 0,
    topPosition: 0,
  },
];
