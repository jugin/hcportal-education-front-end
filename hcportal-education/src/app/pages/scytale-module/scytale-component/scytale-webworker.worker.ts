/// <reference lib="webworker" />

import {
  TranspositionGuess,
  TranspositionGuessResults,
} from '../../../models/table-transposition.model';
import AnalysisText from '../../../analysis-text';

function guessBlock(
  encText: string,
  decText: string,
  block: number,
  refBigrams
): TranspositionGuessResults {
  if (encText.length === 0) {
    return null;
  }
  if (encText.length === 1) {
    return null;
  }

  const decTextFreqBigrams = AnalysisText.getFreqOfBigrams(decText);
  const decTexttSum: number = AnalysisText.getSumOfDiffBigramsFromRef(
    decTextFreqBigrams[0],
    decText.length - 1,
    refBigrams
  );

  const guessResult: TranspositionGuessResults = {
    trueKey: {
      block,
      decryptedText: decText,
      sum: decTexttSum,
    },
    allGuesses: [],
  };

  const encTextLen = encText.length;
  for (let colNum = 2; colNum < encTextLen - 1; colNum++) {
    let decTextIteration = '';
    const rowsNum = Math.ceil(encTextLen / colNum);

    const longColNum = encTextLen % colNum;

    for (let row = 0; row < rowsNum; row++) {
      for (let col = 0; col < colNum; col++) {
        let foundCharIndex;
        if (col > longColNum) {
          foundCharIndex = col * (rowsNum - 1) + longColNum + row;
        } else {
          foundCharIndex = col * rowsNum + row;
        }

        const foundChar = encText[foundCharIndex];
        decTextIteration += foundChar;
      }
    }

    const freqBigramsIteration =
      AnalysisText.getFreqOfBigrams(decTextIteration);
    // Get diff of Referal values (EN) and decrypted text
    // that is fitnes func for evaluate of decrypted text
    const sumIteration: number = AnalysisText.getSumOfDiffBigramsFromRef(
      freqBigramsIteration[0],
      decTextIteration.length - 1,
      refBigrams
    );

    const iterationGuess: TranspositionGuess = {
      block: colNum,
      decryptedText: decTextIteration,
      sum: sumIteration,
    };

    guessResult.allGuesses.push(iterationGuess);

    if (guessResult.bestGuess === undefined) {
      guessResult.bestGuess = iterationGuess;
    } else if (sumIteration < guessResult.bestGuess?.sum) {
      guessResult.bestGuess = iterationGuess;
    }
  }

  return guessResult;
}

addEventListener('message', ({ data }) => {
  postMessage(guessBlock(data[0], data[1], data[2], data[3]));
});
