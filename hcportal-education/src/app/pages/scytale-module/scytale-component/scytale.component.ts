import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { MatLegacyTableDataSource as MatTableDataSource } from '@angular/material/legacy-table';
import { Subject, Subscription } from 'rxjs';
import { HeaderService } from 'src/app/components/header/header.service';
import { REF_BIGRAMS } from '../../../constants/language.constants';
import {
  TOP_GAP,
  SIDE_MENU,
  INITIAL_COLUMNS,
  INITIAL_MESSAGE,
  NAME_CIPHER,
  TYPE_CIPHER,
  MAX_COLUMNS,
  MAX_MESSAGE_LENGTH,
  MIN_COLUMNS,
  MIN_MESSAGE_LENGTH,
} from '../scytale.constants';
import {
  TranspositionGuess,
  TranspositionGuessResults,
} from '../../../models/table-transposition.model';
import { SortTable, Ordering } from '../../../models/common.model';
import TranspositionUtils from '../../../utils/transposition-utils';

@Component({
  selector: 'app-scytale',
  templateUrl: './scytale.component.html',
  styleUrls: ['../../../app.component.css', './scytale.component.css'],
})
export class ScytaleComponent implements OnInit, OnDestroy {
  private blockSize = INITIAL_COLUMNS;
  private message = INITIAL_MESSAGE;
  private shouldAddX = false;
  private subscribedBlockSize: Subscription;
  private subscribedMessage: Subscription;
  private subscribedShouldAddX: Subscription;
  private webWorker: Worker;
  private refBigrams = JSON.parse((REF_BIGRAMS as any).default);

  blockSizeToDisplay: string[];
  tableData;
  minBlockSize = MIN_COLUMNS;
  maxBlockSize = MAX_COLUMNS;
  minMessageLength = MIN_MESSAGE_LENGTH;
  maxMessageLength = MAX_MESSAGE_LENGTH;
  encryptedMessage;
  sideMenu = SIDE_MENU;
  topGap = TOP_GAP;
  cipherInputsForm: UntypedFormGroup;
  tableReady = false;
  tableReadySubject = new Subject<boolean>();
  calcDone = false;
  transpositionGuessResults: TranspositionGuessResults | null;
  bestGuessResult: TranspositionGuess | null;
  //Variables for Final Table
  columnsBestResults: string[] = ['block', 'sum', 'decryptedText'];
  stickyColumns = {
    stickyStart: 'block',
    stickyEnd: '',
    stickyHeader: true,
  };
  dataSourceBestResults = new MatTableDataSource<any>();
  dataSourceBestResultsReady = new Subject<boolean>();
  sortBestResults: SortTable = {
    sortByColumn: 'block',
    order: Ordering.asc,
  } as SortTable;

  constructor(headerService: HeaderService) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next(NAME_CIPHER);
    headerService.cipherType.next(TYPE_CIPHER);
  }

  ngOnInit(): void {
    this.cipherInputsForm = new UntypedFormGroup({
      blockSize: new UntypedFormControl(this.blockSize, [
        Validators.required,
        Validators.min(this.minBlockSize),
        Validators.max(this.maxBlockSize),
      ]),
      message: new UntypedFormControl(this.message, [
        Validators.required,
        Validators.pattern('^[a-zA-Z]+$'),
        Validators.minLength(this.minMessageLength),
        Validators.maxLength(this.maxMessageLength),
      ]),
      shouldAddX: new UntypedFormControl(this.shouldAddX),
    });

    this.subscribedMessage =
      this.cipherInputsForm.controls.message.valueChanges.subscribe(
        (newMessage) => {
          this.message = newMessage;
        }
      );

    this.subscribedBlockSize =
      this.cipherInputsForm.controls.blockSize.valueChanges.subscribe(
        (newBlockSize) => {
          this.blockSize = newBlockSize;
        }
      );

    this.subscribedShouldAddX =
      this.cipherInputsForm.controls.shouldAddX.valueChanges.subscribe(
        (newShoudAddX) => {
          this.shouldAddX = newShoudAddX;
        }
      );

    if (typeof Worker !== 'undefined') {
      this.webWorker = new Worker(
        new URL('./scytale-webworker.worker', import.meta.url),
        {
          type: `module`,
        }
      );

      this.webWorker.onmessage = (event) => {
        this.transpositionGuessResults =
          event.data as TranspositionGuessResults;
        this.bestGuessResult = this.transpositionGuessResults?.bestGuess;

        const allBestGuessNthEl =
          this.transpositionGuessResults?.allGuesses?.map((guess) => {
            guess.sum = Math.round(guess.sum * 1000) / 1000;
            return guess;
          });
        this.tableReadySubject.next(true);
        this.tableReady = true;
        this.dataSourceBestResults.data = allBestGuessNthEl;
        this.dataSourceBestResultsReady.next(true);
        this.calcDone = true;
      };
    }

    this.createTableAndEncrypt();
    this.tableReadySubject.next(true);
    this.tableReady = true;
    this.guess();
  }

  public createTableAndEncrypt() {
    this.tableReadySubject.next(false);
    this.tableReady = false;
    this.createTable();
    this.encrypt();
    this.guess();
  }

  public createTable() {
    this.tableReadySubject.next(false);
    this.tableReady = false;
    const splitMessage = this.message.split('');
    const resultTableData = [];
    const rows = Math.ceil(splitMessage.length / this.blockSize);

    const fillerElement = this.shouldAddX ? 'x' : null;

    // populate table by rows
    for (let i = 0; i < rows; i++) {
      // populate first column of table with row numbers
      resultTableData[i] = { ...resultTableData[0], ['0']: `${i + 1}` };

      for (let j = 0; j < this.blockSize; j++) {
        resultTableData[i] = {
          ...resultTableData[i],
          // retrieve char from plaintext calculated by number of columns * current row + column offset
          [`${j + 1}`]: splitMessage[this.blockSize * i + j]
            ? splitMessage[this.blockSize * i + j].toLowerCase()
            : fillerElement,
        };
      }
    }

    this.blockSizeToDisplay = Array.from(
      // + 1 because first column is row number
      { length: this.blockSize + 1 },
      (_, columnIndex) => `${columnIndex}`
    );
    this.tableData = resultTableData;
    this.tableReadySubject.next(true);
    this.tableReady = true;
  }

  private encrypt() {
    this.encryptedMessage = TranspositionUtils.encrypt(this.tableData);
  }

  public guess() {
    this.calcDone = false;
    this.dataSourceBestResultsReady.next(false);
    this.transpositionGuessResults = null;
    this.bestGuessResult = null;

    this.webWorker.postMessage([
      this.encryptedMessage,
      this.message.toLowerCase(),
      this.blockSize,
      [...this.refBigrams],
    ]);
  }

  ngOnDestroy() {
    this.subscribedBlockSize.unsubscribe();
    this.subscribedMessage.unsubscribe();
    this.subscribedShouldAddX.unsubscribe();
    this.webWorker.terminate();
  }
}
