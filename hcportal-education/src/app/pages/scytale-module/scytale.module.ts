import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { ScytaleComponent } from './scytale-component/scytale.component';

const scytaleRoutes: Routes = [{ path: '', component: ScytaleComponent }];

@NgModule({
  declarations: [ScytaleComponent],
  imports: [CommonModule, RouterModule.forChild(scytaleRoutes), SharedModule],
})
export class ScytaleModule {}
