import {
  EN_ALPHABET_FREQUENCY,
  ALPHABET,
} from "../../constants/language.constants";

export const ITERATIONS = 10000;

export const NAME_CIPHER = "Monoalphabetic Substitution Cipher";
export const TYPE_CIPHER = "Hill-Climbing attack on";

export const EQUATION_L1_DIST_2gram = "\\sum_{i=A}^{i=Z} \\sum_{j=A}^{j=Z} | {m_{i,j}} - {r_{i,j}} |";

export const EXAMPLE_RND_ALPHABET = [
  "v",
  "b",
  "d",
  "e",
  "f",
  "c",
  "g",
  "h",
  "j",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "i",
  "r",
  "z",
  "s",
  "k",
  "t",
  "u",
  "x",
  "w",
  "y",
  "a",
];

export const MESSAGE =
  "theenglishwikipediawasthefirstwikipediaeditionandhasremainedthelargestithaspioneeredmanyi" +
  "deasasconventionspoliciesorfeatureswhichwerelateradoptedbywikipediaeditionsinsomeoftheother" +
  "languagestheseideasincludefeaturedarticlestheneutralpointofviewpolicynavigationtemplatesthesor" +
  "tingofshortstubarticlesintosubcategoriesdisputeresolutionmechanismssuchasmediationandarbitration" +
  "andweeklycollaborationstheenglishwikipediahasadoptedfeaturesfromwikipediasinotherlanguagesthesefeatures" +
  "includeverifiedrevisionsfromthegermanwikipediadewikiandtownpopulationlookuptemplatesfromthedutchwikipedianlwikial" +
  "thoughtheenglishwikipediastoresimagesandaudiofilesaswellastextfilesmanyoftheimageshavebeenmovedtowikimediacommonswiththe" +
  "samenameaspassedthroughfileshowevertheenglishwikipediaalsohasfairuseimagesandaudiovideofileswithcopyrightrestrictionsmostof" +
  "whicharenotallowedoncommonsmanyofthemostactiveparticipantsinthewikimediafoundationandthedevelopersofthemediawikisoftwarethat" +
  "powerswikipediaareenglishusers";

export const CHART_OPTIONS_COMPARE_BIGRAMS = {
  chart: {
    type: "column",
    scrollablePlotArea: {
      minWidth: 700,
      scrollPositionX: 1,
    },
  },
  title: {
    text: "Compare bigrams frequency with language data values",
  },
  // subtitle: {
  //     text: 'Source: WorldClimate.com'
  // },
  xAxis: {
    categories: [],
    crosshair: true,
  },
  yAxis: {
    min: 0,
    title: {
      text: "Frequency of bigrams",
    },
  },
  tooltip: {
    pointFormat: "Population {point.y:.1f} %</b>",
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0,
    },
  },
  series: [
    {
      name: "Frequencies of bigrams for decrypted text",
      data: [],
    },
    {
      name: "Reference frequencies for language",
      data: [],
    },
  ],
};

export const CHART_OPTIONS_ITER_SCORE = {
  chart: {
    type: "column",
    scrollablePlotArea: {
      minWidth: 700,
      scrollPositionX: 1,
    },
  },
  title: {
    text: "",
  },
  // subtitle: {
  //     text: 'Source: WorldClimate.com'
  // },
  xAxis: {
    categories: [],
    crosshair: true,
    title: {
      text: "Iterations",
    },
  },
  yAxis: {
    min: 0,
    title: {
      text: "Score/Match rate",
    },
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat:
      '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.3f} </b></td></tr>',
    footerFormat: "</table>",
    shared: true,
    useHTML: true,
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0,
    },
  },
  series: [
    {
      name: "Sum",
      data: [],
    },
    {
      name: "Match rate",
      data: [],
    },
  ],
};

export const CHART_OPTIONS_MATCH_RATE_INCREASE = {
  chart: {
    type: 'column',
    scrollablePlotArea: {
      minWidth: 700,
      scrollPositionX: 1
    }
  },
  title: {
    text: ''
  },
  // subtitle: {
  //     text: 'Source: WorldClimate.com'
  // },
  xAxis: {
    categories: [],
    crosshair: true,
    title: {
      text: 'Iterations'
    }
  },
  yAxis: {
    title: {
      text: 'Increase/Decrease of Match Rate'
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">Iteration: {point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
},
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [   {
    name: 'Match rate',
    data: []
  }
 ]
};

export const TOP_GAP = 110;
export const SIDE_MENU = [
  {
    title: "Input setup",
    active: true,
    id: "inputs",
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: "Hill-Climbing attack description",
    active: true,
    id: "findKey",
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: "Hill-Climbing setup",
    active: true,
    id: "attack",
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: "The best key candidate",
    active: true,
    id: "result",
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: "Comparison of score and match rate",
    active: true,
    id: "compareBigrams",
    bottomPosition: 0,
    topPosition: 0,
  },
  // {
  //   title: "Match rate change details",
  //   active: true,
  //   id: "increaseDecMatchRate",
  //   bottomPosition: 0,
  //   topPosition: 0,
  // },



];
