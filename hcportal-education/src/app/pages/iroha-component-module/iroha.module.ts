import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { IrohaComponent } from './iroha-component/iroha.component';
import { IrohaService } from './iroha.service';


const irohaRoutes: Routes = [
    { path: '', component: IrohaComponent },
  ];
  
@NgModule({
  declarations: [
      IrohaComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(irohaRoutes),
    SharedModule
  ],
  providers: [IrohaService]

})
export class IrohaModule { }
  