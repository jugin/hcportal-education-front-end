import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import {
  FormBuilder,
  UntypedFormGroup,
  Validators,
  AbstractControl,
  UntypedFormControl,
} from "@angular/forms";
import { MatButtonToggleChange } from "@angular/material/button-toggle";
import { Subscription } from "rxjs";
import { IrohaService } from "../iroha.service";
import {
  NAME_CIPHER,
  TYPE_CIPHER,
  EQUATION,
  ALPHABETJP,
  TOP_GAP,
  SIDE_MENU,
} from "../iroha.constant";
import { data, fn } from "jquery";
import {HeaderService} from "../../../components/header/header.service";


@Component({
  selector: 'app-iroha',
  styleUrls: ["../../../app.component.css",'./iroha.component.css'],
  templateUrl: './iroha.component.html',


})
export class IrohaComponent implements OnInit, OnDestroy {


  constructor(headerService: HeaderService) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next(NAME_CIPHER);
    headerService.cipherType.next(TYPE_CIPHER);
  }

  sideMenu = SIDE_MENU;
  topGap = TOP_GAP;

  private subscribeIrohaMessage: Subscription;
  irohaInputsForm: UntypedFormGroup;
  irohakey= "そとかあめ";
  finalText= "";
  steps:any[]=[];
  stepshort:any[]=[];
  encryptedText = "";
  decryptedText="";
  currentTableData: any[] = [];

  endec=true;




  alphabetJP=ALPHABETJP;
  equation = EQUATION;



  toggleTable() {
    this.currentTableData = this.currentTableData === this.steps ? this.stepshort : this.steps;
  }

  createIrohaKanaArray(){
    const irohaKana=[];
    for (let i = 0; i < this.alphabetJP.length; i++) {
        irohaKana[i] = this.alphabetJP[i].split("");
    }
    return irohaKana;
}

  irohaEncrypt(){
    this.endec=true;
    this.currentTableData = [];
    this.decryptedText="";
    this.encryptedText="";
    const irohaOrder=this.createIrohaKanaArray();
    console.log(irohaOrder.length);
    for (var i = 0; i < this.irohakey.length; i++ ){
      this.encryptedText+= this.irohaReturnPosition(this.irohakey[i],irohaOrder);
      this.steps.push({input: String(this.irohakey[i]),output: String(this.encryptedText[i])+""+this.encryptedText[i+1]});
    }

  this.stepshort=this.steps.slice(0,3);
  }

  irohaDecrypt() {
    this.endec=false;
    this.currentTableData = [];
    this.decryptedText="";
    this.encryptedText="";
    const irohaOrder = this.createIrohaKanaArray();
    for (let i = 0, j = 0; i < this.irohakey.length; i += 2, j++) {
      var row = parseInt(this.irohakey[i]) - 1;
      var col = parseInt(this.irohakey[i + 1]) - 1;
      this.decryptedText += irohaOrder[row][col];
      this.steps.push({ input: String(this.decryptedText[j]), output: String(this.irohakey[i]) + "" + this.irohakey[i + 1] });
    }
    this.stepshort = this.steps.slice(0, 3);
  }


  irohaReturnPosition(tmp, alphabet){
    for (var i = 0; i < alphabet.length; i++){
      for (var j = 0; j < alphabet[i].length; j++){
        if (tmp === alphabet[i][j]){

          return [String(i+1) + String(j+1)];
        }
      }
    }
    return [String(-1) + String(-1)];
  }

  ngOnInit(): void {
    this.irohaInputsForm = new UntypedFormGroup({
      irohakey: new UntypedFormControl(this.irohakey, [
        Validators.required,
      ]),
    })
    this.subscribeIrohaMessage = this.irohaInputsForm.controls.irohakey.valueChanges.subscribe(
      (newKey) => {
        this.irohakey = newKey;
      })
  }

  ngOnDestroy(): void {
    this.subscribeIrohaMessage.unsubscribe();
  }

}
