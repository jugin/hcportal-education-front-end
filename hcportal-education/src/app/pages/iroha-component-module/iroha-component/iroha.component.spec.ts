import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IrohaComponent } from './iroha.component';

describe('IrohaComponent', () => {
  let component: IrohaComponent;
  let fixture: ComponentFixture<IrohaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IrohaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IrohaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
