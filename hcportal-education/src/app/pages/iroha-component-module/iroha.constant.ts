export const NAME_CIPHER = `I-RO-HA`;
export const TYPE_CIPHER = 'Demonstration of';

const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
export const ALPHABETJP = [
  'いろはにほへと',
  'ちりぬるをわか',
  'よたれそつねな',
  'らむうゐのおく',
  'やまけふこえて',
  'あさきゆめみし',
  'ゑひもせすん'
];

export const EQUATION = "P = 1 / 48";


export const TOP_GAP = 110;
export const SIDE_MENU = [
  {
    title: "I-RO-HA Cipher",
    active: true,
    id: "intro",
    bottomPosition: 0,
    topPosition: 0

  },
  {
    title: "Input setup",
    active: true,
    id: "inputs",
    bottomPosition: 0,
    topPosition: 0

  },
  {
    title: "Guide to encryption and decryption",
    active: true,
    id: "description",
    bottomPosition: 0,
    topPosition: 0

  },
];
