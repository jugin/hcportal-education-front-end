import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Jn25Component } from './jn25.component';

describe('Jn25Component', () => {
  let component: Jn25Component;
  let fixture: ComponentFixture<Jn25Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Jn25Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Jn25Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
