class Trie {
    map:{[key:string]:Trie} = {};
    isWord:boolean = false;
    code:number = 0;
    additive:number = 0;
    
	constructor() {
    }

    public insert(word: string, code: number, additive: number): void {
        this.add(word, 0, code, additive, this)
    }

    public search(word: string): { isWord: boolean, code: number, additive: number, remaining: string } {
        return this.find(word, 0, this)
    }

    private add(word: string, index: number, code: number, additive: number, subtrie: Trie): void {
  	  	if(index == word.length) {
    	  	subtrie.isWord = true;
            subtrie.code = code;
            subtrie.additive = additive;
    	  	return; 
        }
  		if(!subtrie.map[word.charAt(index)]){
    	  	subtrie.map[word.charAt(index)] = new Trie()
  		}
        return this.add(word, index + 1, code, additive, subtrie.map[word.charAt(index)])
    }

    private find(word: string, index: number, subtrie: Trie): { isWord: boolean, code: number, additive: number, remaining: string } {
  		  if(subtrie.map[word[index]]){
    		  return this.find(word ,index + 1, subtrie.map[word.charAt(index)])
  		  }
          return { isWord: subtrie.isWord, code: subtrie.code, additive: subtrie.additive, remaining: word.substring(index) } 
	}
}


function encode(plainText: string, codeMap: Trie): { error: boolean, cipherText: string, remaining: string } {
	var tmp = plainText
	var output = ""

	var error = false
	while (tmp != "" && !error) {
		var res = codeMap.search(tmp);
		error = !res.isWord
		if (!error) {
			tmp = res.remaining;
			output += (res.code + res.additive % 100000)
		}
	}
	
	return { error: error, cipherText: output, remaining: tmp }
}