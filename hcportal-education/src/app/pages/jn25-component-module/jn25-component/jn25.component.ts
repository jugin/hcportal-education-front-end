import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import {
  FormBuilder,
  UntypedFormGroup,
  Validators,
  AbstractControl,
  UntypedFormControl,
} from "@angular/forms";
import { MatButtonToggleChange } from "@angular/material/button-toggle";
import { Subscription } from "rxjs";
import { Jn25Service } from "../jn25.service";
import {
  NAME_CIPHER,
  TYPE_CIPHER,
  EQUATION,
  TABLE_DATA,
  COLUMNS_SCHEMA,
  TOP_GAP,
  SIDE_MENU,
  TABLE_DATAEN,
} from "../jn25.constant";
import { data } from "jquery";
import {HeaderService} from "../../../components/header/header.service";

class Trie {
  map:{[key:string]:Trie} = {};
  isWord:boolean = false;
  code:number = 0;
  additive:number = 0;



  public insert(word: string, code: number, additive: number): void {
      this.add(word, 0, code, additive, this)
  }

  public search(word: string): { isWord: boolean, code: number, additive: number, remaining: string } {
      return this.find(word, 0, this)
  }

  private add(word: string, index: number, code: number, additive: number, subtrie: Trie): void {
      if(index == word.length) {
        subtrie.isWord = true;
          subtrie.code = code;
          subtrie.additive = additive;
        return;
      }
    if(!subtrie.map[word.charAt(index)]){
        subtrie.map[word.charAt(index)] = new Trie()
    }
      return this.add(word, index + 1, code, additive, subtrie.map[word.charAt(index)])
  }

  private find(word: string, index: number, subtrie: Trie): { isWord: boolean, code: number, additive: number, remaining: string } {
      if(subtrie.map[word[index]]){
        return this.find(word ,index + 1, subtrie.map[word.charAt(index)])
      }
        return { isWord: subtrie.isWord, code: subtrie.code, additive: subtrie.additive, remaining: word.substring(index) }
}
}

function encode(plainText: string, codeMap: Trie): { error: boolean, cipherText: string, remaining: string, steps: any[], stepshort: any[] } {
  var tmp = plainText
  var output = ""
  var steps: any[] = []
  var stepshort: any[] = []

  var error = false

  var processedCharsCount = 0;
  while (tmp != "" && !error) {
    var res = codeMap.search(tmp);
    error = !res.isWord
    if (!error) {
      tmp = res.remaining;
      output += (String(((Number(res.code) + Number(res.additive)) % 100000)))
      var plainTextPosition = plainText.substring(processedCharsCount, plainText.length - tmp.length);

      steps.push({step1: String(plainTextPosition) +" -> " , step2: String(res.code)+" + "+ String(res.additive)+" = ", step3: String(Number(res.code) + Number(res.additive))+" modulo 100 000 =" ,result: (String(((Number(res.code) + Number(res.additive)) % 100000)))});
      processedCharsCount += plainTextPosition.length;
    }
  }
  stepshort=steps;


  return { error: error, cipherText: output, remaining: tmp, steps:steps, stepshort: stepshort.slice(0, 3)}
}



@Component({
  selector: 'app-jn25',
  styleUrls: ["../../../app.component.css",'./jn25.component.css'],
  templateUrl: './jn25.component.html',


})
export class Jn25Component implements OnInit, OnDestroy {
  constructor(headerService: HeaderService) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next(NAME_CIPHER);
    headerService.cipherType.next(TYPE_CIPHER);
  }
  private subscribeJn25Message: Subscription;
  jn25InputsForm: UntypedFormGroup;
  jn25input= "部隊到着1730";


  //TABLEDATA
  dataSource: any = TABLE_DATA;



  currentTableData: any[] = [];

  //COLUMNNAME
  displayedColumns: string[] = COLUMNS_SCHEMA.map(col => col.key);
  columnsSchema: any = COLUMNS_SCHEMA;


  // var for sideNavbar
  sideMenu = SIDE_MENU;
  topGap = TOP_GAP;
  equation = EQUATION;


  steps: any[];
  stepshort: any[];



  encryptedText = "";
  decryptedText="";

  //BOOLEAN
  language=false;
  endec=true; //true for encrypt




  ngOnInit(): void {
    this.jn25InputsForm = new UntypedFormGroup({
      jn25input: new UntypedFormControl(this.jn25input, [
        Validators.required,
      ]),

    })
    this.subscribeJn25Message = this.jn25InputsForm.controls.jn25input.valueChanges.subscribe(
      (newKey) => {
        this.jn25input = newKey;
      }),
    this.currentTableData = this.steps;
  }

  toggleTable() {
    this.currentTableData = this.currentTableData === this.steps ? this.stepshort : this.steps;
  }

  switchLanguage(){
    //JP
    if (this.language){
      this.language=!this.language;
      this.dataSource=TABLE_DATA;
      this.jn25input="部隊到着1730";

    }
    //EN
    else{
      this.language=!this.language;
      this.dataSource=TABLE_DATAEN;
      this.jn25input="CONVOYARRIVAL1730"
    }
      // Update the form control value
    this.jn25InputsForm.controls.jn25input.setValue(this.jn25input);
  }

  addRow() {
    const newRow = {id: this.dataSource[this.dataSource.length-1].id + 1, "key": "", "value": 0, "addittive": 0, isEdit: true}
    this.dataSource = [...this.dataSource, newRow];
  }

  removeRow(id: number) {
    this.dataSource = this.dataSource.filter((u) => u.id !== id);
  }




  encryptJN25(){
    this.endec=true;
    //LOWERCASE INPUT
    var lowerCase = this.jn25input.toLowerCase();
    //CLEAR OUTPUT
    this.encryptedText = "";
    this.decryptedText="";

    //SORT TABLE
    var codeMap = new Trie();

    for (var j = 0; j < this.dataSource.length; j++){
      codeMap.insert(this.dataSource[j].key.toLowerCase(), this.dataSource[j].value, this.dataSource[j].addittive);

    }

    console.log(codeMap);

    this.encryptedText = encode(lowerCase, codeMap).cipherText;



    const result = encode(lowerCase, codeMap);
    this.encryptedText = result.cipherText;
    this.steps = result.steps;
    this.stepshort = result.stepshort;



  }

  decryptJN25(){
    this.endec=false;
    this.steps = [];
    this.stepshort = [];

    this.encryptedText = "";
    this.decryptedText="";
    this.dataSource = this.dataSource.sort((a, b) => (a.key.length < b.key.length ? -1 : (a.key.length > b.key.length ? 1 : (a.key < b.key ? -1 : 1))));
    var tmpInput = String(this.jn25input);

    for (var j = 0; j < this.dataSource.length; j++) {
      // Subtract the additive from the value and then replace the encrypted value with the original key
      const encryptedValue = String(((Number(this.dataSource[j].value) + Number(this.dataSource[j].addittive)) % 100000));

      while (tmpInput.includes(encryptedValue)){

        tmpInput=tmpInput.replace(encryptedValue,this.dataSource[j].key);
        this.steps.push({
          step1: encryptedValue,
          step2: encryptedValue + " - " + String(this.dataSource[j].addittive),
          step3:  String(Number(encryptedValue)-this.dataSource[j].addittive) + " modulo 100 000" + " = ",
          result: this.dataSource[j].key});



          /*step1: encryptedValue + " - " + String(this.dataSource[j].addittive),
          step2: String(Number(encryptedValue)-this.dataSource[j].addittive) + " - " + String(this.dataSource[j].value),
          step3:  String(Number(encryptedValue)-this.dataSource[j].value) + " modulo 100 000" + " = ", result: this.dataSource[j].key});*/
      }

      //tmpInput = tmpInput.replaceAll(String(((Number(this.dataSource[j].value) + Number(this.dataSource[j].addittive)) % 100000)), this.dataSource[j].key);

      this.stepshort = this.steps.slice(0,3);
      console.log(this.stepshort);


    }
    this.decryptedText += tmpInput;

  }

  generateNumber(){
    for (var i = 0; i < this.dataSource.length; i++ ){

      this.dataSource[i].value = Math.round(
        (Math.random() * 89999) / 3) * 3 + 10000;
    }
  }

  generateAddittive(){
    for (var i = 0; i < this.dataSource.length; i++ ){

      this.dataSource[i].addittive = Math.round(
        (Math.random() * 89999) / 3) * 3 + 10000;
    }
  }

  ngOnDestroy(): void {
    this.subscribeJn25Message.unsubscribe();
  };
}
