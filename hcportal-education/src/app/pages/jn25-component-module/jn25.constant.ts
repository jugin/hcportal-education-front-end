export const NAME_CIPHER = `JN-25`;
export const TYPE_CIPHER = 'Demonstration of';


export const TABLE_DATAEN = [

  {id: 1, "key": "1", "value": 88750, "addittive": 30649},
  {id: 2, "key": "2", "value": 48313, "addittive": 71116},
  {id: 3, "key": "3", "value": 29341, "addittive": 56545},
  {id: 4, "key": "4", "value": 36784, "addittive": 78769},
  {id: 5, "key": "5", "value": 42772, "addittive": 16657},
  {id: 6, "key": "6", "value": 82990, "addittive": 62962},
  {id: 7, "key": "7", "value": 55192, "addittive": 94846},
  {id: 8, "key": "8", "value": 62338, "addittive": 79102},
  {id: 1, "key": "9", "value": 52369, "addittive": 67291},
  {id: 2, "key": "0", "value": 99553, "addittive": 68110},
  {id: 3, "key": "CONVOY", "value": 64393, "addittive": 94630},
  {id: 3, "key": "ARRIVAL", "value": 20230, "addittive": 50038},

];

export const TABLE_DATA = [
  {id: 1, "key": "1", "value": 88750, "addittive": 30649},
  {id: 2, "key": "2", "value": 48313, "addittive": 71116},
  {id: 3, "key": "3", "value": 29341, "addittive": 56545},
  {id: 4, "key": "4", "value": 36784, "addittive": 78769},
  {id: 5, "key": "5", "value": 42772, "addittive": 16657},
  {id: 6, "key": "6", "value": 82990, "addittive": 62962},
  {id: 7, "key": "7", "value": 55192, "addittive": 94846},
  {id: 8, "key": "8", "value": 62338, "addittive": 79102},
  {id: 1, "key": "9", "value": 52369, "addittive": 67291},
  {id: 2, "key": "0", "value": 99553, "addittive": 68110},

  {id: 3, "key": "部隊", "value": 64393, "addittive": 94630},
  {id: 4, "key": "到着", "value": 86833, "addittive": 74044},
  {id: 5, "key": "今日", "value": 20230, "addittive": 50038},
  {id: 6, "key": "行き", "value": 95596, "addittive": 12064},
  {id: 7, "key": "船団", "value": 45001, "addittive": 67618},
  {id: 8, "key": "ひ", "value": 39106, "addittive": 52123},

];


export const OUTPUT_TABLE = [
  {"substitution":"a", "step": 5000, "result": 0
  }
];

export const COLUMNS_SCHEMA = [
  {
      key: "key",
      type: "text",
      label: "Plaintext character"
  },
  {
      key: "value",
      type: "number",
      label: "Number value"
  },
  {
      key: "addittive",
      type: "number",
      label: "Added superencipher"
  },
  {
    key: "isEdit",
    type: "isEdit",
    label: ""
  }
];

export const EQUATION = "P = (1/N) * (1/M)";

export const TOP_GAP = 110;
export const SIDE_MENU = [
  {
    title: "Introduction",
    active: true,
    id: "intro",
    bottomPosition: 0,
    topPosition: 0

  },
  {
    title: "Input setup",
    active: true,
    id: "inputs",
    bottomPosition: 0,
    topPosition: 0

  },
  {
    title: "Step-by-step description of the attack",
    active: true,
    id: "description",
    bottomPosition: 0,
    topPosition: 0

  }
];
