import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Jn25Component } from './jn25-component/jn25.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { Jn25Service } from './jn25.service';



const jn25Routes: Routes = [
    { path: '', component: Jn25Component },
  ];
  
@NgModule({
  declarations: [
      Jn25Component
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(jn25Routes),
    SharedModule
  ],
  providers: [Jn25Service]

})
export class Jn25Module { }
  