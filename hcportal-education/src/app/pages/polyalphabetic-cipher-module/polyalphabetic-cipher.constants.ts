export const NAME_CIPHER = "Vigenere Cipher";
export const TYPE_CIPHER = "Friedman test and brute-force attack on";

export const COLUMNS_REFFREQ_LANGUAGE: string[] = ["name", "value"];

export const MESSAGE =
  "theenglishwikipediawasthefirstwikipediaeditionandhasremainedthelargestithaspioneeredmanyi" +
  "deasasconventionspoliciesorfeatureswhichwerelateradoptedbywikipediaeditionsinsomeoftheother" +
  "languagestheseideasincludefeaturedarticlestheneutralpointofviewpolicynavigationtemplatesthesor" +
  "tingofshortstubarticlesintosubcategoriesdisputeresolutionmechanismssuchasmediationandarbitration" +
  "andweeklycollaborationstheenglishwikipediahasadoptedfeaturesfromwikipediasinotherlanguagesthesefeatures" +
  "includeverifiedrevisionsfromthegermanwikipediadewikiandtownpopulationlookuptemplatesfromthedutchwikipedianlwikial" +
  "thoughtheenglishwikipediastoresimagesandaudiofilesaswellastextfilesmanyoftheimageshavebeenmovedtowikimediacommonswiththe" +
  "samenameaspassedthroughfileshowevertheenglishwikipediaalsohasfairuseimagesandaudiovideofileswithcopyrightrestrictionsmostof" +
  "whicharenotallowedoncommonsmanyofthemostactiveparticipantsinthewikimediafoundationandthedevelopersofthemediawikisoftwarethat" +
  "powerswikipediaareenglishusers";

export const EQUATION_VIGENERE = "y_i = x_i + k_{i \\bmod r} \\bmod 26";
export const EQUATION_IC1 = "\\sum_{i=A}^{i=Z}\\frac {n_i (n_i - 1)} {N (N - 1)}";
export const EQUATION_IC2 = "\\sum_{i=A}^{i=Z} {n_i}^2";
export const EQUATION_L1_DIST = "\\sum_{i=A}^{i=Z} | {m_i} - {r_i} |";

export const TOP_GAP = 110;
export const SIDE_MENU = [
  {
    title: "Input setup",
    active: true,
    id: "inputs",
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: "Friedman test",
    active: true,
    id: "alghorithm",
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: "Dividing the cipher text into cosets",
    active: true,
    id: "findkey",
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: "Description of the attack",
    active: true,
    id: "attack",
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: "Checking the result",
    active: true,
    id: "allcombinations",
    bottomPosition: 0,
    topPosition: 0,
  },

];
