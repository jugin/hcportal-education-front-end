import { NgModule } from '@angular/core';
import { A11yModule } from '@angular/cdk/a11y';

import { CommonModule } from '@angular/common';
import { AnagramMethodComponent } from './anagram-method-component/anagram-method.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AnagramColorboxesComponent } from './anagram-method-component/anagram-colorboxes-component/anagram-colorboxes.component';

const anagramRoutes: Routes = [{ path: '', component: AnagramMethodComponent }];

@NgModule({
  exports: [A11yModule],
  declarations: [AnagramMethodComponent, AnagramColorboxesComponent],
  imports: [CommonModule, RouterModule.forChild(anagramRoutes), SharedModule],
})
export class AnagramMethodModule {}
