import { EvaluationLength, EvaluationMethod } from "../../models/anagram.model";

export const NAME_CIPHER = 'Anagram method';
export const TYPE_CIPHER = 'Demonstration of';
export const INITIAL_MESSAGES = [
  // 'avnipnrecasettudolrlteegmathnnuepdcrecdtitloatenpyysdmioscsiasjhoflelieps',
  // 'rrltiidesymyyargiagsacatennsknhaastnshampypscomnteeednaheaecaethttkwattrx',
  // 'aeetolmeepncmhcpsynrmrsfraehueeinyeubdeyayoaeeraftirdioprltqrngdgnsdonrlm',
  'sisylanatpyrcnideplehscinortcelednasretupmoclatigidfotnempolevedehtsatsuj',
  'dekcarcneebsahthguacneebrevensahohwrelliklaireslaturbaybtnesegassemdedoca',
  'namirperyltneuqerfyehtdnasmelborplarudecorpesehtfoynamesingocerdidynamreg',
];
// export const PLAINTEXTS = [
//   'justasthedevelopmentofdigitalcomputersandelectronicshelpedincryptanalysis',
//   'thearmyhadcryptanalystsattacktheirownenigmasandintheseattacksgermanysexpe',
//   'germanydidrecognisemanyoftheseproceduralproblemsandtheyfrequentlyrepriman',
// ];
export const INITIAL_HINT = 'justas';
export const TOP_GAP = 120;
export const SIDE_MENU = [
  {
    title: 'Input setup',
    active: true,
    id: 'inputs',
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: 'Movable table',
    active: true,
    id: 'example',
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: 'Checking the results',
    active: true,
    id: 'results',
    bottomPosition: 0,
    topPosition: 0,
  },
];

export const MIN_HINT_LENGTH = 3;
export const MAX_HINT_LENGTH = 10;

export const MIN_MESSAGE_LENGTH = 10;
export const MAX_MESSAGE_LENGTH = 500;
export const INITIAL_MIN_DICT_WORD_LENGTH = 3;

export const EVALUATION_METHODS = [
  { value: EvaluationMethod.DICT5000, label: 'Dictionary of 5000 words' },
  { value: EvaluationMethod.DICT1000, label: 'Dictionary of 1000 words' },
  { value: EvaluationMethod.DICT10000, label: 'Dictionary of 10000 words' },
  { value: EvaluationMethod.DICT25000, label: 'Dictionary of 25000 words' },
  { value: EvaluationMethod.DICT50000, label: 'Dictionary of 50000 words' },
  { value: EvaluationMethod.DICT100000, label: 'Dictionary of 100000 words' },
  { value: EvaluationMethod.BIGRAMS, label: 'Matching bigrams' },
];

export const EVALUATION_LENGTHS = [
  { value: EvaluationLength.HINT_LENGTH, label: 'Hint length' },
  { value: EvaluationLength.DOUBLE_HINT_LENGTH, label: 'Double hint length' },
  { value: EvaluationLength.WHOLE, label: 'Whole Texts' },
];
