import { Component } from '@angular/core';
import {
  FormArray,
  FormControl,
  FormGroup,
  UntypedFormArray,
  Validators,
} from '@angular/forms';
import { Subject, Subscription } from 'rxjs';

import { HeaderService } from '../../../components/header/header.service';
import {
  NAME_CIPHER,
  TYPE_CIPHER,
  SIDE_MENU,
  TOP_GAP,
  MIN_MESSAGE_LENGTH,
  MAX_MESSAGE_LENGTH,
  MIN_HINT_LENGTH,
  MAX_HINT_LENGTH,
  INITIAL_MESSAGES,
  INITIAL_HINT,
  EVALUATION_METHODS,
  EVALUATION_LENGTHS,
  INITIAL_MIN_DICT_WORD_LENGTH,
} from '../anagram-method.constants';
import {
  TAnagramColumns,
  TAnagramColumn,
  TPositionedColors,
  TTopColumns,
  TSelectValue,
  EvaluationMethod,
  EvaluationLength,
  WorkerResult,
} from '../../../models/anagram.model';
import { ColorboxClickEvent } from './anagram-colorboxes-component/anagram-colorboxes.component';
import { EQUATION_L1_DIST_2gram } from '../../monoalphabetic-cipher-module/monoalphabetic-cipher.constant';
import { MatTableDataSource } from '@angular/material/table';
import { SortTable, Ordering } from '../../../models/common.model';

interface AnagramForm {
  hint: FormControl<string>;
  messageArray: FormArray<FormControl<string>>;
}

interface AttackAnagramForm {
  evaluationMethod: FormControl<EvaluationMethod>;
  evaluationLength: FormControl<EvaluationLength>;
  minDictWordLength: FormControl<number>;
}

@Component({
  selector: 'app-anagram-method',
  templateUrl: './anagram-method.component.html',
  styleUrls: ['../../../app.component.css', './anagram-method.component.css'],
})
export class AnagramMethodComponent {
  private subscribedHint: Subscription;
  private messages: string[] = INITIAL_MESSAGES;
  private minDictWordLength = INITIAL_MIN_DICT_WORD_LENGTH;
  private subscribedMessageArray: Subscription;
  private subscribedEvaluationMethod: Subscription;
  private subscribedEvaluationLength: Subscription;
  private subscribedMinDictWordLength: Subscription;
  private webWorker: Worker;

  equation_score = EQUATION_L1_DIST_2gram;
  evaluationMethods = EVALUATION_METHODS;
  evaluationLengths = EVALUATION_LENGTHS;
  sideMenu = SIDE_MENU;
  topGap = TOP_GAP;

  hint: string = INITIAL_HINT;
  cipherInputsForm: FormGroup<AnagramForm>;
  attackInputsForm: FormGroup<AttackAnagramForm>;
  minMessageLength = MIN_MESSAGE_LENGTH;
  maxMessageLength = MAX_MESSAGE_LENGTH;
  minHintLength = MIN_HINT_LENGTH;
  maxHintLength = MAX_HINT_LENGTH;
  evaluationMethod = EVALUATION_METHODS[0].value;
  evaluationLength = EVALUATION_LENGTHS[0].value;

  messageLength: number | null = this.messages[0]?.length || 0;

  columnsBestResults = ['score', 'anagrams', 'select'];
  stickyColumns = {
    stickyStart: 'score',
    stickyEnd: '',
    stickyHeader: true,
  };
  dataSourceBestResults = new MatTableDataSource<any>();
  dataSourceBestResultsReady = new Subject<boolean>();
  sortBestResults: SortTable = {
    sortByColumn: 'score',
    order: Ordering.desc,
  } as SortTable;

  tableReady = false;
  movableColumns: TAnagramColumns = [];
  positionedColors: TPositionedColors = [];
  topTableReady = false;
  topMovableColumns: TAnagramColumns = [];

  get messageArray() {
    return this.cipherInputsForm.get('messageArray') as FormArray;
  }

  constructor(headerService: HeaderService) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next(NAME_CIPHER);
    headerService.cipherType.next(TYPE_CIPHER);
  }

  ngOnInit(): void {
    this.cipherInputsForm = new FormGroup<AnagramForm>({
      hint: new FormControl(this.hint, [
        Validators.pattern('^[a-zA-Z]+$'),
        Validators.minLength(this.minHintLength),
        Validators.maxLength(this.maxHintLength),
      ]),

      messageArray: new UntypedFormArray(
        this.messages.map((message, i) =>
          i === 0
            ? new FormControl(message || '', [
                Validators.required,
                Validators.pattern('^[a-zA-Z]+$'),
                Validators.minLength(this.minMessageLength),
                Validators.maxLength(this.maxMessageLength),
              ])
            : new FormControl(message || '', [
                Validators.required,
                Validators.pattern('^[a-zA-Z]+$'),
                Validators.minLength(this.messageLength),
                Validators.maxLength(this.messageLength),
              ])
        )
      ),
    });

    this.attackInputsForm = new FormGroup<AttackAnagramForm>({
      evaluationMethod: new FormControl(this.evaluationMethod, [
        Validators.required,
      ]),
      evaluationLength: new FormControl(this.evaluationLength, [
        Validators.required,
      ]),
      minDictWordLength: new FormControl(this.minDictWordLength, [
        Validators.min(0),
      ]),
    });

    this.subscribedEvaluationMethod =
      this.attackInputsForm.controls.evaluationMethod.valueChanges.subscribe(
        (newEvaluationMethod) => {
          this.evaluationMethod = newEvaluationMethod;
        }
      );

    this.subscribedEvaluationLength =
      this.attackInputsForm.controls.evaluationLength.valueChanges.subscribe(
        (newEvaluationLength) => {
          this.evaluationLength = newEvaluationLength;
        }
      );

    this.subscribedMinDictWordLength =
      this.attackInputsForm.controls.minDictWordLength.valueChanges.subscribe(
        (newMinDictWordLength) => {
          this.minDictWordLength = newMinDictWordLength;
        }
      );

    this.subscribedHint =
      this.cipherInputsForm.controls.hint.valueChanges.subscribe((newHint) => {
        this.hint = newHint;
      });

    this.subscribedMessageArray =
      this.cipherInputsForm.controls.messageArray.valueChanges.subscribe(
        (newMessages) => {
          this.messages = newMessages;
          if (this.messageLength === newMessages[0].length) {
            return;
          }

          this.messageLength = newMessages[0].length;
          this.cipherInputsForm.controls.messageArray.controls.forEach(
            (control, index) => {
              if (index === 0) {
                return;
              }
              control.setValidators([
                Validators.required,
                Validators.pattern('^[a-zA-Z]+$'),
                Validators.minLength(this.messageLength),
                Validators.maxLength(this.messageLength),
              ]);
              control.updateValueAndValidity();
            }
          );
        }
      );

    
    this.createMovableTable();
    this.runAttack();
  }

  private initiateWebWorker() {
    if (typeof Worker !== 'undefined') {
      this.webWorker = new Worker(
        new URL('./anagram-method-webworker.worker', import.meta.url),
        {
          type: `module`,
        }
      );

      this.webWorker.onmessage = (event) => {
        const result = event.data as WorkerResult;
        if (!result) {
          this.topTableReady = true;
          return;
        }

        if (!result.topColumns) {
          return;
        }

        this.dataSourceBestResults.data= result.topColumns
        this.topMovableColumns = result.topColumns[0]?.columns;
        this.topTableReady = true;
        this.dataSourceBestResultsReady.next(true);
      };
    }
  }

  // https://stackoverflow.com/questions/10014271/generate-random-color-distinguishable-to-humans
  // generate random color based on hsl color values with golden angle approximation
  private selectColor(number) {
    const hue = number * 137.508; // use golden angle approximation
    return `hsl(${hue},50%,75%)`;
  }

  public createMovableTable() {
    if (!this.cipherInputsForm.controls.messageArray.valid) {
      return;
    }
    this.tableReady = false;
    this.topTableReady = false;
    const columns = this.messages[0]?.length || 0;
    const rows = this.messages.length;

    const resultStrips: TAnagramColumns = [
      ...Array(columns),
    ].map<TAnagramColumn>((_, index) => ({
      pos: index + 1,
      rows: [],
    }));

    let resultPositionedColors: TPositionedColors = [];

    for (let row = 0; row < rows; row++) {
      const message = this.messages[row].split('');
      const uniqueColor = this.selectColor(Math.floor(Math.random() * 10));
      resultPositionedColors = [
        ...resultPositionedColors,
        { rowPosition: row + 1, color: uniqueColor },
      ];
      for (let column = 0; column < columns; column++) {
        const char = message[column];
        const showColor = message.filter((item) => item === char).length === 1;
        const color = showColor ? uniqueColor : undefined;
        resultStrips[column].rows.push({ char, color, showColor });
      }
    }

    this.positionedColors = resultPositionedColors;
    this.movableColumns = resultStrips;
    this.tableReady = true;
    this.runAttack();
  }

  public runAttack() {
    this.dataSourceBestResultsReady.next(false);
    this.topTableReady = false;
    if (this.webWorker) {
      this.webWorker.terminate();
    }
    this.initiateWebWorker();
    this.webWorker.postMessage({
      movableColumns: this.movableColumns,
      hint: this.hint,
      minDictWordLength: this.minDictWordLength,
      evaluationMethod: this.evaluationMethod,
      evaluationLength: this.evaluationLength,
    });
  }

  public terminateAttack() {
    if (this.webWorker) {
      this.webWorker.terminate();
    }
    this.topTableReady = true;
  }

  public removeField(index: number) {
    if (!index) {
      return;
    }

    this.cipherInputsForm.controls.messageArray.controls.splice(index);
  }

  public addField() {
    this.cipherInputsForm.controls.messageArray.push(
      new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-zA-Z]+$'),
        Validators.minLength(this.messageLength),
        Validators.maxLength(this.messageLength),
      ])
    );
  }

  public onRowClick(row: number) {
    this.topMovableColumns = [...this.dataSourceBestResults.data[row].columns];
  }

  public onColorboxClick({ value, index: rowIndex }: ColorboxClickEvent) {
    this.movableColumns.forEach((column) => {
      column.rows[rowIndex] = { ...column.rows[rowIndex], showColor: value };
    });
  }

  ngOnDestroy() {
    this.subscribedHint.unsubscribe();
    this.subscribedMessageArray.unsubscribe();
    this.subscribedEvaluationMethod.unsubscribe();
    this.subscribedEvaluationLength.unsubscribe();
    this.subscribedMinDictWordLength.unsubscribe();
    this.webWorker.terminate();
  }
}
