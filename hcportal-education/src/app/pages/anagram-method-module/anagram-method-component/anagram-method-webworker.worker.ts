/// <reference lib="webworker" />
import AnagramColumnsUtils from '../../../utils/anagram-columns-util';
import {
  TTopColumns,
  TAnagramWorkerArgs,
  TAnagramColumns,
  WorkerResult,
} from '../../../models/anagram.model';

function getRowContainsHint(args: TAnagramWorkerArgs) {
  const { movableColumns, hint } = args;
  const splitHint = hint.split('');

  const firstRow = movableColumns.map((column) => column.rows[0].char);
  const uniqueHintChars = [...new Set(splitHint)];
  const rowContainsHint = uniqueHintChars.reduce<boolean>(
    (acc, currentChar) => {
      if (!acc) {
        return acc;
      }

      const hintLen = splitHint.filter((c) => c === currentChar).length;
      const rowLen = firstRow.filter((c) => c === currentChar).length;
      return rowLen >= hintLen;
    },
    true
  );

  return rowContainsHint;
}

function getIndexGroupsFromHint(
  args: TAnagramWorkerArgs
): Map<string, Array<number>> {
  const { movableColumns, hint } = args;
  const splitHint = hint.split('');

  const map = new Map();

  for (let i = 0; i < splitHint.length; i++) {
    const char = splitHint[i];
    const indexes = [];
    for (let j = 0; j < movableColumns.length; j++) {
      const column = movableColumns[j];
      if (column.rows[0].char === char) {
        indexes.push(j);
      }
    }
    map.set(char, indexes);
  }

  return map;
}

function swapByPos(
  movableColumns: TAnagramColumns,
  index1: number,
  index2: number
): string {
  if (movableColumns[index1].isFixed || movableColumns[index2].isFixed) {
    return;
  }
  const item1 = movableColumns[index1];
  movableColumns[index1] = movableColumns[index2];
  movableColumns[index2] = item1;

  return item1.rows[0]?.char;
}

function guessColumnsFromHint(
  args: TAnagramWorkerArgs,
  indexToSwap: number,
  hintIndex: number,
  hintIndexGroups: Map<string, Array<number>>,
  topColumns: TTopColumns
) {
  const { movableColumns, evaluationMethod, hint } = args;

  const columnsCopy = JSON.parse(
    JSON.stringify(movableColumns)
  ) as TAnagramColumns;
  const hintIndexGroupsCopy = new Map(hintIndexGroups);
  const hintChar = hint[hintIndex];

  const hintIndexes = hintIndexGroupsCopy.get(hintChar);
  hintIndexes.splice(indexToSwap, 1);

  const swappedChar = swapByPos(columnsCopy, hintIndex, indexToSwap);

  // ukoncovacia podmienka
  if (hintIndex >= hint.length - 1) {
    const score = AnagramColumnsUtils.getScoreFromColumns({
      ...args,
      movableColumns: columnsCopy,
    });
    const anagrams: string[] = AnagramColumnsUtils.getRowTextsFromColumns(
      columnsCopy,
      hint.length
    );
    AnagramColumnsUtils.maybeInsertIntoTopColumns(
      score,
      columnsCopy,
      topColumns,
      anagrams,
      evaluationMethod
    );
    return;
  }

  const swappedIndexes = hintIndexGroupsCopy.get(swappedChar);
  for (let i = 0; i < swappedIndexes?.length; i++) {
    if (swappedIndexes[i] === hintIndex) {
      swappedIndexes[i] = indexToSwap;
    }
  }

  const nextHintChar = hint[hintIndex + 1];
  const nextHintIndexes = hintIndexGroupsCopy.get(nextHintChar);
  for (let i = 0; i < nextHintIndexes?.length; i++) {
    const newIndexToSwap = nextHintIndexes[i];
    guessColumnsFromHint(
      {
        ...args,
        movableColumns: columnsCopy,
      },
      newIndexToSwap,
      hintIndex + 1,
      hintIndexGroups,
      topColumns
    );
  }
}

function guessColumns(args: TAnagramWorkerArgs): WorkerResult {
  const { hint } = args;

  if (!hint || !getRowContainsHint(args)) {
    return null;
  }

  const hintIndexGroups = getIndexGroupsFromHint(args);
  postMessage({ indexGroups: hintIndexGroups });
  const topColumns: TTopColumns = [];
  const firstIndexes = hintIndexGroups.get(hint[0]);
  for (let i = 0; i < firstIndexes?.length; i++) {
    const indexToSwap = firstIndexes.splice(i, 1)[0];
    guessColumnsFromHint(args, indexToSwap, 0, hintIndexGroups, topColumns);
  }
  
  return { topColumns, indexGroups: hintIndexGroups, hint };
}

addEventListener('message', ({ data }) => {
  postMessage(guessColumns(data));
});
