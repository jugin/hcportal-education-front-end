import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { TPositionedColors } from '../../../../models/anagram.model';

export type ColorboxClickEvent = {
  value: boolean;
  index: number;
};

@Component({
  selector: 'app-anagram-colorboxes',
  templateUrl: './anagram-colorboxes.component.html',
  styleUrls: [
    '../../../../app.component.css',
    './anagram-colorboxes.component.css',
  ],
})
export class AnagramColorboxesComponent implements OnInit, OnChanges {
  @Input() positionedColors: TPositionedColors = [];

  @Output() onColorboxClick = new EventEmitter<ColorboxClickEvent>();

  selectedValues: boolean[];

  ngOnInit() {
    this.selectedValues = this.positionedColors.map(() => true);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.selectedValues = changes.positionedColors.currentValue.map(() => true);
  }

  public onColorboxClickLocal(index: number) {
    const newValue = !this.selectedValues[index];
    this.selectedValues[index] = newValue;
    this.onColorboxClick.emit({ value: newValue, index });
  }
}
