import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatternHelpComponentComponent } from './pattern-help-component.component';

describe('PatternHelpComponentComponent', () => {
  let component: PatternHelpComponentComponent;
  let fixture: ComponentFixture<PatternHelpComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatternHelpComponentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PatternHelpComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
