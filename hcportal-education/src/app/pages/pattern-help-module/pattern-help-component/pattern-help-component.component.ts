import { Component, OnInit, OnDestroy } from '@angular/core';
import {SIDE_MENU, TOP_GAP, TYPE_CIPHER, DEFAULT_INPUT, TEXT_MAX, TEXT_MIN} from '../pattern-help.constant';
import {} from '../pattern-help.service';
import {} from '../pattern-help.module';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { HeaderService } from "../../../components/header/header.service";

@Component({
  selector: 'app-pattern-help-component',
  templateUrl: './pattern-help-component.component.html',
  styleUrls: ["../../../app.component.css", './pattern-help-component.component.css']
})
export class PatternHelpComponentComponent implements OnInit, OnDestroy {

  private subscrMessage: Subscription;

  sideMenu = [];

  textMin = TEXT_MIN;
  textMax = TEXT_MAX;

  inputForm: UntypedFormGroup;

  input_string = DEFAULT_INPUT;
  input_string_arr:string[] = [];

  computationDone = false;

  list : any[];
  checkedList : any[];
  currentSelected : {};

  showDropDown = false;

  selected_methods = {};

  showSubst = false;
  showRepet = false;
  showFreqv = false;
  showDist = false;

  subst_answer = [];
  repet_answer = [];
  repet_answer_str = "";
  freqv_answer = [];
  dist_answer = [];
  dist_answer_str = "";

  constructor(
    headerService: HeaderService
  ){
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next("Pattern conversion methods");
    headerService.cipherType.next("Demonstration of");

    this.list = 
      [
        {name :'Substitution',checked : false},
        {name :'Repetition',checked : false},
        {name :'Frequency',checked : false},
        {name :'Distance',checked : false}
      ];
      this.checkedList = [];
      this.selected_methods = 
        {
          'Substitution':false,
          'Repetition':false,
          'Frequency':false,
          'Distance':false
        }
      this.input_string_arr = this.createArrOfLetters();
      this.computeForSubstitution();
      this.computeForRepetition();
      this.computeForFrequency();
      this.computeForDistance();
      this.updateSideMenu();
  }

  ngOnInit(): void {
    this.inputForm = new UntypedFormGroup({
      input_message: new UntypedFormControl(DEFAULT_INPUT, [
        Validators.required,
        Validators.minLength(TEXT_MIN),
        Validators.maxLength(TEXT_MAX),
      ]),
    });

    this.subscrMessage = this.inputForm.controls.input_message.valueChanges.subscribe(
      (newMessage) => {
        this.input_string = newMessage;
        this.input_string_arr = this.createArrOfLetters();
      }
    );
  }

/*SOURCE: https://stackblitz.com/edit/multiselect-checkbox-dropdown?file=src%2Fapp%2Fmulti-select-dropdown%2Fmulti-select-dropdown.component.html*/

  getSelectedValue(status:Boolean,value:string){
    this.selected_methods[value] = !this.selected_methods[value]
  }

  onConversionSubmit(){
    this.showSubst = this.selected_methods["Substitution"];
    this.showRepet = this.selected_methods["Repetition"];
    this.showFreqv = this.selected_methods["Frequency"];
    this.showDist = this.selected_methods["Distance"];
    
    this.updateSideMenu();
    
    if(this.showSubst){ this.computeForSubstitution(); }
    if(this.showRepet){ this.computeForRepetition(); }
    if(this.showFreqv){ this.computeForFrequency(); }
    if(this.showDist){ this.computeForDistance(); }
  }

  computeForSubstitution(){
    let result = [];
    let d = {};
    let i = 1;
    for(let l of this.input_string_arr){
      l = l.toLowerCase();
      if(d[l] == undefined){
        d[l] = i;
        i += 1;
      }
      result.push(d[l]);
    }
    this.subst_answer = result;
  }

  computeForRepetition(){
    let result = [];
    let d = {};
    let i = 1;
    let alreadyPushed = {};
    for(let l of this.input_string_arr){
      l = l.toLowerCase();
      if(d[l] == undefined){
        d[l] = [];
      }
      d[l].push(i);
      i += 1;
      if(alreadyPushed[l] == undefined && d[l].length > 1){
        alreadyPushed[l] = true;
        result.push(d[l]);
      }
    }
    this.repet_answer = result;
    this.repet_answer_str = "";
    let first = true;
    for(let p of result){
      if(!first){ this.repet_answer_str += ", "}
      let _first = true;
      for(let l of p){
        if(!_first){ this.repet_answer_str +="-" }
        this.repet_answer_str += l;
        _first = false;
      }
      first = false;
    }
  }

  computeForFrequency(){
    let result = [];
    let d = {};
    let i = 1;
    for(let l of this.input_string_arr){
      l = l.toLowerCase();
      if(d[l] == undefined){
        d[l] = 1;
      }
      else{
        d[l] = d[l] += 1;
      }
    }
    for(let l of this.input_string_arr){
      l = l.toLowerCase();
      result.push(d[l]);
    }
    this.freqv_answer = result;
  }

  computeForDistance(){
    let result = "";
    let d = {};
    let i = 1;
    for(let l of this.input_string_arr){
      l = l.toLowerCase();
      if(d[l] == undefined){
        d[l] = [];
      }
      d[l].push(i);
      i += 1;
    }
    let first = true;
    for(let l in d){
      l = l.toLowerCase();
      if(d[l].length > 1){
        if(!first){ result += ", " }
        let _first = true;
        for(let j = 0; j < d[l].length - 1; j+=1){
          if(!_first) {result += "-"}
          result += d[l][j+1] - d[l][j] -1;
          _first = false;
        }
        first = false;
      }
    }
    this.dist_answer_str = result;
  }

  createArrOfLetters(){
    let result = this.input_string.replaceAll(" ", "");
    return result.split('');
  }

  updateSideMenu(){
    this.sideMenu = [];
    this.sideMenu.push(
      {
        title: "String conversion to patterns",
        active: true,
        id: "string_conversion",
        bottomPosition: 0,
        topPosition: 0
      }
    );
    if(this.showSubst){
      this.sideMenu.push(
        {
          title: "Substitution method",
          active: true,
          id: "subst",
          bottomPosition: 0,
          topPosition: 0
        }
      );
    }
    if(this.showRepet){
      this.sideMenu.push(
        {
          title: "Repetition method",
          active: true,
          id: "repet",
          bottomPosition: 0,
          topPosition: 0
        } 
      );
    }
    if(this.showFreqv){
      this.sideMenu.push(
        {
          title: "Frequency method",
          active: true,
          id: "freqv",
          bottomPosition: 0,
          topPosition: 0
        } 
      );
    }
    if(this.showDist){
      this.sideMenu.push(
        {
          title: "Distance method",
          active: true,
          id: "dist",
          bottomPosition: 0,
          topPosition: 0
        } 
      );
    }
  }

  ngOnDestroy(): void {
    this.subscrMessage.unsubscribe();
  }
}
