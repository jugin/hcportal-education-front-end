export const TYPE_CIPHER = 'Patterns\' Relations';

export const TEXT_MIN = 1;
export const TEXT_MAX = 2000;

export const TOP_GAP = 110;
export const SIDE_MENU = [
    {
        title: "String conversion to patterns",
        active: true,
        id: "string_conversion",
        bottomPosition: 0,
        topPosition: 0
    },
    {
      title: "Substitution method",
      active: true,
      id: "subst",
      bottomPosition: 0,
      topPosition: 0
    },
    {
      title: "Repetition method",
      active: true,
      id: "repet",
      bottomPosition: 0,
      topPosition: 0
    } ,
    {
      title: "Frequency method",
      active: true,
      id: "freqv",
      bottomPosition: 0,
      topPosition: 0
    } 

];

export const DEFAULT_INPUT = "Be Brave"

export const BUTTON_TEXT = "Convert"