import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatternHelpComponentComponent} from './pattern-help-component/pattern-help-component.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { PatternHelpService } from './pattern-help.service';

const patternRoutes: Routes = [
  { path: '', component: PatternHelpComponentComponent },
];

@NgModule({
  declarations: [
    PatternHelpComponentComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(patternRoutes),
    SharedModule
  ],
  providers: [PatternHelpService]

})
export class PatternHelpModule { }
