import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MrRobotRot13 } from './mr-robot-rot13-component/mr-robot-rot13.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { MrRobotRot13Service } from './mr-robot-rot13.service';

const caesarRoutes: Routes = [
    { path: '', component: MrRobotRot13 },
];

@NgModule({
    declarations: [
        MrRobotRot13
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(caesarRoutes),
        SharedModule
    ],
    providers: [MrRobotRot13Service]

})
export class MrRobotRot13Module { }
