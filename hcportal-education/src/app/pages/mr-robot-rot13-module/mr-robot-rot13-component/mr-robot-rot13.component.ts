import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  UntypedFormGroup,
  Validators,
  UntypedFormControl,
} from "@angular/forms";

import CommonUtils from "../../../utils/common-utils";
import { MrRobotRot13Service } from "../mr-robot-rot13.service";
import {
  MESSAGE,
  NAME_CIPHER,
  TYPE_CIPHER,
  TOP_GAP,
  SIDE_MENU,
  MRROBOT,
} from "../mr-robot-rot13.constant";
import { Subscription } from "rxjs";
import { HeaderService } from "../../../components/header/header.service";

@Component({
  selector: "app-mr-robot-rot13",
  styleUrls: ['./mr-robot-rot13.component.css',"../../../app.component.css"],
  templateUrl: "./mr-robot-rot13.component.html",
})
export class MrRobotRot13 implements OnInit, OnDestroy {
  
  private key = 13;
  private subscrKey: Subscription;
  private message = MESSAGE;
  private subscrMessage: Subscription;
  mr_robot = MRROBOT;

  // var for sideNavbar
  sideMenu = SIDE_MENU;
  topGap = TOP_GAP;

  encryptedText = "";
  decryptedText = "";
  
  cipherInputsForm: UntypedFormGroup;

  constructor(
    private mrRobotRot13Service: MrRobotRot13Service,
    headerService: HeaderService
  ) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next(NAME_CIPHER);
    headerService.cipherType.next(TYPE_CIPHER);
  }

  ngOnInit(): void {

    this.cipherInputsForm = new UntypedFormGroup({
      key: new UntypedFormControl(this.key, [
        Validators.required,
        Validators.min(1),
        Validators.max(26),
      ]),
      message: new UntypedFormControl(MESSAGE, [
        Validators.required,
        Validators.pattern("^[a-zA-Z]+$"),
        Validators.minLength(1),
        Validators.maxLength(2000),
      ]),
    });

    this.subscrMessage = this.cipherInputsForm.controls.message.valueChanges.subscribe(
      (newMessage) => {
        this.message = newMessage;
      }
    );

    this.subscrKey = this.cipherInputsForm.controls.key.valueChanges.subscribe(
      (newKey) => {
        this.key = newKey;
      }
    );

    this.enDeCryptMessage();
  }

  public enDeCryptMessage() {
    
    const formatedMessage = CommonUtils.stripWhiteSpToLowerCase(this.message);
    this.encryptedText = this.mrRobotRot13Service.encrypt(
      formatedMessage,
      this.key
    );
    this.decryptedText = this.mrRobotRot13Service.decrypt(
      this.key,
      this.encryptedText
    );
    
  }

  ngOnDestroy() {
    this.subscrMessage.unsubscribe();
    this.subscrKey.unsubscribe();
  }
}
