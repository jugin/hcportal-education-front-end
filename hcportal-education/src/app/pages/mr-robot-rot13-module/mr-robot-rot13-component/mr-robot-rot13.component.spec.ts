import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MrRobotRot13 } from './mr-robot-rot13.component';

describe('MrRobotRot13Component', () => {
  let component: MrRobotRot13;
  let fixture: ComponentFixture<MrRobotRot13>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MrRobotRot13]
    })
      .compileComponents();

    fixture = TestBed.createComponent(MrRobotRot13);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
