import { A_ASCII, ALPHABET } from '../../constants/language.constants';

export class MrRobotRot13Service {

    encrypt(formatedMessage: string, key: number): string {
        let encryptedText = '';
        let encryptedText2 = '';
        
        for (const letter of formatedMessage) {
            const letterAscii = letter.charCodeAt(0);
            const encryptedLetter: number = ((letterAscii - A_ASCII + key + 26) % 26) + A_ASCII;
            encryptedText += String.fromCharCode(encryptedLetter);
        }

        for (let i = 0; i < encryptedText.length; i++) {
            const char = encryptedText[i].toLowerCase(); 
            if (char >= 'a' && char <= 'z') {
                const num = char.charCodeAt(0) - 'a'.charCodeAt(0) + 1;
                encryptedText2 += (num < 10 ? '0' + num : num.toString()) + ' ';
            }
        }

        return encryptedText2;
    }

    decrypt(inputKey: number, encryptedText: string): string {
        let decryptedText = '';
        let decryptedText2 = '';

        for (let i = 0; i < encryptedText.length; i += 3) { 
            const numStr = encryptedText.substring(i, i + 2); 
            const num = parseInt(numStr, 10);
            if (num >= 1 && num <= 26) {
                const char = String.fromCharCode(A_ASCII + num - 1).toLowerCase(); 
                decryptedText += char;
            }
        }

        for (const letter of decryptedText) {
            const letterAscii = letter.charCodeAt(0);
            const decryptedLetter = ((letterAscii - 'a'.charCodeAt(0) - inputKey + 26) % 26) + 'a'.charCodeAt(0);
            decryptedText2 += String.fromCharCode(decryptedLetter);
        }

        return decryptedText2;
    }

    public decryptForEveryKey(encryptedText: string): Array<string> {
        const decryptedTexts = [];
        for (let key = 1; key <= ALPHABET.length; key++) {
            decryptedTexts.push(
                this.decrypt(key, encryptedText)
            );
        }
        return decryptedTexts;
    }

}
