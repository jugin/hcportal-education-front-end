export const NAME_CIPHER = 'Cipher form the Mr. Robot TV show';
export const TYPE_CIPHER = 'Demonstration of a';
export const MRROBOT = "assets/img/mr_robot.PNG";

export const MESSAGE = 'theperrinpageswillhelpyoufindyourcallingbutdontbedupedcutdownthewoodstheybeerdos';

export const TOP_GAP = 110;
export const SIDE_MENU = [
    {
        title: "Description of the cipher",
        active: true,
        id: "description",
        bottomPosition: 0,
        topPosition: 0

    },
    {
        title: "Input setup",
        active: true,
        id: "inputs",
        bottomPosition: 0,
        topPosition: 0
    },
    {
        title: "Analysis",
        active: true,
        id: "analysis",
        bottomPosition: 0,
        topPosition: 0
    }
];
