/// <reference lib="webworker" />
import PermutationMovableColumnsUtils from '../../../utils/permutation-movable-columns-utils';
import {
  TPermutationMovableColumns,
  TPermutationWorkerArgs,
  TTopColumns,
} from '../../../models/permutation-table.model';

function swapColsByPos(
  movableColumns: TPermutationMovableColumns,
  index1: number,
  index2: number
) {
  const item1 = movableColumns[index1];
  movableColumns[index1] = movableColumns[index2];
  movableColumns[index2] = item1;
}

function swapRowsByPos(
  movableColumns: TPermutationMovableColumns,
  colIndex: number,
  index1: number,
  index2: number
) {
  const item1 = movableColumns[colIndex].rows[index1];
  movableColumns[colIndex].rows[index1] = movableColumns[colIndex].rows[index2];
  movableColumns[colIndex].rows[index2] = item1;
}

function permuteRows(
  movableColumns: TPermutationMovableColumns,
  colIndex: number,
  l: number,
  r: number,
  topColumns: TTopColumns,
  minDictWordLength?: number
) {
  // permutation of rows finished
  if (l == r) {
    const columnsCopy = JSON.parse(JSON.stringify(movableColumns));
    try {
      const decTextIteration =
        PermutationMovableColumnsUtils.getTextFromColumns(columnsCopy);

      const score =
        PermutationMovableColumnsUtils.getDictLongestOccuranceFromText(
          decTextIteration,
          minDictWordLength
        );

      PermutationMovableColumnsUtils.maybeInsertIntoTopColumns(
        score,
        columnsCopy,
        decTextIteration,
        topColumns
      );
    } 
    finally {
      const movableColumnsCopy = JSON.parse(JSON.stringify(columnsCopy));
      const nextColIndex = ++colIndex;

      if (nextColIndex > movableColumns.length - 1) {
        return;
      }
      permuteRows(
        movableColumnsCopy,
        nextColIndex,
        0,
        movableColumnsCopy[nextColIndex].rows.length - 1,
        topColumns,
        minDictWordLength
      );

      return;
    }
  } else {
    for (let i = l; i <= r; i++) {
      swapRowsByPos(movableColumns, colIndex, l, i);
      permuteRows(
        movableColumns,
        colIndex,
        l + 1,
        r,
        topColumns,
        minDictWordLength
      );
      swapRowsByPos(movableColumns, colIndex, l, i);
    }
  }
}

// https://www.geeksforgeeks.org/write-a-c-program-to-print-all-permutations-of-a-given-string/
// find find movableColumns.length! permutations
function permuteCols(
  movableColumns: TPermutationMovableColumns,
  l: number,
  r: number,
  topColumns: TTopColumns,
  minDictWordLength?: number
) {
  // permutation of columns finished
  if (l == r) {
    // permutate rows?
    permuteRows(
      movableColumns,
      0,
      0,
      movableColumns[0].rows.length - 1,
      topColumns,
      minDictWordLength
    );
  } else {
    for (let i = l; i <= r; i++) {
      swapColsByPos(movableColumns, l, i);
      permuteCols(movableColumns, l + 1, r, topColumns, minDictWordLength);
      swapColsByPos(movableColumns, l, i);
    }
  }
}

function permuteAllColumns(args: TPermutationWorkerArgs): TTopColumns {
  const topColumns: TTopColumns = [];
  const { minDictWordLength, movableColumns } = args;
  permuteCols(
    movableColumns,
    0,
    movableColumns.length - 1,
    topColumns,
    minDictWordLength
  );
  return topColumns;
}

addEventListener('message', ({ data }) => {
  postMessage(permuteAllColumns(data));
});
