import {
  CdkDragDrop,
  moveItemInArray,
  DragDropModule,
} from '@angular/cdk/drag-drop';
import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { TPermutationMovableColumns } from '../../../../models/permutation-table.model';

@Component({
  selector: 'app-permutation-movable-table',
  templateUrl: './permutation-movable-table.component.html',
  styleUrls: [
    '../../../../app.component.css',
    './permutation-movable-table.component.css',
  ],
})
export class PermutationMovableTable implements OnInit, OnChanges {
  @Input() movableColumns?: TPermutationMovableColumns;

  displayMovableColumns: TPermutationMovableColumns = [];
  dividerTopIndex: number;

  ngOnInit() {
    this.displayMovableColumns = [...this.movableColumns];
    this.updateDividerTop(this.displayMovableColumns);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.displayMovableColumns = [
      ...(changes.movableColumns?.currentValue || []),
    ];
    this.updateDividerTop(this.displayMovableColumns);
  }

  public updateDividerTop(columns: TPermutationMovableColumns) {
    let newDividerTop = 0;
    const firstColumn = columns[0];
    const rowLength = firstColumn?.rows.length;
    for (let i = 0; i < rowLength; i++) {
      if (firstColumn?.rows[i]) {
        break;
      }
      newDividerTop++;
    }
    this.dividerTopIndex = newDividerTop;
  }

  public dropRow(event: CdkDragDrop<any, any>, stripIndex: number) {
    moveItemInArray(
      this.displayMovableColumns[stripIndex].rows,
      event.previousIndex,
      event.currentIndex
    );
    this.updateDividerTop(this.displayMovableColumns);
  }

  public dropCol(event: CdkDragDrop<any, any>) {
    moveItemInArray(
      this.displayMovableColumns,
      event.previousIndex,
      event.currentIndex
    );
    this.updateDividerTop(this.displayMovableColumns);
  }
}
