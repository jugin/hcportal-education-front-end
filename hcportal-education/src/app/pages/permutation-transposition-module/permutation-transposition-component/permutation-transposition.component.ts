import { Component } from '@angular/core';
import {
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { Subject, Subscription } from 'rxjs';

import { HeaderService } from '../../../components/header/header.service';
import {
  NAME_CIPHER,
  TYPE_CIPHER,
  SIDE_MENU,
  TOP_GAP,
  INITIAL_MESSAGE,
  MIN_MESSAGE_LENGTH,
  MAX_MESSAGE_LENGTH,
  INITIAL_PERMUTATION,
  MAX_COLUMNS,
  MIN_COLUMNS,
  INITIAL_COLUMNS,
  INITIAL_MIN_DICT_WORD_LENGTH,
  EQUATION_SCORE,
  MAX_ATTACK_BLOCK_SIZE,
} from '../permutation-transposition.constants';
import ValidationUtils from '../../../utils/validation-utils';
import TranspositionUtils from '../../../utils/transposition-utils';
import {
  TPermutationMovableColumns,
  TPermutaionColumn,
  TTopColumns,
} from '../../../models/permutation-table.model';
import { MatTableDataSource } from '@angular/material/table';
import { Ordering, SortTable } from '../../../models/common.model';

@Component({
  selector: 'app-permutation-transposition',
  templateUrl: './permutation-transposition.component.html',
  styleUrls: [
    '../../../app.component.css',
    './permutation-transposition.component.css',
  ],
})
export class PermutationTranspositionComponent {
  private permutation = INITIAL_PERMUTATION;
  private message = INITIAL_MESSAGE;
  private shouldAddX = false;
  private blockSize = INITIAL_COLUMNS;
  private minDictWordLength = INITIAL_MIN_DICT_WORD_LENGTH;
  private subscribedPermutation: Subscription;
  private subscribedMessage: Subscription;
  private subscribedShouldAddX: Subscription;
  private subscribedBlockSize: Subscription;
  private subscribedMinDictWordLength: Subscription;
  private webWorker: Worker;

  sideMenu = SIDE_MENU;
  topGap = TOP_GAP;

  tableReady = false;
  decryptedTableReady = false;
  tableReadySubject = new Subject<boolean>();
  tableData;
  blockSizeToDisplay: string[];
  minBlockSize = MIN_COLUMNS;
  maxBlockSize = MAX_COLUMNS;
  maxAttackBlockSize = MAX_ATTACK_BLOCK_SIZE;
  encryptedMessage: string;
  minMessageLength = MIN_MESSAGE_LENGTH;
  maxMessageLength = MAX_MESSAGE_LENGTH;
  cipherInputsForm: UntypedFormGroup;
  movableColumns: TPermutationMovableColumns = [];
  bestMovableColumns: {
    columns: TPermutationMovableColumns;
    permutation: string;
  };
  bestDecText: string;

  columnsBestResults = ['score', 'permutation','decryptedText', 'select'];
  dataSourceBestResults = new MatTableDataSource<any>();
  dataSourceBestResultsReadySubject = new Subject<boolean>();
  dataSourceBestResultsReady = false;
  stickyColumns = {
    stickyStart: 'score',
    stickyEnd: '',
    stickyHeader: true,
  };
  sortBestResults: SortTable = {
    sortByColumn: 'score',
    order: Ordering.desc,
  } as SortTable;

  completeColumnsLength = 0;
  incompleteColumnsLength = 0;
  // ciphers to add to add to current column from previous one
  fillerRows = 0;

  equationScore = EQUATION_SCORE;

  constructor(headerService: HeaderService) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next(NAME_CIPHER);
    headerService.cipherType.next(TYPE_CIPHER);
  }

  get maxBlockSizeError() {
    return this.cipherInputsForm
      .get('blockSize')
      .hasError('permutationAttackMaxBlockSize');
  }

  ngOnInit(): void {
    this.cipherInputsForm = new UntypedFormGroup({
      permutation: new UntypedFormControl(this.permutation, [
        Validators.required,
        ValidationUtils.permutationBaseValidator(),
        ValidationUtils.permutationSequenceValidator(),
      ]),
      message: new UntypedFormControl(this.message, [
        Validators.required,
        Validators.pattern('^[a-zA-Z]+$'),
        Validators.minLength(this.minMessageLength),
        Validators.maxLength(this.maxMessageLength),
      ]),
      shouldAddX: new UntypedFormControl(this.shouldAddX),

      blockSize: new UntypedFormControl(this.blockSize, [
        Validators.required,
        Validators.min(this.minBlockSize),
        Validators.max(this.maxBlockSize),
        ValidationUtils.permutationAttackMaxBlockSize(this.maxAttackBlockSize),
      ]),
      minDictWordLength: new UntypedFormControl(this.minDictWordLength, [
        Validators.min(0),
      ]),
    });

    this.subscribedPermutation =
      this.cipherInputsForm.controls.permutation.valueChanges.subscribe(
        (newPermutation) => {
          this.permutation = newPermutation;
        }
      );

    this.subscribedMessage =
      this.cipherInputsForm.controls.message.valueChanges.subscribe(
        (newMessage) => {
          this.message = newMessage;
        }
      );

    this.subscribedShouldAddX =
      this.cipherInputsForm.controls.shouldAddX.valueChanges.subscribe(
        (newShoudAddX) => {
          this.shouldAddX = newShoudAddX;
        }
      );

    this.subscribedBlockSize =
      this.cipherInputsForm.controls.blockSize.valueChanges.subscribe(
        (newBlockSize) => {
          this.blockSize = newBlockSize;
        }
      );

    this.subscribedMinDictWordLength =
      this.cipherInputsForm.controls.minDictWordLength.valueChanges.subscribe(
        (newMinDictWordLength) => {
          this.minDictWordLength = newMinDictWordLength;
        }
      );

    this.createTableAndEncrypt();
    this.runAttack();
  }

  private initiateWebWorker() {
    if (typeof Worker !== 'undefined') {
      this.webWorker = new Worker(
        new URL(
          './permutation-transposition-webworker.worker',
          import.meta.url
        ),
        {
          type: `module`,
        }
      );

      this.webWorker.onmessage = (event) => {
        const result = event.data as TTopColumns;
        this.dataSourceBestResults.data = result;
        this.bestMovableColumns = { ...result[0] };
        this.bestDecText = result[0].decryptedText;
        this.dataSourceBestResultsReady = true;
        this.dataSourceBestResultsReadySubject.next(true);
      };
    }
  }

  public createTableAndEncrypt() {
    this.createTable();
    this.encrypt();
    this.createMovableTableAndFillers();
  }

  public createMovableTableAndFillers() {
    this.decryptedTableReady = false;
    this.createMovableTable();
    this.populateStripsFiller();
    this.decryptedTableReady = true;
  }

  public createTable() {
    this.tableReadySubject.next(false);
    this.tableReady = false;
    const splitPermutation = TranspositionUtils.transformPermutation(
      this.permutation
    );
    const columns = splitPermutation.length;
    const rows = Math.ceil(this.message.length / columns);

    const resultTableData = [];
    const fillerElement = this.shouldAddX ? 'x' : null;

    // populate table by rows
    for (let i = 0; i < rows; i++) {
      // populate first column of table with row numbers
      resultTableData[i] = { ...resultTableData[0], ['0']: `${i + 1}` };

      // for (let j = 0; j < columns; j++) {
      splitPermutation.forEach((permutation, tableColIndex) => {
        const j = Number(permutation) - 1;
        return (resultTableData[i] = {
          ...resultTableData[i],
          // retrieve char from plaintext calculated by number of columns * current row + column offset
          [`${j + 1}`]: this.message[columns * i + j]
            ? this.message[columns * i + j].toLowerCase()
            : fillerElement,
        });
      });
    }

    this.blockSizeToDisplay = splitPermutation;
    this.tableData = resultTableData;
    this.tableReadySubject.next(true);
    this.tableReady = true;
  }

  private encrypt() {
    this.encryptedMessage = TranspositionUtils.encrypt(
      this.tableData,
      this.blockSizeToDisplay
    );
  }

  public createMovableTable() {
    const rows = Math.ceil(this.encryptedMessage.length / this.blockSize);
    this.completeColumnsLength =
      this.encryptedMessage.length % this.blockSize || this.blockSize;
    this.incompleteColumnsLength = this.blockSize - this.completeColumnsLength;

    this.fillerRows = Math.min(
      this.completeColumnsLength,
      this.incompleteColumnsLength
    );

    const resultStrips: TPermutationMovableColumns = [
      ...Array(this.blockSize),
    ].map<TPermutaionColumn>((_, index) => ({ pos: index + 1, rows: [[]] }));

    // create shallow copy of ZT to later modify
    let encryptedMessageCopy = this.encryptedMessage.slice();

    for (let column = 0; column < this.blockSize; column++) {
      const rowsInterations =
        column < this.completeColumnsLength ? rows : rows - 1;

      for (let row = 0; row < rowsInterations; row++) {
        const foundChar = encryptedMessageCopy[0];
        if (!foundChar) {
          continue;
        }

        encryptedMessageCopy = encryptedMessageCopy.slice(
          1,
          encryptedMessageCopy.length
        );
        resultStrips[column].rows[0].push(foundChar.toUpperCase());
      }
    }

    // add fillerRows from previous column
    resultStrips.forEach((_, stripIndex) => {
      // don't add to first column
      if (stripIndex < 1) {
        return;
      }
      const fillerCiphers = TranspositionUtils.getFillerCiphers(
        resultStrips,
        stripIndex,
        this.fillerRows
      );

      resultStrips[stripIndex].rows[0].unshift(...fillerCiphers);
    });
    this.movableColumns = resultStrips;
  }

  private populateStripsFiller = () => {
    // operation expects string rows to be on 0-th index
    const longestStripLength = this.movableColumns.reduce(
      (previousLength, strip): number =>
        strip.rows[0]?.length > previousLength
          ? strip.rows[0]?.length
          : previousLength,
      0
    );

    this.movableColumns.forEach((strip, stripIndex) => {
      const diff = longestStripLength - strip.rows[0].length;
      const stripFillersLength =
        diff > 0 ? this.fillerRows + diff : this.fillerRows;
      if (stripIndex === 0) {
        strip.rows.unshift(...Array(diff > 0 ? diff : 0).fill(''));
        return;
      }

      strip.rows = [...strip.rows, ...Array(stripFillersLength).fill('')];
    });
  };

  public runAttack() {
    // don't run on error
    if (!!this.maxBlockSizeError) {
      return;
    }

    this.dataSourceBestResultsReadySubject.next(false);
    this.dataSourceBestResultsReady = false;
    if (this.webWorker) {
      this.webWorker.terminate();
    }
    this.initiateWebWorker();
    this.webWorker.postMessage({
      movableColumns: [...this.movableColumns],
      minDictWordLength: this.minDictWordLength,
    });
  }

  public terminateAttack() {
    if (this.webWorker) {
      this.webWorker.terminate();
    }
    this.dataSourceBestResultsReady = true;
  }

  public onRowClick(row: number) {
    this.bestMovableColumns = { ...this.dataSourceBestResults.data[row] };
    this.bestDecText = this.dataSourceBestResults.data[row].decryptedText;
  }

  ngOnDestroy() {
    this.subscribedPermutation.unsubscribe();
    this.subscribedMessage.unsubscribe();
    this.subscribedShouldAddX.unsubscribe();
    this.subscribedBlockSize.unsubscribe();
    this.subscribedMinDictWordLength.unsubscribe();
    this.webWorker.terminate();
  }
}
