import { NgModule } from '@angular/core';
import { A11yModule } from '@angular/cdk/a11y';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CdkTableModule } from '@angular/cdk/table';
import { MatLegacyTableModule as MatTableModule } from '@angular/material/legacy-table';
import { CommonModule } from '@angular/common';
import { PermutationTranspositionComponent } from './permutation-transposition-component/permutation-transposition.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { PermutationMovableTable } from './permutation-transposition-component/permutation-movable-table/permutation-movable-table.component';

const permutationTranspositionRoutes: Routes = [
  { path: '', component: PermutationTranspositionComponent },
];

@NgModule({
  exports: [A11yModule, CdkTableModule, DragDropModule, MatTableModule],
  declarations: [PermutationTranspositionComponent, PermutationMovableTable],
  imports: [
    CommonModule,
    RouterModule.forChild(permutationTranspositionRoutes),
    SharedModule,
  ],
})
export class PermutationTranspositionModule {}
