export const NAME_CIPHER =
  'Simple columnar transposition';
export const TYPE_CIPHER = 'Attack based on strips on';
export const INITIAL_PERMUTATION = '(5, 4, 3, 2, 1)';
export const MIN_MESSAGE_LENGTH = 5;
export const MAX_MESSAGE_LENGTH = 500;
export const INITIAL_MESSAGE =
  'inacolumnarciphertheoriginalmessageisarrangedinarectanglefromlefttorightandtoptobottomnextakeyischosenandusedtoassignanumbertoeachcolumnintherectangletodeterminetheorderofrearrangement';
export const TOP_GAP = 120;
export const SIDE_MENU = [
  {
    title: 'Input setup',
    active: true,
    id: 'inputs',
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: 'Encrypted message',
    active: true,
    id: 'encryptedMessage',
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: 'Movable strips',
    active: true,
    id: 'example',
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: 'Description of the attack',
    active: true,
    id: 'attack',
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: 'Run the attack',
    active: true,
    id: 'autoAttack',
    bottomPosition: 0,
    topPosition: 0,
  },
];
export const INITIAL_COLUMNS = 5;
export const MIN_COLUMNS = 1;
export const MAX_COLUMNS = 100;
export const INITIAL_MIN_DICT_WORD_LENGTH = 3;
export const MAX_ATTACK_BLOCK_SIZE = 6;

export const EQUATION_SCORE = `\\sum_{i=1}^{i=5000}{
\\begin{cases}
   |w_i| &\\text{} w_i \\subseteq plaintext  \\\\
   0 &\\text{} w_i \\nsubseteq plaintext
\\end{cases}
}`;
