import matplotlib.pyplot as plt
import numpy as np

frequencies2 = None

def f1():
    f = open("./src/app./pages./dictionary-module/dictionary_5000.txt", "r")
    d = {}
    sum_words = 0
    for item in f.readlines():
        item = item.split('\n')[0]
        for c in item:
            sum_words += 1
            if d.get(c) == None:
                d[c] = 1
            else:
                d[c] += 1

    x = []
    d = dict(sorted(d.items()))
    for symbol,freq in d.items():
        x.append(round(freq / sum_words * 100, 1))
        #print(symbol + ": " + str(round(freq / sum_words * 100, 1) ))
    f.close()

    # Extract letters and frequencies
    letters = list(d.keys())
    frequencies1 = x

    # Set the width of the bars and the positions of the groups
    bar_width = 0.35  # Adjust this value based on your preference
    bar_positions_set1 = np.arange(len(letters))
    bar_positions_set2 = bar_positions_set1 + bar_width

    # Create a bar plot
    bars = plt.bar(bar_positions_set1, frequencies1 ,width=bar_width)
    bars = plt.bar(bar_positions_set2, frequencies2 ,width=bar_width)

    # Add labels and title
    plt.xlabel('Symboly')
    plt.ylabel('Frekvencie (%)')
    plt.title('Frekvenčná analyza článku a slovníka')

    # Add accuracy annotations on top of each bar
    #for bar, frequency in zip(bars, frequencies):
    #    plt.text(bar.get_x() + bar.get_width() / 2 - 0.15, bar.get_height() + 0.2, f'{frequency}%', ha='center')


    # Adjust the x-axis ticks and spacing
    plt.xticks((bar_positions_set1 + bar_positions_set2) / 2, letters)
    plt.legend(['Slovník 5000 slov ENG','Náhodný článok ENG'])

    # Display the plot
    plt.show()

def f2():

    f = open("./src/app./pages./dictionary-module/dictionary_5000.txt", "r")
    d = {}
    sum_words = 0
    for item in f.readlines():
        item = item.split('\n')[0]
        for c in item:
            sum_words += 1
            if d.get(c) == None:
                d[c] = 1
            else:
                d[c] += 1

    x = []
    d = dict(sorted(d.items()))
    for symbol,freq in d.items():
        x.append(round(freq / sum_words * 100, 1))
        #print(symbol + ": " + str(round(freq / sum_words * 100, 1) ))
    f.close()

    # Extract letters and frequencies
    letters = list(d.keys())
    frequencies = x

    # Create a bar plot
    plt.bar(letters, frequencies)

    # Add labels and title
    plt.xlabel('Symboly')
    plt.ylabel('Frekvencie (%)')
    plt.title('ENG Slovník 5000 slov')

    # Display the plot
    plt.show()

def f3():

    f = "Australia quietened the wild support from the strong home crowd by dismissing their previously unbeaten opponents for before Travis Head s sensational century meant they romped to victory with seven overs to spare After their bowlers expertly took advantage of a slow pitch  Australia were themselves reduced to as India hit back in an electric new ball spell But Head and Marnus Labuschagne calmly weathered the storm with a stand of as Indian hope drifted away from the worlds largest cricket stadium Head was caught for from balls with just two runs needed but Glenn Maxwell flogged the winning runs a ball later while Labuschagne ended not out from The superb victory means Australia extend their record as the most successful side in over World Cup history and now sit four titles clear of the rest of the pack It also caps a six month period in which they beat India to win the World Test Championship and retained the Ashes in England India meanwhile were left crestfallen as their bid for a first white ball title since an achievement which looked unstoppable as they made rampant progress through the semi final and group stage fell at the final hurdle"
    f = f.lower()
    d = {}
    sum_words = 0
    for item in f:
        item = item.split(' ')[0]
        for c in item:
            sum_words += 1
            if d.get(c) == None:
                d[c] = 1
            else:
                d[c] += 1

    x = []
    d = dict(sorted(d.items()))
    for symbol,freq in d.items():
        x.append(round(freq / sum_words * 100, 1))
        #print(symbol + ": " + str(round(freq / sum_words * 100, 1) ))
  

    # Extract letters and frequencies
    letters = list(d.keys())
    frequencies = x
    # Create a bar plot
    #plt.bar(letters, frequencies)

    # Add labels and title
    #plt.xlabel('Symboly')
    #plt.ylabel('Frekvencie (%)')
    #plt.title('ENG Náhodný Text')

    # Display the plot
    #plt.show()
    x.append(0.1)
    return frequencies

frequencies2 = f3()
f1()