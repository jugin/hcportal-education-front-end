
import { Box } from './Box';
import { BOX_COLORS, BOX_DEFAULT_WIDTH, CONST_WORD_SEPAR_REPLACED } from './dictionary.constants';

export class DictionaryService {
  global_is_separator:Boolean;
  global_word_separator = {}

  global_results_names:{};
  global_pattern_dict:{};
  global_input_string:string;
  global_input_string_patterns:[];
  global_candidate_list:[];             // arr[index] = 'x'+ i ; input="ABAC" arr[0]='x1', arr[1]='x2', arr[2]='x1', arr[3]='x3'
  global_candidate_list_sorted:{} = {}; // dict[pattern] = ['result1', 'result2', ...];
  solved_mask:string = "";
  global_results_f3:[];
  global_mask_best_score:number = 0
  global_minimum_score_on_candidate:number = 0;

  global_single_word_dictionary:{}
  global_multi_word_dictionary:{}
  global_boxes_names:Box[]

  global_forbidden_symbols:{} = {}

  global_max_window_size = 12;
  global_min_window_size = 5;

  private splitInputToChars(encrypted_message:string, char_separator:string):[string[],{}]{
    let splits = encrypted_message.split(char_separator)
    let d = {};
    let index = 1
    for(let character of splits){
      if(d[character] == undefined ){
        if(character == CONST_WORD_SEPAR_REPLACED){ d[character] = CONST_WORD_SEPAR_REPLACED; }
        else{ d[character] = index; index = index + 1 }
      }
    }
    return [splits, d];
  }

  public initializeTable(encrypted_message:string, char_separator:string, is_separator){
    let boxes_names:Box[] = []
    let isFirst = false;
    let i = 1;
    let index = 0;
    let d = {}
    
    for(let message_split of encrypted_message.split(CONST_WORD_SEPAR_REPLACED)){
      
      let [splits,_] = this.splitInputToChars(message_split, char_separator)

      for(let character of splits){
        if(d[character] == undefined ){
          if(character == CONST_WORD_SEPAR_REPLACED){ d[character] = CONST_WORD_SEPAR_REPLACED; }
          else{ d[character] = i; i = i + 1 }
        }
      }
      
      this.global_is_separator = is_separator
      for(let character of splits){
        if(character == ''){continue}
        if(isFirst){
          boxes_names.push(this.initiateBox(CONST_WORD_SEPAR_REPLACED, d[CONST_WORD_SEPAR_REPLACED], index, BOX_COLORS.UNCLICABLE_TEXT));
          index += 1;
        }
        boxes_names.push(this.initiateBox(character, d[character], index))
        index += 1;
        isFirst = false;
      }
      isFirst = true; 
    }
    return boxes_names;
  }

  public init(input_boxes:Box[], char_separator:string){
    this.global_boxes_names = input_boxes;
    this.global_candidate_list_sorted = {}
    this.global_candidate_list = []
    let tmp_input_string = ""
    for(let b of input_boxes){
      if(b.wasSolved){
        tmp_input_string += b.solvedName;
      }
      else{
        tmp_input_string += b.displayName;
      }
      if(char_separator != null){
        tmp_input_string += char_separator;
      }
    }
    this.global_input_string = tmp_input_string;
    
    let [splits,d] = this.splitInputToChars(this.global_input_string, char_separator)
    this.initializeMask(input_boxes);

    if(this.global_input_string.length < 250){this.global_minimum_score_on_candidate = 0}
    else if(this.global_input_string.length < 500){this.global_minimum_score_on_candidate = 2}
    else if(this.global_input_string.length < 1000){this.global_minimum_score_on_candidate = 4}
    else{
      this.global_minimum_score_on_candidate = 6
    }
    
    let i = 1;
    let input_pattern_list:any = [];
    
    for(let substr of splits){
      if( d[substr] == undefined) {
        if(substr == CONST_WORD_SEPAR_REPLACED){
          d[substr] = "";
        }
        else{
          d[substr] = "x"+i;
          i+=1;
        }
      }
    }
    for(let substr of splits){
      input_pattern_list.push(d[substr])
    }
    for(let i = 0; i < splits.length; i+=1){
      let substr = d[splits[i]]
      if(this.global_candidate_list_sorted[substr] == undefined){
        this.global_candidate_list_sorted[substr] = [];
      }
      this.global_candidate_list_sorted[substr].push(i)
    }
    this.global_candidate_list = input_pattern_list;
  }

  private initializeMask(boxes_names){
    this.solved_mask = "";
    let mask:string = "";
    for( let b of boxes_names){
      if(b.displayName == CONST_WORD_SEPAR_REPLACED){ mask+=CONST_WORD_SEPAR_REPLACED }
      else{ mask+='*'}
    }
    this.solved_mask = mask;
  }

  public updateDictionaries(d1:{}, d2:{}){
    this.global_single_word_dictionary = d1;
    this.global_multi_word_dictionary = d2;
  }

  public checkReplacedCharacters(input_boxes:Box[]){
    for(let i = 0; i < input_boxes.length; i+=1){
      let box = input_boxes[i];
      if(box.wasSolved){
        this.solved_mask = this.replaceCharAt(this.solved_mask, i, box.displayName)
        if(this.global_forbidden_symbols[box.displayName] == undefined){
          this.global_forbidden_symbols[box.displayName] = 0;
        }
      }
    }
  }

  public findSingleWord(selected_boxes_names:Box[]): [Box[],Box[]]{
    // an array of boxes to be printed as results on right side
    let sorted_by_freq:Box[] = [];
    let sorted_by_name:Box[] = []; 
    let d = {};
    let i = 1;

    // will be of pattern x1x2x2x3 for word 'abbc'
    let transformed = "";

    // keeps all already solved boxes for future's usage in this function
    let transformed_solved = "";

    for(let box of selected_boxes_names){
      if(!(box.currentName in d)) {
        d[box.currentName] = "x"+i;
        i+=1;
      }
    }
    for(let box of selected_boxes_names){
      transformed += d[box.currentName];
      if(box.wasSolved){
        transformed_solved += box.solvedName;
      }
    }
    i = 1;
    for(let [key, value] of Object.entries(this.global_single_word_dictionary)){
      // if you found a word with pattern (value) same as word we have marked (transformed), then..
      if(value === transformed){ 
        // if we have solved any boxes before and marked them for another usage, then...
        if(transformed_solved !== ""){
          let index = 0;
          let str_index = 0;
          let flag = false;
          let c = transformed_solved.split('')[str_index]
          for(let _box of selected_boxes_names){
              if( _box.wasSolved ){
                if(key.charAt(index) !== c){
                  flag = true
                  break;
                }
                else{
                  str_index += 1;
                  c = transformed_solved.split('')[str_index]
                }
              }
              index += 1;
          }
          if(flag){ continue }
        }
        let box = this.initiateBox(key, value, this.global_boxes_names.length + i, BOX_COLORS.NEW_RESULT)
        let box2 = this.initiateBox(key, value, this.global_boxes_names.length + i, BOX_COLORS.NEW_RESULT)
          
        sorted_by_freq.push(box);
        sorted_by_name.push(box2);
        i += 1;
      }
    }
    // result
    if(sorted_by_freq.length < 1){ return [[],[]] }

    let index = sorted_by_name[0].id;
    sorted_by_name.sort((a, b) => (a.currentName > b.currentName) ? 1 : -1);
    for(let box of sorted_by_name){
      box.id = index;
      index += 1;
    }
    return [sorted_by_freq, sorted_by_name]
  }

  public findTwoWords_v3(selected_boxes_names:Box[], remove_space:Boolean=false):[Box[],Box[]]{
    let d_message = {}
    let index_message = 1
    let input = [];
    let sorted_by_freq:Box[] = []
    let sorted_by_name:Box[] = []
    for(let b of selected_boxes_names){
      input.push(b.currentName);
    }

    for(let j = 0; j < input.length; j+=1){
      if(d_message[input[j]] == undefined){
        d_message[input[j]] = index_message;
        index_message += 1;
      }
    }

    let message_pattern = ""
    let message_pattern_solved = []
    let d_repetitions = {}
    let index = 1

    let check_sum = 0

    for(let j = 0; j < input.length; j+=1){
      message_pattern += "x"+d_message[input[j]];
      if(selected_boxes_names[j].wasSolved){
        message_pattern_solved.push([j,selected_boxes_names[j].currentName]); 
      }
      if(d_repetitions[input[j]] == undefined){
        d_repetitions[input[j]] = []
      }
      d_repetitions[input[j]].push(index)
      index += 1;
    }

    for(let i = 1; i < input.length; i+=1){
      let d = {};
      index = 1;
      let p_sub_1 = ""
      let ps_sub_1 = []
      let p_sub_2 = ""
      let ps_sub_2 = []
      for(let j = 0; j < i; j+=1){
        if(d[input[j]] == undefined){
          d[input[j]] = index;
          index += 1;
        }
        p_sub_1 += "x"+d[input[j]];
        if(selected_boxes_names[j].wasSolved){
          ps_sub_1.push([j,selected_boxes_names[j].solvedName]); 
        }
      } 
      index = 1;
      d = {};
      for(let j = i; j < input.length; j+=1){
        if(d[input[j]] == undefined){
          d[input[j]] = index;
          index += 1;
        }
        p_sub_2 += "x"+d[input[j]];
        if(selected_boxes_names[j].wasSolved){
          ps_sub_2.push([j-i,selected_boxes_names[j].solvedName]); 
        }
      }
      
      let res = this.testPatternsForTwoWords(check_sum, p_sub_1, ps_sub_1, p_sub_2, ps_sub_2, message_pattern, this.global_multi_word_dictionary, this.global_boxes_names, remove_space);
      
      sorted_by_freq.push(...res[0])
      check_sum = sorted_by_freq.length
      sorted_by_name.push(...res[1])
    }
    return [sorted_by_freq, sorted_by_name];
  }

  private testPatternsForTwoWords(check_sum, pattern1, solved_idx_pattern1, pattern2, solved_idx_pattern2, original_message_pattern, sorted_dictionary_by_pattern:{}, boxes_names:Box[], remove_space:Boolean=false):[Box[],Box[]]{
    let sorted_by_freq:Box[] = []
    let sorted_by_name:Box[] = []
    for(let i1 in sorted_dictionary_by_pattern[pattern1]){
      let word1 = sorted_dictionary_by_pattern[pattern1][i1]
      let flag = false;
      for(let s_idx1 of solved_idx_pattern1){
        if(word1.charAt(s_idx1[0]) != s_idx1[1]){ 
          flag = true; 
          break;
        }
      }
      if(flag) {continue;}
      for(let i2 in sorted_dictionary_by_pattern[pattern2]){
        let word2 = sorted_dictionary_by_pattern[pattern2][i2]
        let flag2 = false;
        for(let s_idx2 of solved_idx_pattern2){
          if(word2.charAt(s_idx2[0]) != s_idx2[1]){ 
            flag2 = true; 
            break;
          }
        }
        if(flag2) {continue;}
        let combined_string = ""+word1+" "+word2;
        
        let index = 1
        let d = {}
        let combined_string_pattern = ""
        for(let i = 0; i < combined_string.length; i+=1){
          if(combined_string[i] == CONST_WORD_SEPAR_REPLACED) {continue}
          if(d[combined_string[i]] == undefined){
            d[combined_string[i]] = index;
            index += 1;
          }
          combined_string_pattern += "x"+d[combined_string[i]];
        }
        if(original_message_pattern != combined_string_pattern){continue;}
        if(remove_space){
          combined_string = ""+word1+word2;
        }
        let box = this.initiateBox(combined_string, combined_string, boxes_names.length + sorted_by_freq.length + check_sum + 1, BOX_COLORS.NEW_RESULT)
        let box2 = this.initiateBox(combined_string, combined_string, boxes_names.length + sorted_by_freq.length + check_sum + 1, BOX_COLORS.NEW_RESULT)

        sorted_by_freq.push(box);
        sorted_by_name.push(box2);
      }
    }
    return [sorted_by_freq,sorted_by_name];
  }

  public findGoodCandidates(all_pattern_dictionaries:{}, char_separator:string):any[]{
    let results_f3 = []
    if(this.global_is_separator){
      let splits = this.global_input_string.split(CONST_WORD_SEPAR_REPLACED)
      
      let total_length = 0;
      for(let split of splits){
        let result_array = []
        for(let element of split.split(char_separator)){
          if(element != ""){result_array.push(element)}
        }
        let len = result_array.length
        let r_f3 = this.initiateResultsWithSeparator(total_length, total_length+len-1, result_array, 0, len, all_pattern_dictionaries);

        total_length += len + 1;
        if(r_f3 == null) {continue}
        results_f3.push(...r_f3);
      }
      results_f3.sort((a,b)=>b[a.length-1] - a[a.length-1]) 
    }
    else{
      for(let i = this.global_min_window_size; i < this.global_max_window_size; i+=1){
        for(let j = 0; j <= this.global_input_string.length - i; j+=1){
          let split = this.global_input_string.slice(j, j+i)
          let result_array = []
          for(let element of split.split(char_separator)){
            if(element != ""){result_array.push(element)}
          }
          let len = result_array.length
          let r_f3 = this.initiateResultsWithoutSeparator(j, j+i, result_array, 0, len, all_pattern_dictionaries);
          if(r_f3 == null) {
            continue
          }
          results_f3.push(...r_f3);
        }
      }
      results_f3.sort((a,b)=>b[a.length-1] - a[a.length-1]) 
      // find single multi for each of the first 1/3 of result from above
      
      results_f3 = this.findSingleMultiForResults(results_f3 as [])
    }
    return results_f3;
  }

  private initiateResultsWithSeparator(total_begin_index:number, total_end_index:number, str:string[], begin_index:number, end_index:number, all_pattern_dictionaries:{}, showWindowSize:Boolean=false):[]{
    let numerical_dictionary = all_pattern_dictionaries["eng_5000_sorted_by_pattern"]
    let pattern_dictionary = all_pattern_dictionaries["eng_5000_repetition_sorted_by_pattern"]
    let r_f3 = []
    let num_results = [], pattern_results = []

    let [res_str, counter_v3] = this.findRepetitionPattern(str, begin_index, end_index);
    
    if(counter_v3 <= 0 ) {return null}
    
    let num_res = numerical_dictionary[this.findNumeralPattern(str)]
    if( num_res != undefined){
      num_results.push(...num_res)
    }
    let pat_res = pattern_dictionary[res_str]
    if( pat_res != undefined){
      pattern_results.push(...pat_res)
    }

    if(num_results.length <= 0 && pattern_results.length <= 0){ return null; }

    r_f3.push([total_begin_index, total_end_index, str, res_str, num_results, pattern_results, pattern_results.length, counter_v3]);

    return r_f3 as []
  }

  private initiateResultsWithoutSeparator(total_begin_index:number, total_end_index:number, str:string[], begin_index:number, end_index:number, all_pattern_dictionaries:{}, showWindowSize:Boolean=false):[]{
    let numerical_dictionary = all_pattern_dictionaries["eng_5000_sorted_by_pattern"]
    let pattern_dictionary = all_pattern_dictionaries["eng_5000_repetition_sorted_by_pattern"]
    let r_f3 = [] 

    let [res_str, counter_v3] = this.findRepetitionPattern(str, begin_index, end_index);

    if(counter_v3 <= 0 || counter_v3 < this.global_minimum_score_on_candidate) {return null}

    if(numerical_dictionary[this.findNumeralPattern(str)] == undefined && pattern_dictionary[res_str] == undefined){return null}

    r_f3.push([total_begin_index, total_end_index, str, res_str, [], [], 0, counter_v3]);

    return r_f3 as []
  }

  private findSingleMultiForResults(print_results:[]):[]{ 
    let results = []
    for( let i = 0; i < print_results.length; i+=1){
      let single_and_multi_word_results = []
      let row = print_results[i]
      let t_b_index = row[0]
      let t_e_index = row[1]
      if(t_e_index - t_b_index <= 5){continue}
      let single_word_results = this.findSingleWord(this.global_boxes_names.slice(t_b_index, t_e_index))
      if(single_word_results != null && single_word_results != undefined && single_word_results[0].length > 0 && single_word_results[0].length < 10){ //&& single_word_results[0].length < 10
        let result_array = []
        for(let res of single_word_results[0]){
          let box:Box = res;
          result_array.push(box.currentName)
        }
        single_and_multi_word_results.push(...result_array)
      }

      let multi_word_results = this.findTwoWords_v3(this.global_boxes_names.slice(t_b_index, t_e_index), true);
      if(multi_word_results != null && multi_word_results != undefined && multi_word_results[0].length > 0 && multi_word_results[0].length < 10){ //&& multi_word_results[0].length < 10
        let result_array = []
        for(let res of multi_word_results[0]){
          let box:Box = res;
          result_array.push(box.currentName)
        }
        single_and_multi_word_results.push(...result_array)
      }
      if(single_and_multi_word_results.length <= 0){continue}
      results.push([row[0],row[1],row[2],row[3],row[4],single_and_multi_word_results,row[6],row[7]])
    }
    return results as [];
  }

  public findNumeralPattern(input_string:string[]):string{
    let d = {};
    let i = 1;
    // will be of pattern x1x2x2x3 for word 'abbc'
    let input_pattern = "";
    for(let substr of input_string){
      if( d[substr] == undefined) {
        d[substr] = "x"+i;
        i+=1;
      }
    }
    for(let substr of input_string){
      input_pattern += d[substr];
    }
    return input_pattern
  }

  public findRepetitionPattern(input_string:string[], begin_index:number, end_index:number):[string, number]{
    let index = 0, counter_v3 = 0
    let d = {}, current_results = []

    for(let k = begin_index; k < end_index; k+=1){
      let l = input_string[k]
      if (d[l] == undefined){ d[l] = [] }   
      d[l].push(index)
      index += 1
      if( d[l].length > 1){ current_results.push(d[l]); }
    }

    let first = true
    let input_pattern = "";
    for(let i = 0; i < current_results.length; i+=1){
      if(!first){ input_pattern += ";"}
      let _first = true
      let c = 0
      let c2 = 0
      let last = Number.MAX_VALUE
      for(let l of current_results[i]){ // cycle through all elements in [..., 1-3-5,...]
        if(!_first){ input_pattern +="," }
        input_pattern += l
        _first = false
        if(last + 1 === l){c2 += 1; }
        last = l;
        c += 1;
      }
      first = false
      counter_v3 += c
      if(c2 > 0){
        counter_v3 += Math.pow(2,2)
      }
    }
    return [input_pattern, counter_v3]
  }

  public initiateBox(character:string, pattern:string, index:number, color:string=BOX_COLORS.NEW_BOX):Box{
    let w = BOX_DEFAULT_WIDTH
    if(character.length > 5){w = ((character.length)*15) + "px"}
    return {
      id:index, 
      pattern:pattern,
      originalName:character,
      currentName:character, 
      displayName:character, 
      solvedName:character, 
      isClicked:false,  
      wasSolved:false, 
      color:color,
      width:w} as Box;
  }

  public sortAndLoadDictionary(all_dictionaries, name:string){
    let sorted_dict = {}
    let items = Object.keys( all_dictionaries[name] ).map(
      (key) => { return [key, all_dictionaries[name][key]] });

    items.sort(
      (first, second) => { return second[0].length - first[0].length }
    )
    let sorted_dict_by_patterns = {}
    items.forEach(pair => {
      sorted_dict[pair[0]] = pair[1]
      if(sorted_dict_by_patterns[pair[1]] == undefined){
        sorted_dict_by_patterns[pair[1]] = []
      }
      sorted_dict_by_patterns[pair[1]].push(pair[0])
    });
    all_dictionaries[name + "_sorted"] = sorted_dict
    all_dictionaries[name + "_sorted" + "_by_pattern"] = sorted_dict_by_patterns
  }

  public setBoxSelected(inner_box:Box, currentName:string, p:string){
    inner_box.currentName = currentName;  
    inner_box.displayName = p;
    inner_box.solvedName = p;
    inner_box.wasSolved = true;
    inner_box.isClicked = false;
    inner_box.color = BOX_COLORS.TEXT_SOLVED;
  }

  public checkIfFailed(arr1:Box[], arr2:Box[]):boolean{
    if(arr1 == undefined    || arr2 == undefined || 
       arr1 == null         || arr2 == null || 
       arr1[0] == undefined || arr2[0] == undefined || 
       arr1.length <= 0     || arr2.length <= 0){
      return true;
    }
    return false;
  }

  //// AUTOMATIC SOLVER ////
  public solveRecursivelyInitializer(results_f3, dictionary:{}, separator, depth, width):[Box[],string]{
    this.global_mask_best_score = 0;
    this.global_pattern_dict = dictionary["eng_5000_repetition_sorted_by_pattern"];
    this.global_results_f3 = results_f3;
    this.global_is_separator = separator;

    if(results_f3.length == 0){return}
    if(separator){
      this.solveRecursively(0,depth,width,this.solved_mask, this.global_forbidden_symbols)
    }
    else{
      this.solveRecursively(0,depth,width,this.solved_mask, this.global_forbidden_symbols)
    }
    
    let new_global_boxes:Box[] = []
    let i = this.global_boxes_names[0].id;
    let index = 0
    let j = 0
    for(let p of this.solved_mask){
      if(this.global_word_separator[index] != undefined){
        let box = this.initiateBox(CONST_WORD_SEPAR_REPLACED, "",i + j, BOX_COLORS.UNCLICABLE_TEXT)
        new_global_boxes.push(box)
        j+=1
      }
      let inner_box = this.global_boxes_names[i]
      if(inner_box == undefined){break}
      inner_box.id = i+j;
      if(p != '*' && p != ' '){
        this.setBoxSelected(inner_box, inner_box.currentName, p);
      }
      new_global_boxes.push(inner_box)
      i+=1
      index += 1
    }
    return [new_global_boxes, this.solved_mask];
  }

  private solveRecursively(lvl:number, max_depth:number, max_candidate:number, mask_replaced:string, forbidden_symbols:{}){
    let new_fobidden_symbols = {}
    Object.assign(new_fobidden_symbols, forbidden_symbols)
    if(lvl == max_depth){ this.replaceSolved(mask_replaced); return }

    let candidate:any = this.global_results_f3[lvl];
    if(candidate == undefined){ this.replaceSolved(mask_replaced); return }
    let good_candidates = [];

    for(let c of candidate[5]){
      let max_length = candidate[1] - candidate[0] + 1;
      if(c.length > max_length){continue}
      let candidate_failed = false;
      for(let i:number = candidate[0]; i < candidate[1]; i+=1){
        let character:string = mask_replaced[i];
        let correct_index = i - candidate[0];
        if(character != CONST_WORD_SEPAR_REPLACED && character != "*" && c.charAt(correct_index) != character){
          candidate_failed = true;
        }
      }
      if(!candidate_failed){good_candidates.push(c)}
    }

    let counter = 0;
    let len = good_candidates.length;

    if( len > 0){
      for(let i = 0; i < len; i+=1){
        let gc = good_candidates[i];
        counter+=1;
        if(counter >= max_candidate){ this.replaceSolved(mask_replaced); return}
        let updated_mask = this.update(mask_replaced, candidate[0], candidate[1], gc, new_fobidden_symbols);
        if(updated_mask == null) {
          continue;
        }
        this.solveRecursively(lvl+1, max_depth, max_candidate, updated_mask, new_fobidden_symbols);
      }
    }
    else{
      this.solveRecursively(lvl+1, max_depth, max_candidate, mask_replaced, new_fobidden_symbols);
    }
  }

  private update(mask_replaced:string, gc_begin_index:number, gc_end_index:number, gc:string, new_fobidden_symbols:{}):string{
    let updated_mask:string = mask_replaced.slice()
    for(let i = 0; i < gc_end_index - gc_begin_index; i+=1){
      if(gc[i] == undefined){ return updated_mask }
      let tmp:string = this.global_candidate_list[i + gc_begin_index];
      if(tmp == undefined) {break;}
      let list = this.global_candidate_list_sorted[tmp];
      let len = list.length;
      for(let j = 0; j < len; j+=1){
        let indexes = list[j];
        let character = updated_mask.charAt(indexes);
        if( character != CONST_WORD_SEPAR_REPLACED && character != "*" ){
          if(character == gc[i] || new_fobidden_symbols[gc[i]] != undefined){continue;}
          return null;
        }
        updated_mask = this.replaceCharAt(updated_mask, indexes, gc[i])
        new_fobidden_symbols[gc[i]] = 0;
      }
    }
    return updated_mask;
  }

  // REPLACE SOLVED BOXES //
  private replaceSolved(mask_replaced:string){
    let score = 0;
    let word_separator = {}
    if(this.global_is_separator){
      
      for(let symbol of mask_replaced){
        if(symbol != CONST_WORD_SEPAR_REPLACED && symbol != "*"){ score += 1; }
      }
    }
    else{ score = this.countWordsThatFitMask(mask_replaced); }
    if(this.global_mask_best_score < score){
      this.global_mask_best_score = score;
      this.solved_mask = mask_replaced;
      this.global_word_separator = word_separator;
    } 
  }

  private replaceCharAt(string:string, index:number, replacement:string):string{
    return string.substring(0, index) + replacement + string.substring(index + replacement.length);
  }

  private countWordsThatFitMask(mask_replaced:string):number{
    let total_score = 0;
    for(let i = 0; i < mask_replaced.length; i+=1){
      let max_timer = 0;
      let max_score = 0;
      let word_length = 0;
      let max_word_length = 1;
      for(let j = i+1; j < mask_replaced.length; j+=1){
        let score = 0 
        if(max_timer == 10){break;}
        let sub_mask = mask_replaced.substring(i,j)
        let pattern = this.findNumeralPattern(sub_mask.split(''));
        if(this.global_multi_word_dictionary[pattern] != undefined){
          if( this.global_multi_word_dictionary[pattern].includes(sub_mask) ){
            word_length = Math.round(pattern.length / 2)
            score = Math.pow(word_length,2)
            if(score > max_score){
              max_score = score;
              max_timer = 0;
              if(word_length > max_word_length){
                max_word_length = word_length
              }
            }
          }
        }
        max_timer += 1;
      }
      total_score += max_score;
      max_score = 0;
      i+=max_word_length-1;
    }

    return total_score
  }
  // to fully understand and appreciate these accomplishments lets take at some of the most wellknown parts of the human body the head or the spherical body part that contains the brain and rests at the top of the human body has quite a few individual organs and body parts on it it should quickly be mentioned that hair occupies the space on top of the head and the ears the organs responsible for hearing are located on either side of the head from top to bottom the eyebrows or horizontal strips of hair that can be found above the eye are the first components of the head the eyes are below them and are round orblike organs that allow humans to see
  // it goes without saying that humans mammals identifiable as those that stand upright and are comparatively advanced and capable of detailed thought have pretty remarkable bodies given all that theyve accomplished furthermore an especially intelligent human brain produced this text to be sure humans have overcome predators disease and all sorts of other obstacles over thousands of years to fully understand and appreciate these accomplishments lets take at some of the most wellknown parts of the human body the head or the spherical body part that contains the brain and rests at the top of the human body has quite a few individual organs and body parts on it it should quickly be mentioned that hair occupies the space on top of the head and the ears the organs responsible for hearing are located on either side of the head from top to bottom the eyebrows or horizontal strips of hair that can be found above the eye are the first components of the head the eyes are below them and are round orblike organs that allow humans to see the eyes make way for the nose or an external stickingout organ that plays an important part in the breathing and bacteriaelimination processes below that is the mouth or a wide cavernous organ that chews food removes bacteria helps with breathing and more the mouth contains teeth or small whitecolored pointed body parts used to chew food and the tongue or a redcolored boneless organ used to chew food and speak

}