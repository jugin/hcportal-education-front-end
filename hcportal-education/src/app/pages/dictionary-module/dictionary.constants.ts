

export const ENCRYPTED_MESSAGE = 'wkh-pdmru-dlp-ri-wklv-whfkqltxh-lv-wr-lqyhvwljdwh-wr-zkdw-hawhqw-yhuedo-dqg-qrqyhuedo-ihdwxuhv-ri-olduv-dqg-wuxwk-whoohuv-ehkdylrxu-fkdqjh-gxulqj-wkh-frxuvh-ri-uhshdwhg-lqwhuurjdwlrqv';//'thisisacustomencryptedtext';

export const TEXT_MIN = 1;
export const TEXT_MAX = 2000;

export const NAME_CIPHER = 'Monoalphabetic Substitution Cipher';
export const TYPE_CIPHER = 'Dictionary attack on';

export const TOP_GAP = 110;
export const SIDE_MENU = [
  {
    title: "Word Patterns",
    active: true,
    id: "description",
    bottomPosition: 0,
    topPosition: 0
  },
  {
    title: "Attack Description",
    active: true,
    id: "description2",
    bottomPosition: 0,
    topPosition: 0
  },
  {
    title: "Dictionary Attack",
    active: true,
    id: "dictionary_attack",
    bottomPosition: 0,
    topPosition: 0
  }
];

export const BOX_COLORS = {
  "NEW_BOX": "transparent",
  "NEW_RESULT": "#fff8bd",
  "TEXT_CLICKED": "lightgreen",
  "RESULTS_CLICKED": "#ffe500", 
  "TEXT_SOLVED": "#ffa3a3",
  "TEXT_SOLVED_CLICKED": "#FF5050", 
  "TEXT_CLICKED_HIGHLIGHT": "#FFA700",
  "UNCLICABLE_TEXT":"#dedede"

}

export const BOX_DEFAULT_WIDTH:string = "45px"

export const CONST_WORD_SEPAR_REPLACED = ' ';

export const DEPTH = 50;
export const WIDTH = 250;