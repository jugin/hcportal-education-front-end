import { Component, OnInit, OnDestroy, Input, Output, ViewChild, ElementRef } from '@angular/core';
import { SIDE_MENU, TOP_GAP, NAME_CIPHER, TYPE_CIPHER, ENCRYPTED_MESSAGE, TEXT_MAX, TEXT_MIN, BOX_COLORS, CONST_WORD_SEPAR_REPLACED, WIDTH, DEPTH } from '../../dictionary-module/dictionary.constants';
import { DictionaryService } from '../dictionary.service';
import { HeaderService } from "../../../components/header/header.service";
import { UntypedFormGroup, UntypedFormControl,Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Box } from '../Box';
import { ALPHABET } from 'src/app/constants/language.constants';
import { DICT_5000_PATTERNS } from '../dictionary_5000_patterns';
import { DICT_5000_PATTERNS_REPETITION } from '../dictionary_5000_patterns_repetition';

@Component({
  selector: 'app-dictionary',
  templateUrl: './dictionary.component.html',
  styleUrls: ["../../../app.component.css",'./dictionary.component.css']
})
export class Dictionary implements OnInit, OnDestroy {

  @ViewChild('fileUploader') fileUploader:ElementRef;

  private subscrMessage: Subscription;

  depth_is_incorrect = false;
  width_is_incorrect = false;
  mask = "";
  // separators options + ngram options
  custom_dictionary = false;
  custom_input_sep = false;
  custom_input_sep_value = "";
  word_separator_selected = true;
  custom_input_word_sep_value = "-";

  // display primary a pattern or encrypted text
  displayPattern = false;

  // to Find Selected should we find words or try for other combinations
  displaySingleWordsOnly = true;

  // Default dictionary and all dictionaries
  current_dictionary_name = "eng_5000";
  all_pattern_dictionaries = {};

  // Global variables for different boxes on page
  previous_result_clicked:Box = null;
  table: Box[];
  selected_boxes: Box[] = [];
  table_results: Box[];
  selected_dict_result:Box;
  pattern_results: Box[];

  // Global Dictionary to save user INPUT's values
  // transformed into xi, where 'i' is some
  // random value assigned for the currenct character
  dict = {};

  // Constants imported and used for HTML settings
  textMax = TEXT_MAX;    textMin = TEXT_MIN;
  sideMenu = SIDE_MENU;  
  topGap = TOP_GAP;
  cipherInputsForm: UntypedFormGroup;
  showPartialTable: boolean = false;
  encrypted_message = ENCRYPTED_MESSAGE;
  encrypted_message_double = ENCRYPTED_MESSAGE;
  recursive_depth = DEPTH;
  recursive_width = WIDTH;

  // Sorted by NAME and Sorted by frequency result dictionaries
  results_sorted_by_name: Box[] = [];
  results_sorted_by_freq: Box[] = [];
  display_sorted_by_name = false;

  solve_automatically_in_progress = false;

  hint_1_is_active = false;
  hint_2_is_active = false;
  hint_3_is_active = false;

  computation_complete = false;

  constructor(
    private dictionaryService: DictionaryService,
    headerService: HeaderService
  ) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next(NAME_CIPHER);
    headerService.cipherType.next(TYPE_CIPHER);
  }

  ngOnInit(): void {
    // build default dictionaries
    this.all_pattern_dictionaries["eng_5000"] = Object.assign({}, DICT_5000_PATTERNS);
    this.all_pattern_dictionaries["eng_5000_repetition"] = Object.assign({}, DICT_5000_PATTERNS_REPETITION);

    // sort default dictionaries
    this.dictionaryService.sortAndLoadDictionary(this.all_pattern_dictionaries,"eng_5000")
    this.dictionaryService.sortAndLoadDictionary(this.all_pattern_dictionaries,"eng_5000_repetition")
    
    //initialize dictionary patterns using random numbers in Z26
    let index = Math.floor(Math.random() * 26) + 1;
    for(let character of ALPHABET){
      this.dict[character] = index;
      index = (index % 26) + 1;
    }

    this.cipherInputsForm = new UntypedFormGroup({
      encrypted_message: new UntypedFormControl(ENCRYPTED_MESSAGE, [
        Validators.required,
        Validators.minLength(TEXT_MIN),
        Validators.maxLength(TEXT_MAX),
      ]),
    });

    this.subscrMessage = this.cipherInputsForm.controls.encrypted_message.valueChanges.subscribe(
      (newMessage) => { this.encrypted_message = newMessage; this.encrypted_message_double = newMessage; } 
    );
    
    this.createTable(true);
  }

  createTable(fromCode:boolean = false): void{
    if(!fromCode && !this.custom_input_word_sep_value && this.encrypted_message.includes(' ')){
      alert("Please after using SPACE as separator, also set \'word separator\' parameter.");
      return;
    }

    this.mask = ""
    if(this.word_separator_selected && (this.custom_input_word_sep_value == undefined || this.custom_input_word_sep_value.length <= 0)){
      alert("Word Separator is not defined")
      return;
    }
    this.encrypted_message = this.encrypted_message_double;
    let char_separator = this.custom_input_sep_value;
    let word_separator = this.custom_input_word_sep_value;

    if(this.word_separator_selected){
      let splits = this.encrypted_message.split(word_separator)
      if(splits.length < 2){ alert("Incorrect word separator (separator not found)"); return;}
      this.encrypted_message = this.encrypted_message.replaceAll(word_separator, CONST_WORD_SEPAR_REPLACED);
    }
    for(let char of this.encrypted_message){
      if(char == CONST_WORD_SEPAR_REPLACED){
        this.mask += CONST_WORD_SEPAR_REPLACED
      }
      else{
        this.mask += "*"
      }
    }
    // reset all used dictionaries to empty values
    this.resetDictionaries(true)

    // initialize displayed boxes
    this.table = this.dictionaryService.initializeTable(
      this.encrypted_message, 
      char_separator,
      this.word_separator_selected
    )

    if(!this.showPartialTable) { this.showPartialTable = true; }

    // initialize service for upcoming computation, save global variables
    this.dictionaryService.init(
      this.table, 
      char_separator, 
    );

    // update dictionaries that will be used for computation
    // we do not need to refference every dictionary when we want to work with it
    this.dictionaryService.updateDictionaries(
      this.all_pattern_dictionaries[this.current_dictionary_name],
      this.all_pattern_dictionaries[this.current_dictionary_name + "_sorted" + "_by_pattern"]
    );
  }
  
  automaticallySolve(){
    if(this.width_is_incorrect || this.depth_is_incorrect){return}
    // if we do not have separator selected but the option to use separator is
    if(this.word_separator_selected && (this.custom_input_word_sep_value == undefined || this.custom_input_word_sep_value.length <= 0)){
      alert("Word Separator is not defined")
      return;
    }
    this.computation_complete = false;
    this.solve_automatically_in_progress = true;

    // to display loading animation we must delay the element in this function by few milliseconds
    setTimeout(() => {
      // if user replaced some text, make the characters they
      // chose as forbidden to be used in automative computation
      this.dictionaryService.checkReplacedCharacters(this.table);
      // compute possible candidates for this input
      let candidates = this.dictionaryService.findGoodCandidates(this.all_pattern_dictionaries,this.custom_input_sep_value)
      
      // solve recursively
      if(candidates.length == 0){alert("No pattern matches for given input.");return;}
      [this.table, this.mask] = this.dictionaryService.solveRecursivelyInitializer(
        candidates, 
        this.all_pattern_dictionaries,
        this.word_separator_selected, 
        this.recursive_depth, 
        this.recursive_width
      );
      this.dictionaryService.init(
        this.table, 
        this.custom_input_sep_value, 
      );
      this.resetDictionaries();

      this.solve_automatically_in_progress = false;
      this.computation_complete = true;
    }, 50); 
}

  clickBox(id: number): void{
    if(this.table[id].currentName == CONST_WORD_SEPAR_REPLACED) { return }
    let box:Box = this.table[id] as Box;
    let first_box_id:number, last_box_id:number;
    if(this.selected_boxes.length > 0){ // if we want to unselect selected box
      first_box_id = this.selected_boxes[0].id;
      last_box_id = this.selected_boxes[this.selected_boxes.length - 1].id;
      if(box.isClicked){ // is the box we clicked inside the range of boxes?
        if(box.id !== first_box_id && box.id !== last_box_id){ return } 
      }
      else{ 
        if( box.id + 1 !== first_box_id && box.id - 1 !== last_box_id){ return } 
      }
    }
    box.isClicked = !box.isClicked;
    box.isClicked ? this.addClickedInputBox(box) : this.removeClickedInputBox(box, id)
  }

  resultClicked(id: number): void{
    if(this.previous_result_clicked != null){
      this.previous_result_clicked.isClicked = false;
      this.previous_result_clicked.color = BOX_COLORS.NEW_RESULT;
    }
    let result:Box = this.table_results[id-this.table.length-1] as Box;
    result.isClicked = !result.isClicked;
    if(result.isClicked){
      this.selected_dict_result = result
      result.color = BOX_COLORS.RESULTS_CLICKED;
    }
    else{
      this.selected_dict_result = undefined
      result.color = BOX_COLORS.NEW_RESULT;
    }
    this.previous_result_clicked = result
  }

  findResultsForSequence(): void{
    // if no selected boxes to be found, return
    if(this.selected_boxes.length < 1){ alert("Select new boxes to be replaced!"); return }
    // an array of boxes to be printed as results on right side
    this.table_results = [];

    if(this.displaySingleWordsOnly){
      [this.results_sorted_by_freq, this.results_sorted_by_name] 
      = this.dictionaryService.findSingleWord(this.selected_boxes);

      if(this.dictionaryService.checkIfFailed(this.results_sorted_by_freq, this.results_sorted_by_name)){
        alert("Not such word in DATASET");
        return
      }

      this.table_results = (this.display_sorted_by_name ? this.results_sorted_by_name : this.results_sorted_by_freq);
    }
    else{
      [this.results_sorted_by_freq, this.results_sorted_by_name] 
      = this.dictionaryService.findTwoWords_v3(this.selected_boxes); 

      if(this.dictionaryService.checkIfFailed(this.results_sorted_by_freq, this.results_sorted_by_name)){return}

      this.sortByName(this.results_sorted_by_name[0].id);
      
      this.table_results = (this.display_sorted_by_name ? this.results_sorted_by_name : this.results_sorted_by_freq);
    }
  }

  replaceWithResult(): void{
    if(this.selected_boxes.length < 1){ alert("Select new boxes to be replaced!"); return }
    if(this.selected_dict_result === undefined){ alert("Select word to replace with!"); return }

    let RESULT_NAME_PATTERN:string[] = this.selected_dict_result.displayName.split("")
    let BOX_INDEX = 0
    let BEGIN_INDEX:number = 0
    let END_INDEX:number = this.selected_boxes.length

    if(!this.displaySingleWordsOnly){
      for(let pat of RESULT_NAME_PATTERN){
        if(pat == CONST_WORD_SEPAR_REPLACED ) {continue;}
        let box = this.selected_boxes[BOX_INDEX]
        let t = box.pattern
        
        let p = pat
        for(let inner_box of this.table){
          if(inner_box.pattern === t){
            this.dictionaryService.setBoxSelected(inner_box, box.currentName, p);
          }
        }
        BOX_INDEX+=1
      }
    }
    else{
      for(let j = BEGIN_INDEX; j < END_INDEX; j+=1){
        let box = this.selected_boxes[j]
        let t = box.pattern
        let p = RESULT_NAME_PATTERN[BOX_INDEX]
        for(let inner_box of this.table){
          if(inner_box.pattern === t){
            this.dictionaryService.setBoxSelected(inner_box, box.currentName, p);
          }
        }
        BOX_INDEX+=1
      }
    }
    this.resetDictionaries()
  }

  onSortResults(): void{
    this.display_sorted_by_name = !this.display_sorted_by_name
    this.table_results = (this.display_sorted_by_name ? this.results_sorted_by_name : this.results_sorted_by_freq);
  }

  clearAllSelected(): void{
    for(let i = 0; i < this.selected_boxes.length; i+=1){
      let id = this.selected_boxes[i].id;
      let box = this.selected_boxes[i];
      if(box.wasSolved){
        for(let box2 of this.table){
          if(box2.pattern == box.pattern){
            box2.displayName = box2.originalName;
            box2.currentName = box2.originalName;
            box2.solvedName = "";
            box2.wasSolved = false;
            box2.isClicked = false;
            box2.color = BOX_COLORS.NEW_BOX;
          }
        }
      }
      else{
        box.isClicked = false;
        box.color = BOX_COLORS.NEW_BOX;
      }
    }
    this.selected_boxes = [];
    
  }

  onTextPatternChange(): void{
    this.displayPattern = !this.displayPattern
    if(this.displayPattern){
      for(let box of this.table){
        box.currentName = box.displayName
        box.displayName = box.pattern
      }
    }
    else{
      for(let box of this.table){
        box.displayName = box.currentName
        box.currentName = box.originalName
      }
    }
  }

  onSingleMultiChange(): void{
    this.table_results = []
    this.displaySingleWordsOnly = !this.displaySingleWordsOnly;
  }

  onCustomDictOptionChange(dicionary_name:string): void{
    this.current_dictionary_name = dicionary_name
    if(dicionary_name === "custom_xyz") { this.custom_dictionary = true }
    else{ this.custom_dictionary = false }    
    this.dictionaryService.updateDictionaries(
      this.all_pattern_dictionaries[this.current_dictionary_name],
      this.all_pattern_dictionaries[this.current_dictionary_name + "_sorted" + "_by_pattern"]
    );
  }

  onFileSelected(event): void{
    const file:File = event.target.files[0];
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let splits = fileReader.result.toString().split('\n')
      let custom_dict = {}
      let custom_dict2 = {}
      for (let s_str of splits){
        s_str = s_str.split('\r')[0]
        s_str = s_str.split(' ')[0]

        let pattern1 = this.dictionaryService.findNumeralPattern(s_str.split(""))
        custom_dict[s_str] = pattern1
        
        let pattern2 = this.dictionaryService.findRepetitionPattern(s_str.split(""), 0, s_str.length)   
        custom_dict2[s_str] = pattern2
      }
      this.all_pattern_dictionaries["custom_xyz"] = custom_dict
      this.all_pattern_dictionaries["custom_xyz_repetition"] = custom_dict2
      this.dictionaryService.sortAndLoadDictionary(this.all_pattern_dictionaries,"custom_xyz")
      this.dictionaryService.sortAndLoadDictionary(this.all_pattern_dictionaries,"custom_xyz_repetition")
    }
    fileReader.readAsText(file)
  }

  onSelectSpaceAsSeparator(input_word_separator){
    this.word_separator_selected = !this.word_separator_selected;
    if(this.word_separator_selected == false){
      this.custom_input_word_sep_value = undefined;
      input_word_separator.value = '';
    }
  }
  onInputCharSeparatorChange(value:string){
    this.custom_input_sep_value = value;
  }
  onInputWordSeparatorChange(value:string){
    this.custom_input_word_sep_value = value;
  }

  onRecursiveDepthChange(value){
    if (isNaN(+value)) {
      this.depth_is_incorrect = true;
    }
    else{
      this.depth_is_incorrect = false;
      this.recursive_depth = +value;
    }
  }
  onRecursiveWidthChange(value){
    if (isNaN(+value)) {
      this.width_is_incorrect = true;
    }
    else{
      this.width_is_incorrect = false;
      this.recursive_width = +value;
    }
  }

  private resetDictionaries(flag:boolean=false){
    if(flag) { this.table = [] }
    this.previous_result_clicked = null;
    this.table_results = [];
    this.selected_boxes = [];
    this.selected_dict_result = undefined;
    this.pattern_results = [];
  }

  private addClickedInputBox(box:Box){
    this.selected_boxes.push(box)
    this.selected_boxes.sort((a,b) => a.id - b.id)
    box.color = (box.wasSolved ? BOX_COLORS.TEXT_SOLVED_CLICKED : BOX_COLORS.TEXT_CLICKED)
  }

  private removeClickedInputBox(box:Box, id:number){
    box.color = (box.wasSolved ? BOX_COLORS.TEXT_SOLVED : BOX_COLORS.NEW_BOX)
    this.selected_boxes.forEach((element,index)=>{
      if(element.id==id) this.selected_boxes.splice(index,1);
    });
  }

  // to save complexity, sort results by name after finding matches for two words
  private sortByName(index:number){
    this.results_sorted_by_name.sort((a, b) => (a.currentName > b.currentName) ? 1 : -1);
    for(let box of this.results_sorted_by_name){
      box.id = index;
      index += 1;
    }
  }

  onHint1Press(hint1){
    this.hint_1_is_active = !this.hint_1_is_active
    if(!this.hint_1_is_active){ hint1.textContent = "Show Instructions"}
    else if(this.hint_1_is_active){ hint1.textContent = "Hide Instructions"}
  }
  onHint2Press(hint2){
    this.hint_2_is_active = !this.hint_2_is_active
    if(!this.hint_2_is_active){ hint2.textContent = "Show Instructions"}
    else if(this.hint_2_is_active){ hint2.textContent = "Hide Instructions"}
  }
  onHint3Press(hint3){
    this.hint_3_is_active = !this.hint_3_is_active
    if(!this.hint_3_is_active){ hint3.textContent = "Show Instructions"}
    else if(this.hint_3_is_active){ hint3.textContent = "Hide Instructions"}
  }

  ngOnDestroy(): void {
    this.subscrMessage.unsubscribe()
  }

}
