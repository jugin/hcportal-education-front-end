import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Dictionary} from './dictionary-component/dictionary.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { DictionaryService } from './dictionary.service';
import { CustomCheckboxComponent } from './custom-checkbox/custom-checkbox.component';
import { BoxValueComponent } from './box-value/box-value.component';

const dictionaryRoutes: Routes = [
  { path: '', component: Dictionary },
];

@NgModule({
  declarations: [
    Dictionary,
    CustomCheckboxComponent,
    BoxValueComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(dictionaryRoutes),
    SharedModule
  ],
  providers: [DictionaryService]

})
export class DictionaryModule { }
