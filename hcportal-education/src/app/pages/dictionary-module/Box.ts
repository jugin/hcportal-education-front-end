export interface Box{
    id: number,
    pattern: string,
    originalName: string,
    currentName: string,
    displayName: string,
    solvedName: string,
    isClicked: boolean,
    wasSolved: boolean,
    color: string,
    width: string,
}


