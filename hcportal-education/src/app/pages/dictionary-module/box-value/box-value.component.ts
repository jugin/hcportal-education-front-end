import { Component, Input, Output } from '@angular/core';

@Component({
  selector: 'app-box-value',
  templateUrl: './box-value.component.html',
  styleUrls: ["../../../app.component.css",'./box-value.component.css']
})
export class BoxValueComponent {
  @Input() center: string = "0";
  @Input() subscript: string = "";
  @Input() color: string = "transparent";
  @Input() wasSolved: boolean = false;
  @Input() width: string = "50px";
}

export function changeState():void{
  this.wasSolved = true
}
