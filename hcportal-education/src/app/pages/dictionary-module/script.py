
def f1():
    f = open("./src/app./pages./dictionary-module/dictionary_5000.txt", "r")
    s = "export const DICT_5000_PATTERNS = {"
    s2 = "export const DICT_5000_SINGLE_OCCURENCE_PATTERNS = {"
    isFirst = False
    strings = {}
    for item in f.readlines():
        item = item.split('\n')[0]
        i = 1
        d = {}
        d2 = {}
        transformed = ""
        for c in item:
            if d.get(c) == None:
                d[c] = "x"+str(i)
                i+=1
        for c in item:
            transformed += d[c]
        if isFirst:
            s+=","
        else:
            isFirst = True
        strings[item] = transformed
        s+="'" + item + "':'" +transformed+ "'" 
    s+="} \n"
    f2 = open("./src/app./pages./dictionary-module/dictionary_5000_patterns.ts", 'w')
    f2.write(s)

    isFirst = False
    d2 = {}
    for str, pattern in strings.items():
        if d2.get(pattern) == None:
            d2[pattern] = str
        else:
            d2[pattern] = -1
            continue

    for pattern, str in d2.items():
        if str == -1:
            continue
        if isFirst:
            s2+=","
        else:
            isFirst = True
        s2+="'" + str + "':'" +pattern+ "'"
    s2+="}"   
    f2.write(s2)

    f.close()
    f2.close()

def f2():
    f = open("./src/app./pages./dictionary-module/dictionary_5000.txt", "r")
    str_to_print = "export const DICT_5000_PATTERNS_REPETITION = {"
    isFirst = False
    for item in f.readlines():
        item = item.split('\n')[0]
        index = 0
        d = {}
        current_results = {}
        for k in range(0,len(item)):
            l = item[k]
            if d.get(l) == None:
                d[l] = []
            d[l].append(index)
            index += 1
            if len(d[l]) > 1:
                current_results[l] = d[l]
        first = False
        res_str = ""
        for key in current_results:
            if first:
                res_str += ";"
            _first = False
            for next_repetition_id in current_results[key]:#(let l of p){ # cycle through all elements in [..., 1-3-5,...]
                if _first:
                    res_str +=","
                res_str += str(next_repetition_id)
                _first = True
            
            first = True
        if isFirst:
            str_to_print+=","
        else:
            isFirst = True
        str_to_print+="'" + item + "':'" +res_str+ "'" 
    str_to_print+="} \n"
    f2 = open("./src/app./pages./dictionary-module/dictionary_5000_patterns_repetition.ts", 'w')
    f2.write(str_to_print)
    f.close()
    f2.close()

f2()