f = open("./src/app./pages./dictionary-module/dictionary_5000.txt", "r")
arr = []
total_characters = 0
total_rows = 0
max_length = 0
max_length_word = ""
for item in f.readlines():
    item = item.split('\n')[0]
    d = {}
    sub_total = 0
    if len(item) > max_length:
        max_length = len(item)
        max_length_word = item
    for c in item:
        sub_total += 1
        if d.get(c) == None:
            d[c] = True
        else:
            arr.append(item)
            break
    total_characters += sub_total
    total_rows += 1

print(arr)
print(len(arr))
print(total_characters / total_rows)
print(max_length, max_length_word)
f.close()