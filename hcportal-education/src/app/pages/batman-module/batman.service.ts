const letterToImage: Record<string, string> = {
    'A': '/assets/symbols/batman_symbols/a.png',
    'B': '/assets/symbols/batman_symbols/b.png',
    'C': '/assets/symbols/batman_symbols/c.png',
    'D': '/assets/symbols/batman_symbols/d.png',
    'E': '/assets/symbols/batman_symbols/e.png',
    'F': '/assets/symbols/batman_symbols/f.png',
    'G': '/assets/symbols/batman_symbols/g.png',
    'H': '/assets/symbols/batman_symbols/h.png',
    'I': '/assets/symbols/batman_symbols/i.png',
    'J': '/assets/symbols/batman_symbols/j.png',
    'K': '/assets/symbols/batman_symbols/k.png',
    'L': '/assets/symbols/batman_symbols/l.png',
    'M': '/assets/symbols/batman_symbols/m.png',
    'N': '/assets/symbols/batman_symbols/n.png',
    'O': '/assets/symbols/batman_symbols/o.png',
    'P': '/assets/symbols/batman_symbols/p.png',
    'Q': '/assets/symbols/batman_symbols/q.png',
    'R': '/assets/symbols/batman_symbols/r.png',
    'S': '/assets/symbols/batman_symbols/s.png',
    'T': '/assets/symbols/batman_symbols/t.png',
    'U': '/assets/symbols/batman_symbols/u.png',
    'V': '/assets/symbols/batman_symbols/v.png',
    'W': '/assets/symbols/batman_symbols/w.png',
    'X': '/assets/symbols/batman_symbols/x.png',
    'Y': '/assets/symbols/batman_symbols/y.png',
    'Z': '/assets/symbols/batman_symbols/z.png',
};

let accumulatedText = '';

export class BatmanService {

    textToImages(textareaId: string, targetDivId: string, message: string) {
        const textarea = document.getElementById(textareaId) as HTMLTextAreaElement;
        const targetDiv = document.getElementById(targetDivId);

        if (!textarea || !targetDiv) {
            console.error('Textarea or div not found');
            return;
        }

        const text = message;
        targetDiv.innerHTML = '';

        for (const char of text.toUpperCase()) {
            if (letterToImage[char]) {
                const img = document.createElement('img');
                img.src = letterToImage[char];
                targetDiv.appendChild(img);
            }
        }

    }

    decrypt(value: any): void {
        //const button = document.activeElement as HTMLButtonElement;

        //if (button && button.tagName === 'BUTTON' && button.value) {
       if(value){
            this.updateDivWithText(value);
        } else {
            console.error('No active button element with value found');
        }

    }

    updateDivWithText(text: string): void {
        const targetDiv = document.getElementById('target-div-2');
        if (!targetDiv) {
            console.error('Target div not found');
            return;
        }
        accumulatedText += text;
        targetDiv.textContent = accumulatedText;
    }

    eraseLastCharacter(): void {
        console.log('eraseLastCharacter function called');

        if (accumulatedText.length > 0) {
            accumulatedText = accumulatedText.slice(0, -1);

            const targetDiv = document.getElementById('target-div-2');
            if (targetDiv) {
                targetDiv.textContent = accumulatedText;
            } else {
                console.error('Target div not found');
            }
        } else {
            console.error('No characters to erase');
        }
    }

}
