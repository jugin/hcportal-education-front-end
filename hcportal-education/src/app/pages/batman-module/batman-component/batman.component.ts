import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  UntypedFormGroup,
  Validators,
  UntypedFormControl,
} from "@angular/forms";
import CommonUtils from "../../../utils/common-utils";
import { BatmanService } from "../batman.service";
import {
  MESSAGE,
  NAME_CIPHER,
  TYPE_CIPHER,
  TOP_GAP,
  SIDE_MENU,
  THE_BATMAN1,
  THE_BATMAN2,
} from "../batman.constant";
import { Subscription } from "rxjs";
import { HeaderService } from "../../../components/header/header.service";

@Component({
  selector: "app-batman",
  styleUrls: ['./batman.component.css', "../../../app.component.css"],
  templateUrl: "./batman.component.html",
})
export class Batman implements OnInit, OnDestroy {

  private message = MESSAGE;
  private subscrMessage: Subscription;

  the_batman1 = THE_BATMAN1;
  the_batman2 = THE_BATMAN2;

  // var for sideNavbar
  sideMenu = SIDE_MENU;
  topGap = TOP_GAP;

  toggleOptions: string[] = CommonUtils.createArrayOfLength(26, 1);
  cipherInputsForm: UntypedFormGroup;

  constructor(
    private batmanService: BatmanService,
    headerService: HeaderService
  ) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next(NAME_CIPHER);
    headerService.cipherType.next(TYPE_CIPHER);
  }

  ngOnInit(): void {
    this.cipherInputsForm = new UntypedFormGroup({
      message: new UntypedFormControl(MESSAGE, [
        Validators.required,
        Validators.pattern("^[a-zA-Z]+$"),
        Validators.minLength(1),
        Validators.maxLength(2000),
      ]),
    });

    this.subscrMessage = this.cipherInputsForm.controls.message.valueChanges.subscribe(
      (newMessage) => {
        this.message = newMessage;
      }
    );

    this.enDeCryptMessage();
  }

  public enDeCryptMessage() {
    this.batmanService.textToImages('source-textarea', 'target-div', this.message);
  }

  ngOnDestroy() {
    this.subscrMessage.unsubscribe();
  }

  deCrypt(value :any) {
    this.batmanService.decrypt(value);
  }

  erase() {
    this.batmanService.eraseLastCharacter();
  }

}
