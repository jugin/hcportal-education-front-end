export const NAME_CIPHER = 'Cipher form The Batman (2022) movie';
export const TYPE_CIPHER = 'Demonstration of a';
export const THE_BATMAN1 = "assets/img/the_batman1.PNG";
export const THE_BATMAN2 = "assets/img/the_batman2.png";

export const MESSAGE = 'HELIESSTILL';

export const TOP_GAP = 110;
export const SIDE_MENU = [
    {
        title: "Description of the cipher",
        active: true,
        id: "description",
        bottomPosition: 0,
        topPosition: 0

    },
    {
        title: "Input setup",
        active: true,
        id: "inputs",
        bottomPosition: 0,
        topPosition: 0
    },
    {
        title: "Simulation",
        active: true,
        id: "simulation",
        bottomPosition: 0,
        topPosition: 0
    }
];
