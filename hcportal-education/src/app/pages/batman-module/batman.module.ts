import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Batman } from './batman-component/batman.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { BatmanService } from './batman.service';

const caesarRoutes: Routes = [
    { path: '', component: Batman },
];

@NgModule({
    declarations: [
        Batman
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(caesarRoutes),
        SharedModule
    ],
    providers: [BatmanService]

})
export class BatmanModule { }
