import { ALPHABET } from '../../constants/language.constants';

export class MartianAsciiService {

    encrypt(formatedMessage: string): string {

        const encryptedText: string[] = [];
        for (let i = 0; i < formatedMessage.length; i++) {
            const hexCode = formatedMessage.charCodeAt(i).toString(16).toUpperCase();
            encryptedText.push(hexCode);
        }
        return encryptedText.join('');
    }

    decrypt(encryptedText: string): string {

        const hexPairs: string[] = [];
        let decryptedText = '';
        
        for (let i = 0; i < encryptedText.length; i += 2) {
            hexPairs.push(encryptedText.slice(i, i + 2));
        }
    
        for (const hexPair of hexPairs) {
            const letter = String.fromCharCode(parseInt(hexPair, 16));
            decryptedText += letter;
        }

        return decryptedText;
    }

    public decryptForEveryKey(encryptedText: string): Array<string> {

        const decryptedTexts = [];
        for (let key = 1; key <= ALPHABET.length; key++) {
            decryptedTexts.push(
                this.decrypt(encryptedText)
            );
        }
        return decryptedTexts;
    }

}
