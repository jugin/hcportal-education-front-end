import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MartianAscii } from './martian-ascii-component/martian-ascii.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { MartianAsciiService } from './martian-ascii.service';

const caesarRoutes: Routes = [
    { path: '', component: MartianAscii },
];

@NgModule({
    declarations: [
        MartianAscii
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(caesarRoutes),
        SharedModule
    ],
    providers: [MartianAsciiService]

})
export class MartianAsciiModule { }
