export const NAME_CIPHER = 'Cipher form the Martian (2015) movie';
export const TYPE_CIPHER = 'Demonstration of a';
export const MARTIAN1 = "assets/img/martian1.png";
export const MARTIAN2 = "assets/img/martian2.png";
export const MARTIAN_TABLE = "assets/img/martian_table.png";

export const MESSAGE = 'HOWALIVE';

export const TOP_GAP = 110;
export const SIDE_MENU = [
    {
        title: "Description of the cipher",
        active: true,
        id: "description",
        bottomPosition: 0,
        topPosition: 0

    },
    {
        title: "Input setup",
        active: true,
        id: "inputs",
        bottomPosition: 0,
        topPosition: 0
    },
    {
        title: "Analysis",
        active: true,
        id: "analysis",
        bottomPosition: 0,
        topPosition: 0
    }
];
