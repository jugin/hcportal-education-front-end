import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MartianAscii } from './martian-ascii.component';

describe('MartianAsciiModuleComponent', () => {
  let component: MartianAscii;
  let fixture: ComponentFixture<MartianAscii>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MartianAscii ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MartianAscii);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
