import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  UntypedFormGroup,
  Validators,
  UntypedFormControl,
} from "@angular/forms";
import { MartianAsciiService } from "../martian-ascii.service";
import {
  MESSAGE,
  NAME_CIPHER,
  TYPE_CIPHER,
  TOP_GAP,
  SIDE_MENU,
  MARTIAN1,
  MARTIAN2,
  MARTIAN_TABLE,
} from "../martian-ascii.constant";
import { Subscription } from "rxjs";
import { HeaderService } from "../../../components/header/header.service";

@Component({
  selector: "app-martian-ascii",
  styleUrls: ['./martian-ascii.component.css', "../../../app.component.css"],
  templateUrl: "./martian-ascii.component.html",
})
export class MartianAscii implements OnInit, OnDestroy {
  private message = MESSAGE;
  private subscrMessage: Subscription;
  martian1 = MARTIAN1;
  martian2 = MARTIAN2;
  martian_table = MARTIAN_TABLE;

  // var for sideNavbar
  sideMenu = SIDE_MENU;
  topGap = TOP_GAP;
  
  encryptedText = "";
  decryptedText = "";
  
  cipherInputsForm: UntypedFormGroup;

  constructor(
    private martianAsciiService: MartianAsciiService,
    headerService: HeaderService
  ) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next(NAME_CIPHER);
    headerService.cipherType.next(TYPE_CIPHER);
  }

  ngOnInit(): void {

    this.cipherInputsForm = new UntypedFormGroup({
      message: new UntypedFormControl(MESSAGE, [
        Validators.required,
        Validators.pattern("^[a-zA-Z]+$"),
        Validators.minLength(1),
        Validators.maxLength(2000),
      ]),
    });

    this.subscrMessage = this.cipherInputsForm.controls.message.valueChanges.subscribe(
      (newMessage) => {
        this.message = newMessage;
      }
    );
    this.enDeCryptMessage();
  }

  public enDeCryptMessage() {

    this.encryptedText = this.martianAsciiService.encrypt(
      this.message
    );
    this.decryptedText = this.martianAsciiService.decrypt(
      this.encryptedText
    );
  }

  ngOnDestroy() {
    this.subscrMessage.unsubscribe();
  }
}
