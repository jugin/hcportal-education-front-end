import { NgModule } from '@angular/core';
import { A11yModule } from '@angular/cdk/a11y';

import { CommonModule } from '@angular/common';
import { BellasoMethodComponent } from './bellaso-method-component/bellaso-method.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

const bellasoRoutes: Routes = [{ path: '', component: BellasoMethodComponent }];

@NgModule({
  exports: [A11yModule],
  declarations: [BellasoMethodComponent],
  imports: [CommonModule, RouterModule.forChild(bellasoRoutes), SharedModule],
})
export class BellasoMethodModule {}
