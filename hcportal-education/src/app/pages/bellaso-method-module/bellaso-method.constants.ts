export const NAME_CIPHER = `Bellaso's method (creating numeric permutations from passphrases)`;
export const TYPE_CIPHER = 'Demonstration of';
export const INITIAL_MESSAGE = 'gateway';
export const TOP_GAP = 120;
export const SIDE_MENU = [
  {
    title: 'Input setup',
    active: true,
    id: 'inputs',
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: 'Sorted phrase',
    active: true,
    id: 'sortedPhrase',
    bottomPosition: 0,
    topPosition: 0,
  },
  {
    title: 'Created permutation',
    active: true,
    id: 'createdPermutation',
    bottomPosition: 0,
    topPosition: 0,
  },
];
export const MIN_PHRASE_LENGTH = 2;
export const MAX_PHRASE_LENGTH = 2000;
