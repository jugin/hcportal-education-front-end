import { Component } from '@angular/core';
import {
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { Subject, Subscription } from 'rxjs';

import { HeaderService } from '../../../components/header/header.service';
import {
  NAME_CIPHER,
  TYPE_CIPHER,
  SIDE_MENU,
  TOP_GAP,
  INITIAL_MESSAGE,
  MIN_PHRASE_LENGTH,
  MAX_PHRASE_LENGTH,
} from '../bellaso-method.constants';

@Component({
  selector: 'app-bellaso',
  templateUrl: './bellaso-method.component.html',
  styleUrls: ['../../../app.component.css', './bellaso-method.component.css'],
})
export class BellasoMethodComponent {
  private phrase = INITIAL_MESSAGE;
  private subscribedPhrase: Subscription;
  private sortedPhrase: string[];
  phraseImmutable: string;
  sideMenu = SIDE_MENU;
  topGap = TOP_GAP;

  cipherInputsForm: UntypedFormGroup;
  minPhraseLength = MIN_PHRASE_LENGTH;
  maxPhraseLength = MAX_PHRASE_LENGTH;

  sortedPhraseTableReady = new Subject<boolean>();
  sortedPhraseTableData;
  phraseNumbers: string[];

  permutationTableReady = new Subject<boolean>();
  permutationTableData;
  permutation: string[];
  permutationString: string;
  steps: {
    usedLetters: string;
    availableLetters: string;
    pickedLetter: string;
    transformation: string;
  }[];

  constructor(headerService: HeaderService) {
    headerService.showInfoPanel.next(true);
    headerService.cipherName.next(NAME_CIPHER);
    headerService.cipherType.next(TYPE_CIPHER);
  }

  ngOnInit(): void {
    this.sortedPhraseTableReady.next(false);
    this.permutationTableReady.next(false);

    this.cipherInputsForm = new UntypedFormGroup({
      phrase: new UntypedFormControl(this.phrase, [
        Validators.required,
        Validators.pattern('^[a-zA-Z]+$'),
        Validators.minLength(this.minPhraseLength),
        Validators.maxLength(this.maxPhraseLength),
      ]),
    });

    this.subscribedPhrase =
      this.cipherInputsForm.controls.phrase.valueChanges.subscribe(
        (newMessage) => {
          this.phrase = newMessage;
        }
      );

    this.getPermutationFromPhrase();
  }

  public getPermutationFromPhrase() {
    this.phraseImmutable = this.phrase;
    this.createSortedPhraseTable();
    this.createPermutationTable();
  }

  private createSortedPhraseTable() {
    this.sortedPhraseTableReady.next(false);

    const lowercasePhrase = this.phrase.toLowerCase();
    this.sortedPhrase = lowercasePhrase.split('').sort();
    const sortedSplitPhraseCopy = [...this.sortedPhrase];
    //add null to the beggining because we are numbering columns from 1
    sortedSplitPhraseCopy.unshift(null);
    this.sortedPhraseTableData = [sortedSplitPhraseCopy];
    this.phraseNumbers = this.sortedPhrase.map((_, index) =>
      (index + 1).toString()
    );

    this.sortedPhraseTableReady.next(true);
  }

  private createPermutationTable() {
    this.createPermutation();
    this.permutationTableReady.next(false);
    const lowercasePhrase = this.phrase.toLowerCase();
    const splitPhrase = lowercasePhrase.split('');
    splitPhrase.unshift(null);
    this.permutationTableData = [splitPhrase];

    const permutationString = this.permutation.join(', ');
    this.permutationString = `(${permutationString})`;

    this.permutationTableReady.next(true);
  }

  private createPermutation() {
    this.steps = [];
    const availableLettersImmutable = [...this.sortedPhrase];
    let availableLetters = [...this.sortedPhrase];
    let usedLetters = '';

    const permutation: (string | number)[] = Array.from(
      this.phrase,
      (item) => item
    );

    for (let i = 0; i < availableLettersImmutable.length; i++) {
      const pickedLetter = availableLettersImmutable[i];
      const order = i + 1;

      const foundPhraseItemIndex = this.phrase
        .split('')
        .findIndex(
          (item, index) =>
            item === pickedLetter && typeof permutation[index] !== 'number'
        );

      permutation[foundPhraseItemIndex] = order;
      this.steps.push({
        usedLetters,
        availableLetters: availableLetters.join(''),
        pickedLetter,
        transformation: permutation.join(', '),
      });
      usedLetters += pickedLetter;
      availableLetters.shift();
    }

    this.permutation = Array.from(permutation, (item) => item.toString());
  }

  ngOnDestroy() {
    this.subscribedPhrase.unsubscribe();
  }
}
