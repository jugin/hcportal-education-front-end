import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewCipherComponent } from './view-cipher.component';

describe('ViewCipherComponent', () => {
  let component: ViewCipherComponent;
  let fixture: ComponentFixture<ViewCipherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewCipherComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewCipherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
