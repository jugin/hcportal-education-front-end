import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export const PERMUTATION_REGEX =
  /^\s*[1-9]{1}[1-9]*(\s*\,\s*[1-9]{1}[1-9]*)*\s*$/;
export const PERMUTATION_REGEX_CLOSURES =
  /^\s*\({1}\s*[1-9]{1}[1-9]*(\s*\,\s*[1-9]{1}[1-9]*)*\s*\){1}\s*$/;

export default class ValidationUtils {
  static permutationBaseValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;

      if (!value) {
        return null;
      }

      const validPermutation = PERMUTATION_REGEX.test(value);
      const validPermutationClosures = PERMUTATION_REGEX_CLOSURES.test(value);

      const validValidation = validPermutation || validPermutationClosures;

      return !validValidation ? { permutationValid: true } : null;
    };
  }

  static permutationSequenceValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;

      if (!value) {
        return null;
      }

      const noSpecialChars = value.replace(/[^0-9\,]/g, '');
      const splitByComma = noSpecialChars.split(',').sort();

      for (let i = 0; i < splitByComma.length; i++) {
        if (Number(splitByComma[i]) !== i + 1) {
          return { permutationSequence: true };
        }
      }

      return null;
    };
  }

  static permutationAttackMaxBlockSize(maxSize: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;

      if (!value) {
        return null;
      }

      if (value > maxSize) {
        return { permutationAttackMaxBlockSize: true };
      }
      
      return null;
    };
  }
}
