import AnalysisText from '../analysis-text';
import {
  REF_BIGRAMS,
  REF_DICTIONARY_1000,
  REF_DICTIONARY_10000,
  REF_DICTIONARY_100000,
  REF_DICTIONARY_25000,
  REF_DICTIONARY_5000,
  REF_DICTIONARY_50000,
} from '../constants/language.constants';
import {
  EvaluationLength,
  EvaluationMethod,
  TAnagramColumns,
  TAnagramWorkerArgs,
  TTopColumns,
} from '../models/anagram.model';

const TOP_COLUMNS_LENGTH = 10;

export default class AnagramColumnsUtils {
  static refBigrams = JSON.parse((REF_BIGRAMS as any).default);

  static getDictionary(evaluationMethod: EvaluationMethod) {
    switch (evaluationMethod) {
      case EvaluationMethod.DICT1000:
        return REF_DICTIONARY_1000;
      case EvaluationMethod.DICT5000:
        return REF_DICTIONARY_5000;
      case EvaluationMethod.DICT10000:
        return REF_DICTIONARY_10000;
      case EvaluationMethod.DICT25000:
        return REF_DICTIONARY_25000;
      case EvaluationMethod.DICT50000:
        return REF_DICTIONARY_50000;
      case EvaluationMethod.DICT100000:
        return REF_DICTIONARY_100000;
      default:
        return null;
    }
  }

  static getScoreFromDict(
    text: string,
    minDictWordLength: number,
    evaluationMethod: EvaluationMethod
  ): number {
    let longestSubstringLen = 0;
    if (!text) {
      return longestSubstringLen;
    }
    const dictionary = this.getDictionary(evaluationMethod);
    if (!dictionary) {
      return longestSubstringLen;
    }

    for (let i = 0; i < dictionary.length; i++) {
      const word = dictionary[i];
      if (word.length < minDictWordLength) {
        continue;
      }
      if (!text.includes(word)) {
        continue;
      }

      if (word.length > longestSubstringLen) {
        longestSubstringLen = word.length;
      }
    }

    return longestSubstringLen;
  }

  static getScoreFromBigrams(text: string, minDictWordLength: number): number {
    const freqBigramsIteration = AnalysisText.getFreqOfBigrams(text);
    // Get diff of Referal values (EN) and decrypted text
    // that is fitnes func for evaluate of decrypted text
    const sumIteration: number = AnalysisText.getSumOfDiffBigramsFromRef(
      freqBigramsIteration[0],
      text.length - 1,
      this.refBigrams
    );

    return sumIteration;
  }

  static getScore(
    rowTexts: string[],
    evaluationMethod: EvaluationMethod,
    minDictWordLength: number
  ): number {
    switch (evaluationMethod) {
      case EvaluationMethod.BIGRAMS:
        return rowTexts.reduce(
          (scoreAcc, rowText) =>
            scoreAcc +
            this.getScoreFromBigrams(rowText, minDictWordLength ?? 0),
          0
        );
      default:
        return rowTexts.reduce(
          (scoreAcc, rowText) =>
            scoreAcc +
            this.getScoreFromDict(
              rowText,
              minDictWordLength ?? 0,
              evaluationMethod
            ),
          0
        );
    }
  }

  static getRowTextsFromColumns(
    movableColumns: TAnagramColumns,
    length: number
  ): string[] {
    let rowTexts: string[] = [];

    for (let col = 0; col < length; col++) {
      for (let row = 0; row < movableColumns[col].rows.length; row++) {
        const foundChar = movableColumns[col].rows[row].char;
        rowTexts[row] = rowTexts[row] ? rowTexts[row] + foundChar : foundChar;
      }
    }
    return rowTexts;
  }

  static getScoreFromColumns(args: TAnagramWorkerArgs) {
    const {
      movableColumns,
      hint,
      minDictWordLength,
      evaluationMethod,
      evaluationLength,
    } = args;

    let evaluatedTextLength = movableColumns.length;

    switch (evaluationLength as EvaluationLength) {
      case EvaluationLength.HINT_LENGTH:
        evaluatedTextLength = hint.length;
        break;
      case EvaluationLength.DOUBLE_HINT_LENGTH:
        evaluatedTextLength = hint.length * 2;
        break;
      case EvaluationLength.WHOLE:
      default:
        evaluatedTextLength = movableColumns.length;
        break;
    }

    const rowTexts = this.getRowTextsFromColumns(
      movableColumns,
      evaluatedTextLength
    );
    return this.getScore(rowTexts, evaluationMethod, minDictWordLength);
  }

  static maybeInsertIntoTopColumns(
    score: number,
    columns: TAnagramColumns,
    topColumns: TTopColumns,
    anagrams: string[],
    evaluationMethod: EvaluationMethod
  ) {
    for (let i = 0; i < TOP_COLUMNS_LENGTH; i++) {
      if (
        topColumns.reduce(
          (acc, topColumn) =>
            acc || topColumn.anagrams.join('') === anagrams.join(''),
          false
        )
      ) {
        continue;
      }
      const evaluation =
        evaluationMethod === EvaluationMethod.BIGRAMS
          ? score < topColumns[i]?.score
          : score > topColumns[i]?.score;

      if (!topColumns[i] || evaluation) {
        topColumns.splice(i, 0, {
          score,
          columns,
          anagrams,
          select: 'Select',
        });
      }
    }
  }
}
