import {
  TPermutationMovableColumns,
  TTopColumns,
} from '../models/permutation-table.model';
import * as DICTIONARY_5000 from '../constants/dictionary_5000.json';

const TOP_COLUMNS_LENGTH = 10;

export default class PermutationMovableColumnsUtils {
  static dictionary = (DICTIONARY_5000 as any).default;

  static getPermutationFromColumns(
    movableColumns: TPermutationMovableColumns
  ): string {
    const positions = movableColumns.map((c) => c.pos);
    const permutation = positions.join(',');
    return `(${permutation})`;
  }

  static getDictLongestOccuranceFromText(
    text: string,
    minWordLength?: number
  ): number {
    if (!text) {
      return 0;
    }

    const lenOccurance = this.dictionary.reduce((lenAcc, word) => {
      if (word.length < minWordLength) {
        return lenAcc;
      }
      if (!text.toLowerCase().includes(word)) {
        return lenAcc;
      }

      return word.length + lenAcc;
    }, 0);

    return lenOccurance;
  }

  static getTextFromColumns(movableColumns: TPermutationMovableColumns) {
    const rowTexts: string[] = [];

    let firstColumnOffset = 0;
    movableColumns[0].rows.every((r) => {
      if (r) {
        return false;
      }
      ++firstColumnOffset;
      return true;
    });
    for (let col = 0; col < movableColumns.length; col++) {
      let offset = 0;
      loop2: for (let row = 0; row < movableColumns[col].rows.length; row++) {
        const rowObject = movableColumns[col].rows[row];
        if (!rowObject) {
          ++offset;
          continue;
        }
        const remaindingOffset = firstColumnOffset - offset;
        if (remaindingOffset < 0) {
          throw Error(`invalid aligment ${firstColumnOffset} ${offset}`);
        }
        for(let charI = remaindingOffset; charI < rowObject.length; charI++){
          const foundChar = rowObject[charI];
            const rowIndex = charI + offset;
            rowTexts[rowIndex] = rowTexts[rowIndex]
              ? rowTexts[rowIndex] + foundChar
              : foundChar;
        }
        break loop2;
      }
    }

    return rowTexts.join('');
  }

  static maybeInsertIntoTopColumns(
    score: number,
    columns: TPermutationMovableColumns,
    decryptedText: string,
    topColumns: TTopColumns
  ) {
    for (let i = 0; i < TOP_COLUMNS_LENGTH; i++) {
      const containsText = topColumns.reduce(
        (acc, top) => acc || top.decryptedText === decryptedText,
        false
      );
      if (containsText) {
        continue;
      }
      const permutation = this.getPermutationFromColumns(columns);
      if (!topColumns[i] || score > topColumns[i].score) {
        topColumns.splice(i, 0, {
          score,
          columns,
          decryptedText,
          permutation,
          select: 'Select',
        });
      }
    }
  }
}
