import { TPermutationMovableColumns } from '../models/permutation-table.model';

export default class TranspositionUtils {
  static encrypt(tableData, permutation?: (string | number)[]) {
    const baseString = '';
    const columnBaseString = '';
    const columnPermutation = permutation ?? Object.keys(tableData[0]);

    return columnPermutation.reduce(
      (previousString, columnIndex) =>
        columnIndex === '0'
          ? previousString
          : previousString +
            tableData.reduce((previousColumn, _, rowIndex) => {
              if (!tableData[rowIndex][columnIndex]) {
                return previousColumn;
              }
              return previousColumn + tableData[rowIndex][columnIndex];
            }, columnBaseString),
      baseString
    );
  }

  static transformPermutation(value: string) {
    const noSpecialChars = value.replace(/[^0-9\,]/g, '');
    return noSpecialChars.split(',');
  }

  static getFillerCiphers(
    strips: TPermutationMovableColumns,
    stripIndex: number,
    fillerRows: number
  ) {
    // operation expects string rows to be on 0-th index
    const previosColumnLength = strips[stripIndex - 1].rows[0].length;
    const diff = previosColumnLength - fillerRows;
    const fromSliceIndex = diff < 0 ? 0 : diff;

    return strips[stripIndex - 1].rows[0]
      .slice(fromSliceIndex, previosColumnLength)
      .map((cipher: string) => cipher.toLowerCase());
  }
}
