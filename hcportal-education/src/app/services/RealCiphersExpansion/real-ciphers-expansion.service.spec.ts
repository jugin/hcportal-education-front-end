import { TestBed } from '@angular/core/testing';

import { RealCiphersExpansionService } from './real-ciphers-expansion.service';

describe('RealCiphersExpansionService', () => {
  let service: RealCiphersExpansionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RealCiphersExpansionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
