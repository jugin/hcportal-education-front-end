import { TestBed } from '@angular/core/testing';

import { ActiveClassificationService } from './active-classification-service.service';

describe('ActiveClassificationService', () => {
  let service: ActiveClassificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActiveClassificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
