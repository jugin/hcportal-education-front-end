# HCPortal-education-front-end

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) v15.

## Prerequisites

This project uses [npm](https://www.npmjs.com/) and needs [Node.js](https://nodejs.org/en) to work.


Steps to run the project:

* Install Node.js - version 14.20.x, 16.13.x or 18.10.x. [ref](https://angular.io/guide/update-to-version-15)
* Install angular cli - run command `npm i @angular/cli@15`
* Navigate to `hcportal-education` folder in this project
* Install necessary packages - run `npm install` command
* Start local server - run `ng serve` and navigate to `http://localhost:4200/`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
